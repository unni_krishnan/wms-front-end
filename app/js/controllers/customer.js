'use strict';

app.controller('CustomerController', ['$scope', '$uibModal', '$compile', '$state', '$log', 'CustomerService', 'UserProfile', 'AlertService',
    function($scope, $modal, $compile, $state, $log, CustomerService, UserProfile, AlertService) {

        $scope.dataTableOpt = {
            "processing": true,
            "language": {
                "processing": "<img src='img/loading.gif' width='50px' height='50px'>" //add a loading image,simply putting <img src="loader.gif" /> tag.
            },
            "serverSide": true,
            "serverSide": true,
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            "pagingType": "simple_numbers",

            "ajax": {
                "url": __env.apiUrl + '/customer-list',
                "type": "POST",
                "data": {
                    api_key: UserProfile.getApiKey()
                },
            },
            "columnDefs": [
                //     {
                //     "className": "dt-center",
                //     "targets": "_all"
                //   },
                //   {
                //     "targets": [3],
                //     render: function (data, type, row) {
                //       if (data == 1)
                //         return '<span class="label label-success">Active</span>';
                //       else return '<span class="label label-default">Inactive</span>';
                //     }
                //   },
                {
                    "targets": [0],
                    "className": "col-lg-2 col-md-2 col-sm-2 "
                },
                {
                    "targets": [1],
                    "className": "col-lg-2 col-md-3 col-sm-3 "
                },
                {
                    "targets": [2],
                    "className": "col-lg-2 col-md-1 col-sm-1 "
                },
                {
                    "targets": [3],
                    "className": "col-lg-1 col-md-1 col-sm-1 "
                },
                {
                    "targets": [4],
                    "className": "col-lg-1 col-md-1 col-sm-1 "
                },
                {
                    "targets": [5],
                    "className": "col-lg-1 col-md-1 col-sm-1 ",
                    "render": function(data, type, full) {
                        if (data)
                            return '<img width="100px" height="100px" src="' + __env.uploadUrl + '/customers/' + data + '"/>';
                        else return '<img width="100px" height="100px" src="'+__env.baseUrl+'img/no-dp.gif"/>';
                    }
                },
                {
                    "targets": [6],
                    "sortable": false,
                    "className": "col-lg-3 col-md-3 col-sm-3 ",
                    render: function(data, type, full) {
                        return '<span class="btn-primary btn-xs cursor-pointer" ng-click="viewCustomerDetail(' + data + ')"><i class="fa fa-eye hidden-xs"></i></span> &nbsp' +
                            // '<span class="btn-success btn-xs cursor-pointer" ng-click="editCustomer(' + data + ')"><i class="fa fa-pencil hidden-xs"></i> Edit</span>&nbsp' +
                            '<span class="btn-danger btn-xs cursor-pointer" ng-click="deleteCustomer(' + data + ')"><i class="fa fa-trash hidden-xs"></i></span> &nbsp';
                    }
                }

            ],
            "rowCallback": function(row, data, index) {
                $compile(row)($scope);
                return row;

            }
        }

        $scope.addCustomer = function() {
            $modal.open({
                templateUrl: 'partials/customer-add.html',
                controller: 'CustomerModalCtrl',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    customerData: null
                }
            });
        }

        $scope.viewCustomerDetail = function(customerId) {
            $state.go('app.customer-detail', { customerId: customerId });
        }

        $scope.editCustomer = function(customerId) {
            $modal.open({
                templateUrl: 'partials/customer-add.html',
                controller: 'CustomerModalCtrl',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    customerData: function() {
                        return CustomerService.listCustomer({ customer_id: customerId }).then(function(response) {
                            if (response.data.error)
                                AlertService.showErrorNotification("Sorry!! Something went wrong. ");
                            else
                                return response.data.data;
                        }, function(error) { AlertService.showErrorNotification("Sorry!! Something went wrong. "); });
                    }
                }
            });
        }

        $scope.deleteCustomer = function(customerId) {
            var modalInstance = $modal.open({
                templateUrl: 'partials/alert-warning.html',
                controller: 'WarningModalCtrl',
                backdrop: 'static',
                resolve: {
                    data: function() {
                        var warning = {};
                        warning.title = 'Delete';
                        warning.body = 'Are you sure you want to delete this customer?';
                        warning.showCancel = true;
                        return warning;
                    }
                }
            });

            modalInstance.result.then(function(selection) {
                // Warning accepted
                CustomerService.deleteCustomer({ customer_id: customerId }).then(function(response) {
                    $log.debug("Delete Customer Response ", response);
                    if (!response.data.error)
                        $state.go($state.current, {}, { reload: true });
                    else
                        AlertService.showErrorAlert('Error', 'An error occured while deleting customer.Please try again', false);
                }, function(error) { AlertService.showErrorNotification("Sorry!! Something went wrong. "); });
            }, function() {
                //  Warning dismissed
            });
        }

    }
]);


app.controller('CustomerModalCtrl', ['$scope', '$uibModalInstance', '$log', '$state', '$uibModal', 'UserProfile', 'CustomerService', 'customerData', 'AlertService',
    function($scope, $modalInstance, $log, $state, $modal, UserProfile, CustomerService, customerData, AlertService) {

        $scope.vm = {};
        $scope.vm.address_2 = '';
        $scope.vm.status = 1;
        $scope.active = [{ value: 1, text: 'Active' }, { value: 0, text: 'Inactive' }];

        $scope.saveCustomer = function() {
            $log.debug("POST Customer data ", $scope.vm);

            if ($scope.vm.customer_id) {

                // Update the data
                $scope.vm.type = 'basic_info';
                $scope.vm.data = {};
                $scope.vm.data.customer_name = $scope.vm.customer_name;
                $scope.vm.data.address = $scope.vm.address;
                $scope.vm.data.contact = $scope.vm.contact;
                $scope.vm.data.phone = $scope.vm.email;
                $scope.vm.data.email = $scope.vm.phone;
                CustomerService.editCustomer($scope.vm).then(function(response) {
                    $log.debug("Update Vendor Response ", response);
                    if (!response.data.error)
                        $state.go($state.current, {}, { reload: true });
                    else
                        AlertService.showErrorAlert();
                }, function(error) { AlertService.showErrorAlert(); });

            } else {
                // Create new customer

                $scope.vm.created_by = UserProfile.getUserProfile().id;
                CustomerService.createCustomer($scope.vm).then(function(response) {
                    $log.debug("Add Customer Response ", response);
                    if (!response.data.error)
                        $state.go($state.current, {}, { reload: true });
                    else
                        AlertService.showErrorAlert();
                }, function(error) { AlertService.showErrorAlert(); });
            }

            $modalInstance.close();

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.init = function() {
            if (customerData) {
                $scope.vm.customer_id = customerData.basic_info[0].customer_id;
                $scope.vm.customer_name = customerData.basic_info[0].customer_name;
                $scope.vm.address = customerData.basic_info[0].address;
                $scope.vm.email = customerData.basic_info[0].email;
                $scope.vm.phone = customerData.basic_info[0].phone;
                $scope.vm.contact = customerData.basic_info[0].contact;
            }
        }
    }
]);


app.controller('CustomerDetailController', ['$scope', '$http', 'editableOptions', '$log', '$state', '$uibModal', '$localStorage', '$filter', 'editableThemes', 'UserProfile', 'CustomerService', 'customerData', 'AlertService',
    function($scope, $http, editableOptions, $log, $state, $modal, $localStorage, $filter, editableThemes, UserProfile, CustomerService, customerData, AlertService) {

        editableThemes.bs3.inputClass = 'input-sm';
        editableThemes.bs3.buttonsClass = 'btn-sm';
        editableOptions.theme = 'bs3';

        $scope.tabIndex = $localStorage.customerDetailTabIndex ? $localStorage.customerDetailTabIndex : 0;

        $scope.assign = [{ value: 1, text: '✓' }, { value: 0, text: 'x' }];
        $log.debug("Customer Data ", customerData);
        $scope.init = function() {
            if (customerData) {
                $scope.basicData = customerData.basic_info[0];
                if ($scope.basicData.logo)
                    $scope.logoPath = __env.uploadUrl + '/customers/' + $scope.basicData.logo;
                else
                    $scope.logoPath = '';
                $scope.shipperData = customerData.shipper[0];
                $scope.skuData = customerData.skulist[0];
                $scope.billingData = customerData.billing[0];
                $scope.notificationData = customerData.notification[0];

            }
        }

        $scope.setSelectedTab = function(index) {
            $scope.tabIndex = index;
            $localStorage.customerDetailTabIndex = index;

        }

        $scope.updateCustomer = function(type) {
            var formData = {};
            formData.data = {};
            formData.customer_id = $scope.basicData.customer_id;
            switch (type) {
                case 'basic_info':
                    formData.type = 'basic_info';
                    formData.data.customer_name = $scope.basicData.customer_name;
                    formData.data.address_1 = $scope.basicData.address_1;
                    formData.data.address_2 = $scope.basicData.address_2;
                    formData.data.city = $scope.basicData.city;
                    formData.data.zip_code = $scope.basicData.zip_code;
                    formData.data.contact = $scope.basicData.contact;
                    formData.data.phone = $scope.basicData.phone;
                    formData.data.email = $scope.basicData.email;
                    formData.logo = $scope.basicData.logo;
                    break;
                case 'shipper':
                    formData.type = 'shipper';
                    formData.data.upc = $scope.shipperData.upc;
                    formData.data.fedex = $scope.shipperData.fedex;
                    formData.data.usps = $scope.shipperData.usps;
                    formData.data.dhl = $scope.shipperData.dhl;
                    break;
                case 'skulist':
                    formData.type = 'skulist';
                    formData.data.eachs = $scope.skuData.eachs;
                    formData.data.cases = $scope.skuData.cases;
                    formData.data.pallet = $scope.skuData.pallet;
                    formData.data.box = $scope.skuData.box;
                    formData.data.master_case = $scope.skuData.master_case;
                    break;
                case 'notification':
                    formData.type = 'notification';
                    formData.data.low_stock = $scope.notificationData.low_stock;
                    formData.data.receiving = $scope.notificationData.receiving;
                    formData.data.summary = $scope.notificationData.summary;
                    formData.data.adjustment = $scope.notificationData.adjustment;
                    formData.data.lowstock_email = $scope.notificationData.lowstock_email;
                    formData.data.receiving_email = $scope.notificationData.receiving_email;
                    formData.data.summary_email = $scope.notificationData.summary_email;
                    formData.data.adjustment_email = $scope.notificationData.adjustment_email;
                    break;
                case 'billing':
                    formData.type = 'billing';
                    formData.data.pallet_receiving = $scope.billingData.pallet_receiving;
                    formData.data.box_receiving = $scope.billingData.box_receiving;
                    formData.data.pallet = $scope.billingData.pallet;
                    formData.data.container_receiving = $scope.billingData.container_receiving;
                    formData.data.storage_fees = $scope.billingData.storage_fees;
                    formData.data.pick_pack_fee = $scope.billingData.pick_pack_fee;
                    formData.data.touch_fee = $scope.billingData.touch_fee;
                    formData.data.item_fee = $scope.billingData.item_fee;
                    formData.data.delivery_fee = $scope.billingData.delivery_fee;
                    formData.data.pickup_fee = $scope.billingData.pickup_fee;
                    formData.data.monthly_fee = $scope.billingData.monthly_fee;
                    formData.data.print_fee = $scope.billingData.print_fee;
                    formData.data.blank_fee = $scope.billingData.blank_fee;
                    formData.data.neck_label_fee = $scope.billingData.neck_label_fee;
                    formData.data.shipping_markup = $scope.billingData.shipping_markup;
                    formData.data.supplies_markup = $scope.billingData.supplies_markup;
                    break;

            }
            CustomerService.editCustomer(formData).then(function(response) {
                $log.debug("Update Customer Response ", response);
                if (!response.data.error)
                    $state.go($state.current, {}, { reload: true });
                else
                    AlertService.showErrorAlert();
            }, function(error) { AlertService.showErrorAlert(); })

        }

        $scope.addLowStockEmails = function() {
            $modal.open({
                scope: $scope,
                backdrop: 'static',
                template: '<div class="modal-header">{{notificationData.lowstock_email ? "View Emails" : "Add Emails"}}</div >' +
                    '<div class="modal-body">' +
                    '<input type="text" ui-jq="tagsinput" ng-model="notificationData.lowstock_email" class="form-control" />' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<button class="btn btn-primary pull-left" ng-click="addEmail()">Add E-mail</button>' +
                    '<button class="btn btn-default pull-right" ng-click="cancel()">Cancel</button>' +
                    '</div>',
                controller: ['$scope', '$uibModalInstance', function($scope, $modalInstance) {
                    $scope.addEmail = function() {
                        $scope.updateCustomer('notification');
                        $modalInstance.close();
                    };
                    $scope.cancel = function() {
                        $modalInstance.close();
                    }
                }]
            });
        }
        $scope.addAdjustmentEmails = function() {
            $modal.open({
                scope: $scope,
                backdrop: 'static',
                template: '<div class="modal-header">{{notificationData.adjustment_email ? "View Emails" : "Add Emails"}}</div >' +
                    '<div class="modal-body">' +
                    '<input type="text" ui-jq="tagsinput"  ng-model="notificationData.adjustment_email  " class="form-control" />' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<button class="btn btn-primary pull-left" ng-click="addEmail()">Add E-mail</button>' +
                    '<button class="btn btn-default pull-right" ng-click="cancel()">Cancel</button>' +
                    '</div>',
                controller: ['$scope', '$uibModalInstance', function($scope, $modalInstance) {
                    $scope.addEmail = function() {
                        $scope.updateCustomer('notification');
                        $modalInstance.close();
                    };
                    $scope.cancel = function() {
                        $modalInstance.close();
                    }
                }]
            });
        }
        $scope.addSummaryEmails = function() {
            $modal.open({
                scope: $scope,
                backdrop: 'static',
                template: '<div class="modal-header">{{notificationData.summary_email ? "View Emails" : "Add Emails"}}</div >' +
                    '<div class="modal-body">' +
                    '<input type="text" ui-jq="tagsinput" ng-model="notificationData.summary_email" class="form-control" />' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<button class="btn btn-primary pull-left" ng-click="addEmail()">Add E-mail</button>' +
                    '<button class="btn btn-default pull-right" ng-click="cancel()">Cancel</button>' +
                    '</div>',
                controller: ['$scope', '$uibModalInstance', function($scope, $modalInstance) {
                    $scope.addEmail = function() {
                        $scope.updateCustomer('notification');
                        $modalInstance.close();
                    };
                    $scope.cancel = function() {
                        $modalInstance.close();
                    }
                }]
            });
        }
        $scope.addReceivingEmails = function() {
            $modal.open({
                scope: $scope,
                backdrop: 'static',
                template: '<div class="modal-header">{{notificationData.receiving_email ? "View Emails" : "Add Emails"}}</div >' +
                    '<div class="modal-body">' +
                    '<input type="text" ui-jq="tagsinput" ng-model="notificationData.receiving_email" class="form-control" />' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<button class="btn btn-primary pull-left" ng-click="addEmail()">Add E-mail</button>' +
                    '<button class="btn btn-default pull-right" ng-click="cancel()">Cancel</button>' +
                    '</div>',
                controller: ['$scope', '$uibModalInstance', function($scope, $modalInstance) {
                    $scope.addEmail = function() {
                        $scope.updateCustomer('notification');
                        $modalInstance.close();
                    };
                    $scope.cancel = function() {
                        $modalInstance.close();
                    }
                }]
            });
        }
    }
]);
