app.controller("InventoryTransferController", [
  "$scope",
  "$stateParams",
  "$cookies",
  "$uibModal",
  "$compile",
  "$state",
  "$log",
  "$filter",
  "$http",
  "UserProfile",
  "warehouseList",
  "InventoryService",
  "LocationService",
  "$timeout",
  "notify",
  "AlertService",
  function(
    $scope,
    $stateParams,
    $cookies,
    $modal,
    $compile,
    $state,
    $log,
    $filter,
    $http,
    UserProfile,
    warehouseList,
    InventoryService,
    LocationService,
    $timeout,
    notify,
    AlertService
  ) {
    $scope.vm = {};
    $scope.vs= {};
    $scope.vs.data={};
    $scope.quick_transfer = {};
    $scope.locationList = [];
    $scope.warehouseList = warehouseList;
    $scope.locationTypes = [
      {
        value: 1,
        name: "Pallet"
      },
      {
        value: 2,
        name: "Shelf"
      }
    ];
    $scope.showSelection = true;
    $scope.showFromLocation = false;
    $scope.showItemSelection = false;
    $scope.showQnty = false;
    $scope.showToLocation = false;
    $scope.showSuccess = false;
    $scope.takesku = false;
    $scope.takeloc = true;
    $scope.scanStart = true;
    $scope.showResult = false;
    $scope.takeFromloc = false;
    $scope.scanNext = function(val) {
      if (val == 1) {
        console.log("idddd:", $scope.vm.data.location_id);
        if ($scope.vm.data.location_id == undefined) {
          AlertService.showErrorNotification("Invalid location ID !!..");
        } else {
          $scope.takesku = true;
          $scope.vm.data.from_location_id=$scope.vm.data.location_id;
          $scope.takeloc = false;
          $scope.vm.data.item_id=$scope.vm.data.sku
        }
      } else {
        if ($scope.vm.data.to_location_id == undefined) {
          AlertService.showErrorNotification("Invalid location ID !!..");
        } else {
          $scope.showRemark = true;
          $scope.showfloc = false;
          $scope.vm.data.from_location_id=$scope.vm.data.location_id;
          $scope.vm.data.item_id=$scope.vm.data.sku
        }
      }
    };
    var quantity = 0;
    $scope.checkboxselection = function(val, oid,qty,rid) {
      console.log("data successfully recived:",val, oid, qty, rid);

      if (document.getElementById(val).checked == true) {
        $scope.vm.data.receive_id = "";
        $scope.vm.data.reciving_itemsTable_id="";
        quantity = 0;
        document.getElementById(val).checked = false;
      } else {
        document.getElementById(val).checked = true;
        $scope.vm.data.receive_id = oid;
        $scope.vm.data.reciving_itemsTable_id=rid;
        quantity = qty;
      }
      console.log($scope.vm.data.id);
    };
    $scope.check4Qty = function() {
      console.log($scope.vm.data.quantity, quantity);
      if ($scope.vm.data.quantity > quantity) {
        AlertService.showErrorNotification(
          "Required quantity is not available in the location"
        );
      } else {
        $scope.showfloc = true;
        $scope.takeQuantity = false;
      }
    };

    $scope.chack4AdditionalField = function() {
      $scope.vm.data.api_key = UserProfile.getApiKey();
      console.log($scope.vm.data);
      $http
        .post(__env.apiUrl + "/inventory-search-location-sku", $scope.vm.data)
        .then(function(res) {
          if (res.data.error) {
            AlertService.showErrorNotification(
              $scope.vm.data.sku +
                " not found in the given location . Please try again"
            );
            $scope.takesku = false;
            $scope.takeloc = true;
          } else if (!res.data.error) {
            $scope.scanStart = false;
            $scope.showResult = true;
            // console.log("koooi", res.data.data);
            $scope.itemList = res.data.data;
          }
        });
    };
    $scope.ScanTo = function() {
      $scope.showResult = false;
      $scope.takeFromloc = true;
      $scope.takeQuantity = true;
    };

    $scope.submitTransfer = function() {
      // $scope.vm.data.warehouse_id = $cookies.getObject("selectedWarehouse").warehouse_id;
      $scope.vs.data.type = 1;
      $scope.vs.data.remark =$scope.vm.data.remark ? $scope.vm.data.remark : "";
      $scope.vs.data.from_location_id=$scope.vm.data.from_location_id
      $scope.vs.data.to_location_id=$scope.vm.data.to_location_id
      $scope.vs.receive_id=$scope.vm.data.receive_id;
      $scope.vs.data.quantity=$scope.vm.data.quantity;
      $scope.vs.data.item_id=$scope.vm.data.sku;
      $scope.vs.data.receive_id=$scope.vm.data.reciving_itemsTable_id;
      console.log("sending data",$scope.vs);
      InventoryService.initiateTransfer($scope.vs).then(response => {
        $log.debug("Initiate Transfer Response", response);
        if (response.data.error) {
          if (response.data.data.indexOf("quantity") !== -1) {
            // Entered quantity is greater
            AlertService.showErrorAlert(
              "Warning",
              "Entered quantity is greater than the actual quantity of item currently placed in the location. Please try again with a different quantity"
            );
          } else if (response.data.data.indexOf("Item not found") != -1) {
            // Item Not found in location
            AlertService.showErrorAlert(
              "Warning",
              "Entered item is not found in given location. Please try again"
            );
          }
        } else {
          notify({
            message: "Items have been transfered successfully",
            templateUrl: "partials/notification-success.html",
            position: "right",
            duration: 2000
          });
          $timeout(function() {
            $state.go(
              $state.current,
              {},
              {
                reload: true
              }
            );
          }, 2000);
        }
      });
    };
    $scope.startPoint = true;
    $scope.$watch("quick_transfer.data.type", function(val) {
      if (!isNaN(val)) {
        $scope.showSelection = false;
        $scope.showFromLocation = true;
        if (val == 1) {
          $scope.startPoint=false;
        } else {$scope.quickTransferButtonText = "Select To Location";$scope.startPoint=false;}
      }
    });

    $scope.getLocationName = function(val) {
      return $http
        .post(__env.apiUrl + "/search-locationname", {
          api_key: UserProfile.getApiKey(),
          location_name: val
        })
        .then(function(response) {
          $log.debug("Response Location Search ", response);
          if (!response.data.error) return response.data.data;
        });
    };

    angular.element("#quick-transfer-from").focus();
    console.log(
      "Ele",
      angular.element(document.querySelector("#quick-transfer-from"))
    );
    // document.querySelector('#quick-transfer-from').focus()
    $scope.transfer = function(isQuickTransfer) {
      if ($scope.showFromLocation) {
        $scope.showFromLocation = false;
        if ($scope.quick_transfer.data.type == 1) {
          $scope.showItemSelection = true;
          $scope.quickTransferButtonText = "Input Quantity";
        } else {
          $scope.showToLocation = true;
          $scope.quickTransferButtonText = "Transfer";
        }
      } else if ($scope.showItemSelection) {
        $scope.showItemSelection = false;
        $scope.showQnty = true;
        $scope.quickTransferButtonText = "Select To Location";
      } else if ($scope.showQnty) {
        $scope.showQnty = false;
        $scope.showToLocation = true;
        $scope.quickTransferButtonText = "Transfer";
      } else if ($scope.showToLocation) {
        $scope.showFromLocation = false;
        $scope.showToLocation = false;
        $scope.showSelection = true;
        let formData;
        if (isQuickTransfer) {
          // $scope.quick_transfer.data.warehouse_id = $cookies.getObject("selectedWarehouse").warehouse_id;
          formData = $scope.quick_transfer;
        } else {
          // $scope.vm.data.warehouse_id = $cookies.getObject("selectedWarehouse").warehouse_id;
          console.log($scope.vm)
          $scope.vs.data.from_location_id=$scope.vm.data.location_id;
          $scope.vs.data.remark =$scope.vm.data.remark ? $scope.vm.data.remark : "";
          $scope.vs.data.from_location_id=$scope.vm.data.from_location_id
          $scope.vs.data.to_location_id=$scope.vm.data.to_location_id
          $scope.vs.receive_id=$scope.vm.data.receive_id;
          $scope.vs.data.quantity=$scope.vm.data.quantity;
          $scope.vs.data.item_id=$scope.vm.data.sku;
          formData = $scope.vs;
        }
        // formData.type = 1;
        $log.debug("Inventory Transfer Post Data", formData);
        InventoryService.initiateTransfer(formData).then(
          response => {
            $log.debug("Quick Transfer Response", response);
            if (response.data.error) {
              if (response.data.data.indexOf("quantity") !== -1) {
                // Entered quantity is greater
                AlertService.showErrorAlert(
                  "Warning",
                  "Entered quantity is greater than the actual quantity of item currently placed in the location. Please try again with a different quantity"
                );
                $state.go(
                  $state.current,
                  {},
                  {
                    reload: true
                  }
                );
              } else if (response.data.data.indexOf("Item not found") != -1) {
                // Item Not found in location
                AlertService.showErrorAlert(
                  "Warning",
                  "Entered item is not found in given location. Please try again"
                );
                $state.go(
                  $state.current,
                  {},
                  {
                    reload: true
                  }
                );
              } else if (
                response.data.data.indexOf("from location not found") != -1
              ) {
                // From location is not found
                if ($scope.quick_transfer.data.type == 1) {
                  AlertService.showErrorAlert(
                    "Warning",
                    "Specified from location is not found in the system. Please check again"
                  );
                  $state.go(
                    $state.current,
                    {},
                    {
                      reload: true
                    }
                  );
                } else {
                  AlertService.showErrorAlert(
                    "Warning",
                    "Specified Pallet Id is not found in the system. Please check again"
                  );
                  $state.go(
                    $state.current,
                    {},
                    {
                      reload: true
                    }
                  );
                }
              } else if (response.data.data.indexOf("Already occupied") != -1) {
                AlertService.showErrorAlert(
                  "Warning",
                  "Entered to location already contain either a pallet or item. Please check again"
                );
                $state.go(
                  $state.current,
                  {},
                  {
                    reload: true
                  }
                );
              }
            } else {
              notify({
                message: "Items have been transfered successfully",
                templateUrl: "partials/notification-success.html",
                position: "right",
                duration: 2000
              });
              $timeout(function() {
                $scope.quickTransferButtonText = "Select Quantity";

                // $scope.quick_transfer = '';
                // $scope.showToLocation = false;
                // $scope.showFromLocation = true;
                // $scope.showSuccess = false;
                $state.go(
                  $state.current,
                  {},
                  {
                    reload: true
                  }
                );
                // angular.element(document).find('quick-transfer-from').focus();
              }, 2000);
            }
          },
          function(error) {
            AlertService.showErrorAlert(
              "Error",
              "An unexpected error has occured while transfering please try again"
            );
            $state.go(
              $state.current,
              {},
              {
                reload: true
              }
            );
          }
        );
      }
    };

    $scope.dataTableOpts = {
      dom: "lrftip",
      serverSide: true,
      deferLoading: 0,
      ajax: {
        url: __env.apiUrl + "/inventory-list",
        type: "POST",
        data: function(d) {
          // d.warehouse_id = $cookies.getObject("selectedWarehouse").warehouse_id,
          d.api_key = UserProfile.getApiKey();
          (d.user_id = $scope.search.user_id || ""),
            (d.location_name = $scope.search.location_name || ""),
            (d.from_date =
              $filter("date")(
                $scope.search.from_date,
                "yyyy-MM-dd HH:mm:ss "
              ) || ""),
            (d.to_date =
              $filter("date")($scope.search.to_date, "yyyy-MM-dd HH:mm:ss ") ||
              "");
        }
      },
      columnDefs: [
        {
          targets: [3],
          render: function(data, type, full) {
            if (full[6] != "0") {
              return (
                '<span  class="label label-success" popover-class="info animated fadeIn " uib-popover="click to get items in the pallet" popover-placement="top" popover-title="Pallet Movement" popover-trigger="mouseenter"  ng-click="getData(' +
                full[6] +
                ')">' +
                full[6] +
                " </span>"
              );
            } else {
              // console.log()
              return data;
            }
          }
        },
        {
          targets: [4],
          className: "col-lg-2 col-md-2 col-sm-2",
          render: function(data, full, type) {
            return $filter("date")(new Date(data), "longDate");
          }
        }
      ],
      rowCallback: function(row, data, index) {
        $compile(row)($scope);
        return row;
      }
    };
    $scope.getData = function(data) {
      $modal.open({
        backdrop: "static",
        templateUrl: "partials/show-pallet.html",
        controller: [
          "$scope",
          "$uibModalInstance",
          function($scope, $modalInstance) {
            console.log(data);
            var vm = {};
            $scope.popupdata;
            var sku;
            vm.api_key = UserProfile.getApiKey();
            vm.pallet_id = data;
            $http
              .post(__env.apiUrl + "/get_barcode_by_palletid", vm)
              .then(function(res) {
                if (res.data) {
                  $scope.popupdata = res.data.data;
                } else {
                  console.log(res);
                }
                //  console.log($scope.popupdata);
              });
            $scope.cancel = function() {
              $modalInstance.close();
            };
          }
        ]
      });
    };
    $scope.fromDateOptions = {
      maxDate: new Date(new Date().getFullYear() + 100, new Date().getMonth())
    };

    $scope.$watch("search.from_date", function(date) {
      if (!isNaN(date))
        $scope.toDateOptions = {
          maxDate: new Date(
            new Date().getFullYear() + 100,
            new Date().getMonth()
          ),
          minDate: new Date(
            date.getFullYear(),
            date.getMonth(),
            date.getDate() + 1
          )
        };
    });

    LocationService.listLocation({
      // warehouse_id: $cookies.getObject("selectedWarehouse").warehouse_id
    }).then(function(response) {
      $scope.transferLocationList = response.data.data;
      $scope.locationList = response.data.data;
    });

    $scope.listLocations = function(section) {
      var postData = {};
      if (section == 0) postData.warehouse_id = $scope.vm.data.warehouse_id;
      else postData.warehouse_id = $scope.search.warehouse_id;
    };

    $scope.getItemCodes = function(val) {
      return $http
        .post(__env.apiUrl + "/purchase-searchsku", {
          api_key: UserProfile.getApiKey(),
          sku: val
        })
        .then(function(response) {
          $log.debug("Response SKU Search ", response);
          if (!response.data.error) return response.data.data;
        });
    };
    $scope.listMovements = function() {
      if ($scope.search.location_name)
        $scope.search.location_name = $scope.search.location_name.replace(
          /^(\d{2})(\d{2})(\d{2})(\d{2})/,
          "$1-$2-$3-$4"
        );
      else $scope.search.location_name = "";
      if ($scope.search.from_date && $scope.search.to_date) {
        $scope.search.to_date.setHours(23);
        $scope.search.to_date.setMinutes(59);
      }

      $scope.search.api_key = UserProfile.getApiKey;
      $scope.search.user_id = UserProfile.getUserProfile().id;
      $("#inventory")
        .DataTable()
        .ajax.reload();
    };
  }
]);

/**
 *Controller for Inventory Live
 */

app.controller("InventoryLiveController", [
  "$scope",
  "$uibModal",
  "$compile",
  "$state",
  "$log",
  "$filter",
  "$http",
  "$cookies",
  "UserProfile",
  "warehouseList",
  "InventoryService",
  function(
    $scope,
    $modal,
    $compile,
    $state,
    $log,
    $filter,
    $http,
    $cookies,
    UserProfile,
    warehouseList,
    InventoryService
  ) {
    console.log("list", warehouseList);
    $scope.vm = {};
    $scope.loading = false;
    // $scope.warehouseList = warehouseList;
    $scope.vm.location_type = 1;

    $scope.dataTableOpts = {
      dom: "lrtp",
      serverSide: true,
      ajax: {
        url: __env.apiUrl + "/inventory-viewlive",
        type: "POST",
        data: function(d) {
          // d.warehouse_id = $cookies.getObject("selectedWarehouse").warehouse_id,
          (d.location_type = $scope.vm.location_type),
            (d.api_key = UserProfile.getApiKey());
        }
      },
      columnDefs: [
        {
          targets: [2],
          render: function(data, full, type) {
            if (data) return data;
            else return '<span class="label label-danger ">Not Provided</span>';
          }
        },
        {
          targets: [4],
          orderable: false,
          render: function(data, full, type) {
            data = new Date(data);
            return $filter("date")(data, "longDate");
          }
        }
      ],
      rowCallback: function(row, data, index) {
        $compile(row)($scope);
        return row;
      }
    };
  }
]);

/**
 *Controller for inventory adjustments
 */
app.controller("InventoryAdjustmentController", [
  "$scope",
  "$stateParams",
  "$uibModal",
  "$compile",
  "$state",
  "$log",
  "$filter",
  "$http",
  "$cookies",
  "UserProfile",
  "InventoryService",
  "LocationService",
  "AlertService",
  function(
    $scope,
    $stateParams,
    $modal,
    $compile,
    $state,
    $log,
    $filter,
    $http,
    $cookies,
    UserProfile,
    InventoryService,
    LocationService,
    AlertService
  ) {
    $scope.vm = {};
    $scope.vm.search = {};
    $scope.vm.search.location_name = "";
    $scope.vm.search.from_date = "";
    $scope.vm.search.to_date = "";
    $scope.vm.search.user_id = "";
    $scope.locationList = [];

    // Get location name list from server
    $scope.getLocationName = function(val) {
      return $http
        .post(__env.apiUrl + "/search-locationname", {
          api_key: UserProfile.getApiKey(),
          location_name: val
        })
        .then(function(response) {
          $log.debug("Response Location Search ", response);
          if (!response.data.error) return response.data.data;
        });
    };

    // Get Location list from server
    $scope.getLocationList = function() {
      LocationService.getAllocatedLocationList(
        $scope.vm.location_type
      ).then(function(response) {
        if (!response.data.error) $scope.locationList = response.data.data;
      });
    };

    // Datatable options for adjustment listing
    $scope.dataTableOpts = {
      dom: "lrftip",
      serverSide: true,
      ajax: {
        url: __env.apiUrl + "/inventory-adjustmentlist",
        type: "POST",
        data: function(d) {
          d.api_key = UserProfile.getApiKey();
        }
      },
      columnDefs: [
        {
          targets: [3],
          className: "col-lg-2 col-md-2 col-sm-2",
          render: function(data, full, type) {
            return $filter("date")(new Date(data), "longDate");
          }
        }
      ],
      rowCallback: function(row, data, index) {
        $compile(row)($scope);
        return row;
      }
    };

    $scope.fetchItemData = function(val) {
      $scope.locationName = val;
      $http
        .post(__env.apiUrl + "/getItemDetailsByLocation", {
          // warehouse_id: $cookies.getObject("selectedWarehouse").warehouse_id,
          // location_type: $scope.vm.location_type,
          location_id: val,
          api_key: UserProfile.getApiKey()
        })
        .then(function(response) {
          if (!response.data.error) $scope.itemData = response.data.data;
          else $scope.itemData = "";
        });
    };

    $scope.updateItem = function(itemId, sku, location_id, q) {
      // console.log("location id:",location_id);
      console.log(itemId, sku, location_id, q);

      var formData = {};
      // console.log("QNTY", $scope.vm.qnty);
      formData.item_id = sku;
      formData.quantity = $scope.vm.qnty[itemId];
      formData.added_by = UserProfile.getUserProfile().id;
      formData.api_key = UserProfile.getApiKey();
      formData.location_id = location_id;
      console.log("data", formData);
      $http
        .post(__env.apiUrl + "/inventory-adjustmentadd", formData)
        .then(function(response) {
          if (!response.error) {
            AlertService.showSuccessAlert(
              "Item Quantity Updated Successfully",
              1
            );
          } else AlertService.showSuccessAlert("Sorry, Something Worng", 1);
        });
    };
    $scope.flag = true;
    $scope.change = function() {
      $scope.flag = false;
    };

    // $scope.listAdjustment = function() {
    //     $('#adjustment').DataTable().ajax.reload(function(data) { console.log(data); });
    // }
  }
]);

app.controller("InventoryOwnershipController", [
  "$scope",
  "$uibModal",
  "$compile",
  "$state",
  "$log",
  "$filter",
  "$http",
  "$cookies",
  "UserProfile",
  "warehouseList",
  "InventoryService",
  function(
    $scope,
    $modal,
    $compile,
    $state,
    $log,
    $filter,
    $http,
    $cookies,
    UserProfile,
    warehouseList,
    InventoryService
  ) {
    $scope.vm = {};
    $scope.loading = false;
    $scope.vm.location_type = 1;
    $scope.dataTableOpts = {
      dom: "lrtp",
      serverSide: true,
      ajax: {
        url: __env.apiUrl + "/inventory-ownership_transfer",
        type: "POST",
        data: function(d) {
          // console.log('OwnerShip Id', $cookies.getObject("selectedWarehouse").warehouse_id);
          // d.warehouse_id = es.getObject("selectedWarehouse").warehouse_id,
          (d.location_type = $scope.vm.location_type),
            (d.api_key = UserProfile.getApiKey());
        }
      },
      columnDefs: [
        {
          targets: [3],
          render: function(data, full, type) {
            if (data) {
              data = new Date(data);
              return $filter("date")(data, "longDate");
            } else {
              return "";
            }
          }
        },
        {
          targets: [4],
          orderable: false,
          className: "col-lg-3 col-md-3",
          render: function(data, full, type) {
            return (
              '<span class="btn-primary btn-xs cursor-pointer" ng-click="listItem(' +
              data +
              ')"><i class="fa fa-pencil"></i> Change Ownership</span>'
            );
          }
        }
      ],
      rowCallback: function(row, data, index) {
        $compile(row)($scope);
        return row;
      }
    };

    $scope.reloadData = function() {
      $scope.loading = true;
      $("#ownership")
        .DataTable()
        .ajax.reload();
    };
  }
]);

app.controller("InventoryCycleCountController", [
  "$scope",
  "$uibModal",
  "$compile",
  "$state",
  "$log",
  "$filter",
  "$http",
  "$cookies",
  "UserProfile",
  "showCount",
  "InventoryService",
  function(
    $scope,
    $modal,
    $compile,
    $state,
    $log,
    $filter,
    $http,
    $cookies,
    UserProfile,
    showCount,
    InventoryService
  ) {
    $scope.vm = {};
    $scope.loading = false;
    $scope.showCount = showCount;
    $scope.vm.location_type = 1;
    $scope.dataTableOpts = {
      dom: "lrtp",
      serverSide: true,
      ajax: {
        url: __env.apiUrl + "/inventory-viewlive",
        type: "POST",
        data: function(d) {
          // d.warehouse_id = $cookies.getObject("selectedWarehouse").warehouse_id,
          (d.location_type = $scope.vm.location_type),
            (d.api_key = UserProfile.getApiKey());
        }
      },
      columnDefs: [
        {
          targets: [4],
          className: "col-lg-2 col-md-2 col-sm-2",
          orderable: false,
          render: function(data, full, type) {
            data = new Date(data);
            return $filter("date")(data, "longDate");
          }
        }
      ],
      rowCallback: function(row, data, index) {
        $compile(row)($scope);
        return row;
      }
    };
  }
]);

app.controller("ItemRecieveStartCntrl", [
  "$scope",
  "$http",
  "$log",
  "$state",
  "$filter",
  "UserProfile",
  "$parse",
  "InventoryService",
  "AlertService",
  "warehouseList",
  "id_number",
  "notify",
  function(
    $scope,
    $http,
    $log,
    $state,
    $filter,
    UserProfile,
    $parse,
    InventoryService,
    AlertService,
    warehouseList,
    id_number,
    notify
  ) {
    console.log(warehouseList, id_number);
    $scope.id_number = id_number;
    $scope.showSelection = true;
    $scope.showFull = false;
    $scope.showPartial = false;
    $scope.next = false;
    $scope.$watch(
      "receiveOpts",
      function(val) {
        // console.log(val, "hai");
        if (!isNaN(val)) {
          $scope.showSelection = false;
          if (val == 1) {
            $scope.showPartial = true;
          } else if (val == 2) {
            $scope.showFull = true;
          }
        }
      },
      true
    );

    // $scope.test = function(){console.log($scope)}
    $scope.files = {};
    $scope.vm = {};
    $scope.vm.data = {};
    $scope.vm.data.re_boxing = false;
    $scope.vm.data.re_palletizing = false;
    $scope.vm.data.packaging = false;
    $scope.showQntyError = false;
    $scope.vm.data.time = false;
    $scope.vm.close_po = 0;
    $scope.warehouseList = warehouseList;
    $scope.vm.data.po_number = id_number;
    $scope.receiveByOptions = [
      {
        value: 1,
        name: "Close Out"
      },
      {
        value: 2,
        name: "Over Recieve"
      },
      {
        value: 3,
        name: "Receive In Full"
      }
    ];
    $scope.labelOptions = [
      {
        value: 1,
        name: "Box Label"
      },
      {
        value: 2,
        name: "Pallet Label"
      },
      {
        value: 3,
        name: "  Item Labels"
      }
    ];
    $scope.vm.item_data = [];
    $scope.vm.item_all_data = [];
    $scope.quantityArray = [];
    $scope.cancel = function() {
      $state.go("app.inventory.receving");
    };

    $scope.$watch(
      "isClosePo",
      function(newVal) {
        if (!newVal) $scope.vm.close_po = 0;
      },
      true
    );
    $scope.sendData = function() {
      $scope.showQntyError = false;
      var countDiff = false;
      var closePo = false;
      for (var i = 0; i < $scope.vm.item_all_data.length; i++) {
        var diff =
          $scope.itemData[i].total_quantity -
          $scope.itemData[i].received_quantity;
        if (!$scope.vm.item_all_data[i].quantity) {
          $scope.vm.item_all_data[i].quantity = 0;
        }
        if ($scope.vm.item_all_data[i].quantity > diff) {
          countDiff = true;
          break;
        } else countDiff = false;
      }
      for (var i = 0; i < $scope.vm.item_all_data.length; i++) {
        var totalDiff =
          parseInt($scope.itemData[i].total_quantity) -
          parseInt($scope.vm.item_all_data[i].quantity) -
          parseInt($scope.itemData[i].received_quantity);
        if (totalDiff == 0) {
          closePo = true;
        } else {
          closePo = false;
          break;
        }
      }
      if (countDiff) {
        $scope.showQntyError = true;
      } else {
        // Entered Quantity is not greater than expected
        if (closePo) $scope.vm.close_po = 3;
        if ($scope.files.bol) $scope.vm.bill_count = $scope.files.bol.length;
        if ($scope.files.packing_slip)
          $scope.vm.packing_count = $scope.files.packing_slip.length;
        if ($scope.files.qc_form)
          $scope.vm.quality_count = $scope.files.qc_form.length;
        if ($scope.files.pictures)
          $scope.vm.pictures_count = $scope.files.pictures.length;

        for (var i = 1; i <= $scope.vm.bill_count; i++) {
          var model = $parse("bill_" + i);
          model.assign($scope.vm, $scope.files.bol[i - 1]);
        }

        for (var i = 1; i <= $scope.vm.packing_count; i++) {
          var model = $parse("packing_" + i);
          model.assign($scope.vm, $scope.files.packing_slip[i - 1]);
        }

        for (var i = 1; i <= $scope.vm.quality_count; i++) {
          var model = $parse("quality_" + i);
          model.assign($scope.vm, $scope.files.qc_form[i - 1]);
        }

        for (var i = 1; i <= $scope.vm.pictures_count; i++) {
          var model = $parse("pictures_" + i);
          model.assign($scope.vm, $scope.files.pictures[i - 1]);
        }

        $scope.vm.generate_palletid =
          $scope.vm.generate_palletid == true ? 1 : 0;
        $scope.vm.sku = "";
        $scope.vm.quantity = "";

        angular.forEach($scope.itemData, function(value, key) {
          console.log("Key ", key);
          console.log("Value ", value);
          console.log("qnt array", $scope.quantityArray);
          $scope.vm.sku += value.sku;
          $scope.vm.quantity += $scope.quantityArray[key];
          if (key != $scope.itemData.length - 1) {
            $scope.vm.sku += ",";
            $scope.vm.quantity += ",";
          }
          $scope.vm.item_data[key].quantity = $scope.quantityArray[key];
        });
        console.log("&&&&&&&&&&&7",$scope.vm);
        InventoryService.receive($scope.vm).then(

          function(response) {
            $log.debug("Item Receive Response ", response);
            if (response.data.error)
              AlertService.showErrorNotification(
                "Sorry!! Something went wrong. "
              );
            else {
              AlertService.showSuccessNotification(
                "Items have been received successfully"
              );
              $state.go(
                $state.current,
                {},
                {
                  reload: true
                }
              );

              if ($scope.vm.generate_palletid) {
                $modal.open({
                  backdrop: "static",
                  templateUrl: "partials/pallet-id-print.html",
                  controller: [
                    "$scope",
                    "$uibModalInstance",
                    function($scope, $modalInstance) {
                      $scope.responseData = response.data.data;
                      for (var i = 0; i < response.data.data.length; i++) {
                        $scope.responseData[i].dateCreated = new Date(
                          response.data.data[i].created_at
                        );
                      }
                      $scope.options = {
                        width: 2,
                        height: 50,
                        quite: 10,
                        displayValue: false,
                        font: "monospace",
                        textAlign: "center",
                        fontSize: 12,
                        backgroundColor: "",
                        lineColor: "#000"
                      };
                      $scope.cancel = function() {
                        $modalInstance.close();
                      };
                    }
                  ]
                });
              }
            }
            // AlertService.showSuccessAlert('Item has been received successfully', 3);
          },
          function(response) {
            AlertService.showErrorNotification(
              "Sorry!! Something went wrong. "
            );
          }
        );
      }
    };
    $scope.printBoxLabel = function(item) {
      console.log("Item ===== ", item);
      var printContent = "";
      var winAttr =
        "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";
      var popupWin = window.open("", "_blank", winAttr);
      printContent += '<div style="border-radius: 25px;background: #FFF;">';
      printContent += '<table align="center" style="text-align:center;">';
      printContent +=
        '<tr><td colspan="3"><img src="../img/drive_logo.png"></td></tr>';
      printContent +=
        "<tr><td>" +
        $scope.vm.data.po_number +
        "</td><td></td><td>Date: " +
        $filter("date")(new Date(), "MM/dd/yyyy") +
        "</td></tr>";
      printContent += '<tr><td colspan="3">' + item.address + "</td></tr>";
      printContent += '<tr><td colspan="3"><hr></td></tr>';
      printContent += '<tr><td colspan="3">SKU# ' + item.sku + "</td></tr>";
      printContent += '<tr><td colspan="3"><hr></td></tr>';
      printContent += '<tr><td colspan="3">UPC#' + item.upc + "</td></tr>";
      printContent += '<tr><td colspan="3"><hr></td></tr>';
      printContent += '<tr><td colspan="3">QTY#' + item.quantity + "</td></tr>";
      printContent += "</table></div>";

      popupWin.document.open();
      popupWin.document.write(
        "<html><head>" +
          '<style type="text/css">@media print {body { -webkit-print-color-adjust: exact;  -webkit-align-content: center; align-content: center; } }</style></head > <body onload="window.print();window.close();">' +
          printContent +
          "</body></html > "
      );
      popupWin.document.close();
      popupWin.focus();
    };
    $scope.printItemLabel = function(item) {
      console.log("Print Item", item);
      var printContent = "";
      var winAttr =
        "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=10, height=auto, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";
      var popupWin = window.open("", "_blank", winAttr);
      printContent +=
        '<div style="border-radius: 25px;background: #FFF;"><table align= "center" style="text-align:center;"><tr><td>' +
        item.address +
        "</td></tr > ";
      printContent += "<tr><td>SKU# " + item.sku + "</td></tr>";
      printContent += "<tr><td>" + item.barcode + "</td></tr>";
      printContent += "<tr><td>234234234234234234</td></tr></table></div>";

      popupWin.document.open();
      popupWin.document.write(
        '<html><head><style type="text/css">@media print { body { -webkit-print-color-adjust: exact;  -webkit-align-content: center; align-content: center; background-color: #bfd3ea !important; } }</style></head><body onload="window.print();window.close();">' +
          printContent +
          "</body></html>"
      );
      popupWin.document.close();
      popupWin.focus();
    };

    $scope.getItemDetails = function() {
      console.log("Number  ===== ", $scope.vm.data.po_number);
      if ($scope.vm.data.po_number)
        $http
          .post(__env.apiUrl + "/getItemDetailsByponumber", {
            po_number: $scope.vm.data.po_number,
            api_key: UserProfile.getApiKey()
          })
          .then(function(response) {
            $log.debug("Get Item Details Response Data ", response);
            if (response.data.error) {
            } else {
              $scope.itemData = response.data.data.item_details;
              angular.forEach($scope.itemData, function(value, key) {
                console.log("val", value);
                $scope.vm.item_all_data[key] = {};
                $scope.vm.item_all_data[key].sku = value.sku;
                var qntyDiff = value.total_quantity - value.received_quantity;
                console.log("Diff ", qntyDiff);
                var _data = {};
                _data.id = value.id;
                _data.quantity = 0;
                _data.lot = "";
                _data.expiry_date = "";
                _data.serial_number = "";
                _data.rfid_number = "";
                $scope.vm.item_data.push(_data);
                if (value.lot_flag == 1 && qntyDiff != 0) $scope.showLot = true;
                if (value.expiration_date_flag == 1 && qntyDiff != 0)
                  $scope.showExpirationDate = true;
                if (value.serial_number_flag == 1 && qntyDiff != 0) {
                  $scope.showSerialNo = true;
                }
                if (value.rfid_number_flag == 1 && qntyDiff != 0)
                  $scope.showRfid = true;
              });
              $log.debug("Item Data Array ", $scope.itemData);
              $scope.currentWh=$filter("filter")($scope.warehouseList, function(value) {
                if (value.warehouse_id == $scope.itemData[0].warehouse_id) return value;
              });
              // console.log("$scope.currentWhPlan",$scope.currentWhPlan)
            }
          });
    };
    $scope.nextData = function() {
      //  console.log($scope.vm.item_all_data,"hai")
      $scope.skuQuantityArray = [];
      $scope.cellAray = [];
      $scope.cellAray2 = [];
      console.log("val:", $scope.itemData);
      console.log("allData:", $scope.vm.item_all_data);
      $scope.vm.item_all_data.forEach(function(val, index) {
        var a =
          $scope.itemData[index].total_quantity -
          $scope.itemData[index].received_quantity;
        console.log("tot", a);
        if (val.quantityPerPallet) {
          var x = a % val.quantityPerPallet;
          var y = a / val.quantityPerPallet;
          if (x == 0) {
            console.log("x:", x, "y:", y, "ceil(y)", Math.ceil(y));
            for (i = 0; i < Math.ceil(y); i++) {
              $scope.cellAray.push({
                eachArray: val.quantityPerPallet
              });
            }
          }
          if (x != 0) {
            for (i = 1; i < Math.ceil(y); i++) {
              $scope.cellAray.push({
                eachArray: val.quantityPerPallet
              });
            }
            $scope.cellAray.push({
              eachArray: x
            });
          }
          $scope.skuQuantityArray.push({
            address: $scope.itemData[index].address,
            id: $scope.itemData[index].id,
            quantity: val.quantityPerPallet,
            item_details: $scope.itemData[index],
            sku: val.sku,
            childTable: $scope.cellAray
          });
          console.log("skuQuantityArray:", $scope.skuQuantityArray);
          $scope.cellAray2 = $scope.cellAray;
          console.log("cellarray", $scope.cellAray);
          $scope.cellAray = [];
          console.log($scope.itemData[index]);
        }
      });
    };
    if (id_number) {
      $scope.getItemDetails();
    }

    $scope.vn = [];
    $scope.dataArray = [];
    $scope.sendFullData = function() {
      $scope.skuQuantityArray.forEach(function(dat, ind) {
        console.log("sku array", dat);

        dat.childTable.forEach(function(val, index) {
          if (dat.item_details.lot_flag == 1 && $scope.vn[ind][index])
            // $scope.dataArray.push({"lot":$scope.vn[ind][index].lot})
            var lot = $scope.vn[ind][index].lot;
          else var lot = "";
          if (dat.item_details.rfid_number_flag == 1 && $scope.vn[ind][index])
            // $scope.dataArray.push({ "rfid_number":$scope.vn[ind][index].rfid})
            var rfid = $scope.vn[ind][index].rfid;
          else var rfid = "";
          if (dat.item_details.serial_number_flag == 1 && $scope.vn[ind][index])
            // $scope.dataArray.push({ "serial_number":$scope.vn[ind][index].serialNo})
            var sNo = $scope.vn[ind][index].serialNo;
          else var sNo = "";
          if (
            dat.item_details.expiration_date_flag == 1 &&
            $scope.vn[ind][index]
          )
            // $scope.dataArray.push({"expiry_date":$scope.vn[ind][index].exp_date })
            var exp = $scope.vn[ind][index].exp_date;
          else var exp = "";

          $scope.dataArray.push({
            sku: dat.sku,
            quantity: val.eachArray,
            address: dat.address,
            id: dat.id,
            lot: lot,
            rfid_number: rfid,
            serial_number: sNo,
            expiry_date: exp

            // "lot":$scope.vn[ind][index].lot,
            // "serial_number":$scope.vn[ind][index].serialNo,

            // "expiration_date":$scope.vn[ind][index].exp_date
          });
          // console.log("item",$scope.itemData[ind])
          // console.log("ind",$scope.vn[ind][index].lot);
          // console.log("flag",$scope.itemData[index].lot_flag)
        });
      });
      console.log("data array:", $scope.dataArray);
      $scope.showQntyError = false;
      var countDiff = false;
      var closePo = false;
      // for (var i = 0; i < $scope.vm.item_all_data.length; i++) {
      //     var diff = $scope.itemData[i].total_quantity - $scope.itemData[i].received_quantity;
      //     if (!$scope.vm.item_all_data[i].quantity) {
      //         $scope.vm.item_all_data[i].quantity = 0;
      //     }
      //     if ($scope.vm.item_all_data[i].quantity > diff) {
      //         countDiff = true;
      //         break;
      //     } else
      //         countDiff = false;
      // }
      // for (var i = 0; i < $scope.vm.item_all_data.length; i++) {
      //     var totalDiff = parseInt($scope.itemData[i].total_quantity) - parseInt($scope.vm.item_all_data[i].quantity) - parseInt($scope.itemData[i].received_quantity);
      //     if (totalDiff == 0) {
      //         closePo = true;
      //     } else {
      //         closePo = false;
      //         break;
      //     }
      //
      if (countDiff) {
        $scope.showQntyError = true;
      } else {
        // Entered Quantity is not greater than expected
        $scope.vm.close_po = 3;
        if ($scope.files.bol) $scope.vm.bill_count = $scope.files.bol.length;
        if ($scope.files.packing_slip)
          $scope.vm.packing_count = $scope.files.packing_slip.length;
        if ($scope.files.qc_form)
          $scope.vm.quality_count = $scope.files.qc_form.length;
        if ($scope.files.pictures)
          $scope.vm.pictures_count = $scope.files.pictures.length;

        for (var i = 1; i <= $scope.vm.bill_count; i++) {
          var model = $parse("bill_" + i);
          model.assign($scope.vm, $scope.files.bol[i - 1]);
        }

        for (var i = 1; i <= $scope.vm.packing_count; i++) {
          var model = $parse("packing_" + i);
          model.assign($scope.vm, $scope.files.packing_slip[i - 1]);
        }

        for (var i = 1; i <= $scope.vm.quality_count; i++) {
          var model = $parse("quality_" + i);
          model.assign($scope.vm, $scope.files.qc_form[i - 1]);
        }

        for (var i = 1; i <= $scope.vm.pictures_count; i++) {
          var model = $parse("pictures_" + i);
          model.assign($scope.vm, $scope.files.pictures[i - 1]);
        }

        $scope.vm.generate_palletid =
          $scope.vm.generate_palletid == true ? 1 : 0;
        $scope.vm.sku = "";
        $scope.vm.quantity = "";

        $scope.vm.item_all_data = $scope.dataArray;
        $scope.vm.item_data.quantity = "";
        $scope.vm.item_data.sku = "";
        // angular.forEach($scope.itemData, function(value, key) {
        // console.log('Key ', key);
        // console.log('Value ', value);
        // console.log('qnt array', $scope.quantityArray);
        // $scope.vm.sku += value.sku;
        // $scope.vm.quantity += $scope.quantityArray[key];
        //     if (key != $scope.itemData.length - 1) {
        //         $scope.vm.sku += ',';
        //         $scope.vm.quantity += ',';
        //     }
        //     $scope.vm.item_data[key].quantity = $scope.quantityArray[key];
        // });
         console.log("vm.....:", $scope.vm);
        InventoryService.receive($scope.vm).then(
          function(response) {
            $log.debug("Item Receive Response ", response);
            if (response.data.error)
              AlertService.showErrorNotification(
                "Sorry!! Something went wrong. "
              );
            else {
              AlertService.showSuccessNotification(
                "Items have been received successfully"
              );
              $state.go("app.inventory.receving");

              if ($scope.vm.generate_palletid) {
                $modal.open({
                  templateUrl: "partials/pallet-id-print.html",
                  backdrop: "static",
                  controller: [
                    "$scope",
                    "$uibModalInstance",
                    function($scope, $modalInstance) {
                      $scope.responseData = response.data.data;
                      for (var i = 0; i < response.data.data.length; i++) {
                        $scope.responseData[i].dateCreated = new Date(
                          response.data.data[i].created_at
                        );
                      }
                      $scope.options = {
                        width: 2,
                        height: 50,
                        quite: 10,
                        displayValue: false,
                        font: "monospace",
                        textAlign: "center",
                        fontSize: 12,
                        backgroundColor: "",
                        lineColor: "#000"
                      };
                      $scope.cancel = function() {};
                    }
                  ]
                });
              }
            }
            // AlertService.showSuccessAlert('Item has been received successfully', 3);
          },
          function(response) {
            AlertService.showErrorNotification(
              "Sorry!! Something went wrong. "
            );
          }
        );
      }
    };
  }
]);
