app.controller('ASNController', ['$scope', '$stateParams', '$cookies', '$uibModal', '$compile', '$state', '$log', '$filter', 'UserProfile', 'PurchaseOrderService', 'AlertService',
    function($scope, $stateParams, $cookies, $modal, $compile, $state, $log, $filter, UserProfile, PurchaseOrderService, AlertService) {

        $scope.vm = {};

        $scope.dataTableOpt = {
            "processing": true,
            "language": {
                "processing": "<img src='img/loading.gif' width='50px' height='50px'>"
            },
            "serverSide": true,
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            "pagingType": "simple_numbers",

            "ajax": {
                "url": __env.apiUrl + '/purchase-asnlist',
                "type": "POST",
                "data": {
                    api_key: UserProfile.getApiKey(),
                    show_open: 0
                }
            },
            "columnDefs": [{
                    "targets": [0],
                    "className": "col-lg-1 col-md-1 col-sm-1",
                },
                {
                    "targets": [1],
                    "className": "col-lg-2 col-md-2 col-sm-2",
                },
                {
                    "targets": [2],
                    "className": "col-lg-1 col-md-1 col-sm-1",
                    render: function(data, full, type) {
                        if (data == 1) return 'UPS';
                        else if (data == 2) return 'FedEx';
                        else if (data == 3) return 'DHL';
                        else if (data == 4) return 'USPS';
                        else if (data == 5) return 'LTL';
                        else if (data == 6) return 'Pickup';
                        else if (data == 7) return 'Other';
                    }
                },
                {
                    "targets": [3],
                    "className": "col-lg-2 col-md-2 col-sm-2",
                    render: function(data, full, type) {
                        data = new Date(data);
                        return $filter('date')(data, 'longDate');
                    }
                },
                {
                    "targets": [4],
                    // "className": "col-lg-2 col-md-2 col-sm-2",
                    render: function(data, full, type) {
                        var returnString = '<table class="table" frame="vsides">';
                        returnString += '<th>Sku</th><th>Quantity</th>';
                        for (var i = 0; i < data.length; i++) {
                            returnString += '<tr><td>' + data[i].item_id + '</td><td>' + data[i].quantity + '</td></tr>'
                        }
                        returnString += '</table>'
                        return returnString;
                    }
                },
                  {
                      "targets": [5],
                      "sortable": false,
                      "className": "col-lg-1 col-md-1 col-sm-1",
                      render: function (data, type, full) {
                          return '<span class="btn-primary btn-xs cursor-pointer" data-toggle="tooltip" data-placement="left" title="View" ng-click="listAsn(' + data + ')"><i class="fa fa-eye hidden-xs"></i></span> &nbsp' ;
                              // '<span class="btn-success btn-xs cursor-pointer" ng-click="editPo(' + full[0] + ')"><i class="fa fa-pencil hidden-xs"></i> Edit</span>&nbsp' +
                              // '<span class="btn-danger btn-xs cursor-pointer" ng-click="deletePo(' + data + ')"><i class="fa fa-trash hidden-xs"></i> Delete</span> &nbsp';
                      }
                  }

            ],
            "rowCallback": function(row, data, index) {
                $compile(row)($scope);
                return row;

            }
        }

        $scope.createASN = function(id) {
            var modalInstance = $modal.open({
                templateUrl: 'partials/asn-create.html',
                controller: 'ASNModalController',
                size: 'lg',
                resolve: {
                    customerList: function($http, UserProfile) {
                        return $http.post(__env.apiUrl + '/purchase-customerlist', {
                            api_key: UserProfile.getApiKey()
                        }).then(function(response) {
                            console.log("Customer Response", response);
                            return response.data.data;
                        })
                    },
                    warehouseList: function($http, UserProfile) {
                        return $http({
                            method: 'POST',
                            url: __env.apiUrl + '/warehouse-getrights',
                            data: {
                                api_key: UserProfile.getApiKey()
                            }
                        }).then(function(response) {
                            return response.data.data;
                        })
                    },
                }
            });
        }

        $scope.deleteAsn = function(poId) {
            var modalInstance = $modal.open({
                templateUrl: 'partials/alert-warning.html',
                controller: 'WarningModalCtrl',
                resolve: {
                    data: function() {

                      var warning = {};
                        warning.title = 'Delete';
                        warning.body = 'Are you sure you want to delete this Advanced Shipping Notification?';
                        warning.showCancel = true;
                        return warning;
                    }
                }
            });

            modalInstance.result.then(function(selection) {
                // Warning accepted
                PurchaseOrderService.deletePO(poId).then(function(response) {
                    $log.debug("Delete Purchase Order Response ", response);
                    if (!response.data.error)
                        $state.go($state.current, {}, { reload: true });
                    else
                        AlertService.showErrorAlert('Error', 'An error occured while deleting purchase order. Please try again', false);
                }, function(error) { AlertService.showErrorNotification("Sorry!! Something went wrong. ") });
            }, function() {
                //  Warning dismissed
            });
        }

        $scope.editPo = function(poId) {
            var modalInstance = $modal.open({
                templateUrl: 'partials/purchase-order-add.html',
                controller: 'PurchaseOrderModalController',
                resolve: {
                    userList: function($http, UserProfile) {
                        return $http.post(__env.apiUrl + '/item-namelist', { api_key: UserProfile.getApiKey() }).then(function(response) {
                            if (response.data.error)
                              AlertService.showErrorNotification("Sorry!! Something went wrong. ");
                            else
                                return response.data.data;
                        }, function(error) { AlertService.showErrorNotification("Sorry!! Something went wrong. ") })
                    },
                    purchaseData: function(PurchaseOrderService) {
                        return PurchaseOrderService.listPO(poId).then(function(response) {
                            if (response.data.error)
                              AlertService.showErrorNotification("Sorry!! Something went wrong. ");
                            else
                                return response.data.data[0];

                        }, function(error) {AlertService.showErrorNotification("Sorry!! Something went wrong. "); });
                    }
                }
            });
        }
           $scope.listAsn = function(poId) {
            $state.go('app.asnDetail', { poId: poId })
        }
    }
]);

app.controller('ASNModalController', ['$scope', '$log', '$state', 'UserProfile', 'customerList', 'PurchaseOrderService', 'AlertService', '$http', '$filter', '$uibModalInstance', 'warehouseList', function($scope, $log, $state, UserProfile, customerList, PurchaseOrderService, AlertService, $http, $filter, $modalInstance, warehouseList) {

    $scope.customerList = customerList;
    $log.debug("ASN Customer List", customerList);
    $scope.customerData;
    $scope.orderData;
    $scope.vm = {};
    $scope.vm.data = {};
    $scope.vm.item_all_data = [];
    $scope.itemCount = [];
    $scope.itemRowCount = 1;
    $scope.itemCount.push($scope.itemRowCount);
    $scope.totalAmount = 0;
    $scope.itemAmount = [];
    $scope.itemList = [];
    $scope.itemQnty = [];
    $scope.itemPrice = [];
    $scope.warehouseList = warehouseList;
    $scope.warehouseList.push({ warehouse_id: -1, warehouse_name: 'Other' });
    $scope.shippingMethods = [
        { value: 1, name: 'UPS' },
        { value: 2, name: 'FedEx' },
        { value: 3, name: 'DHL' },
        { value: 4, name: 'USPS' },
        { value: 5, name: 'LTL' },
        { value: 6, name: 'Pickup' },
        { value: 7, name: 'Other' }
    ];
    $scope.datepickerOptions = {
        minDate: new Date(),

    };
    $scope.deleteArow = function (id) {

          if ($scope.itemRowCount > 1) {
            $scope.itemCount.splice(id, 1);
            $scope.itemList.splice(id, 1)
            $scope.itemRowCount--;

          }
          $scope.itemAmount.forEach(function (amount) {
            if (!isNaN(amount)){
              $scope.totalAmount=0;
              $scope.totalAmount += amount;}
          });
          console.log($scope)
        }
    $scope.clickItemRow = function(id) {
            if (id == $scope.itemCount.length - 1) {
                $scope.itemRowCount++;
                $scope.itemCount.push($scope.itemRowCount);
                console.log($scope);
            } else {
                $scope.itemCount.splice(id, 1);
                $scope.itemRowCount--;
                $scope.itemAmount.splice(id,1);
                $scope.vm.item_all_data.splice(id, 1);
                $scope.itemList.splice(id, 1);
                console.log($scope)
            }
        }
        // For Auto completing the item sku code
    $scope.getItemCodes = function(val) {
        return $http.post(__env.apiUrl + '/purchase-searchsku', { api_key: UserProfile.getApiKey(), sku: val }).then(function(response) {
            $log.debug('Response SKU Search ', response);
            if (!response.data.error)
                return response.data.data;
        });
    };

    $scope.$watch('itemAmount', function() {
        $scope.totalAmount = 0;
        $scope.itemAmount.forEach(function(amount) { if (!isNaN(amount)) $scope.totalAmount += amount; });
    }, true);

    $scope.$watch('warehouseSelected', function() {
        if ($scope.warehouseSelected && $scope.warehouseSelected.warehouse_id != -1) {
            $scope.vm.data.address_1 = $scope.warehouseSelected.address_1;
            $scope.vm.data.address_2 = $scope.warehouseSelected.address_2;
            $scope.vm.data.city = $scope.warehouseSelected.city;
            $scope.vm.data.zip_code = $scope.warehouseSelected.zip_code;
            $scope.vm.data.warehouse_id = $scope.warehouseSelected.warehouse_id;
        } else if ($scope.warehouseSelected) {
            $scope.vm.data.address_1 = '';
            $scope.vm.data.address_2 = '';
            $scope.vm.data.city = '';
            $scope.vm.data.zip_code = '';
        }
    });

    $scope.updatePurchaseList = function() {
        $scope.orderList = [];
        angular.forEach($scope.customerData.purchase, function(order) {
            if (order.asn == 1 && !order.tracking_number)
                $scope.orderList.push(order);
        })
    }
    $scope.updateFormData = function() {
        $scope.vm.purchase_id = $scope.orderData.id;
        $scope.data.ship_method = $scope.orderData.ship_method
        $scope.data.ship_date = $scope.orderData.ship_date;
        $scope.data.receive_date = $scope.orderData.receive_date;
        $scope.data.sku = $scope.orderData.sku;
    }


    $scope.saveAsn = function() {

        for (var i = 0; i < $scope.itemRowCount; i++) {
            $scope.vm.item_all_data[i].sku = $scope.itemList[i].sku;
        }

        $scope.vm.data.customer_id = $scope.customerData.customer_id;
        $scope.vm.data.created_by = UserProfile.getUserProfile().id;
        $scope.vm.data.receive_date = $filter('date')($scope.vm.data.receive_date, "yyyy-MM-dd HH:mm:ss ");
        $scope.vm.data.ship_date = $filter('date')($scope.vm.data.ship_date, "yyyy-MM-dd HH:mm:ss ");

        $log.debug('ASN Create Post Data ', $scope.vm);
        PurchaseOrderService.createAsn($scope.vm).then(function(response) {
            $log.debug("Create ASN Response ", response);
            if (!response.data.error) {
                $modalInstance.close();
                AlertService.showSuccessNotification('ASN has been created successfully');
                $state.go($state.current, {}, { reload: true });

            }
        }, function() {
            $modalInstance.close();
            AlertService.showErrorAlert('Error', 'An error occured while creating ASN. Please try gain', false);
        })
    }

    $scope.cancel = function() {
        $modalInstance.close();
    }
}]);



app.controller('AsnviewControl', ['$scope', '$log', '$state', '$uibModal', '$http', '$filter', 'editableOptions', 'UserProfile', 'AlertService', 'PurchaseOrderService', 'userList', 'purchaseData', function($scope, $log, $state, $modal, $http, $filter, editableOptions, UserProfile, AlertService, PurchaseOrderService, userList, purchaseData) {

    $scope.itemSelected = true;
    $scope.itemSearchData;
    $scope.vm = {};
    $scope.vm.data = {};
    $scope.purchaseData = purchaseData;
    $scope.purchaseData.receive_date = new Date(purchaseData.receive_date);
    $scope.purchaseData.ship_date = new Date(purchaseData.ship_date);
    $log.debug("User List ", userList);
    $log.debug('Purchase Data ', purchaseData);

    var shippingMethods = [
        { value: 1, name: 'UPS' },
        { value: 2, name: 'FedEx' },
        { value: 3, name: 'DHL' },
        { value: 4, name: 'USPS' },
        { value: 5, name: 'LTL' },
        { value: 6, name: 'Pickup' },
        { value: 7, name: 'Other' }
    ];

    $scope.assign = [{ value: 0, text: 'No', }, { value: 1, text: 'Yes' }]

    $scope.shippingMethod = $filter('filter')(shippingMethods, { value: purchaseData.ship_method })[0];
    if ($scope.shippingMethod.value != 7) {
        $scope.shippingMethod = $scope.shippingMethod.name;
    } else {
        $scope.shippingMethod = $scope.shippingMethod.name + ' , ' + purchaseData.other_method;
    }

    $scope.itemData = purchaseData.item_data;

    angular.forEach($scope.itemData, function(data, index) {
        data.customer_name = $filter('filter')(userList, { customer_id: data.customer_id })[0].customer_name;
        $scope.itemData[index] = data;
    });

    console.log('Item data ==', $scope.itemData);

    $scope.totalAmount = 0;
    for (var i = 0; i < $scope.itemData.length; i++) $scope.totalAmount += ($scope.itemData[i].quantity * $scope.itemData[i].price);

    $scope.dateOptions = {
        formatYear: 'yyyy',
        minDate: new Date(),
        startingDay: 1
    };

    $scope.userData = UserProfile.getUserProfile();

    if ($scope.userData.company_details[0] && $scope.userData.company_details[0].logo)
        $scope.companyLogo = __env.uploadUrl + '/user_companylogo/' + $scope.userData.company_details[0].logo;

    $scope.init = function() {
        if (purchaseData) {
            $scope.vm.data.asn = purchaseData.asn;
            $scope.vm.data.customer_id = purchaseData.customer_id;
            $scope.vm.data.vendor_id = purchaseData.vendor_id;
            $scope.vm.data.quantity = purchaseData.quantity;
            $scope.vm.data.address_1 = purchaseData.address_1;
            $scope.vm.data.address_2 = purchaseData.address_2;
            $scope.vm.data.city = purchaseData.city;
            $scope.vm.data.zip_code = purchaseData.zip_code;
            $scope.vm.data.ship_method = purchaseData.ship_method;
            $scope.vm.data.sku = purchaseData.sku;
            $scope.vm.purchase_id = purchaseData.id;
            $scope.vm.data.ship_date = purchaseData.ship_date;
            $scope.vm.data.receive_date = purchaseData.receive_date;
            $log.debug("Customer Id ", $scope.vm.data);
        }
    }

    $scope.getItemCodes = function(val) {
        return $http.post(__env.apiUrl + '/purchase-searchsku', { api_key: UserProfile.getApiKey(), sku: val }).then(function(response) {
            $log.debug('Response SKU Search ', response);
            if (!response.data.error)
                return response.data.data;
        });
    };

    $scope.updatePO = function() {

        $scope.vm.data.sku = [];
        $scope.vm.data.customer_id = [];
        $scope.vm.data.quantity = [];


        $scope.itemList.forEach(function(_item) { $scope.vm.data.sku.push(_item); });
        $scope.customerData.forEach(function(customer) { $scope.vm.data.customer_id.push(customer.customer_id); });
        $scope.itemQnty.forEach(function(qnty) { $scope.vm.data.quantity.push(qnty); });

        $scope.vm.data.sku = $scope.vm.data.sku.toString();
        $scope.vm.data.customer_id = $scope.vm.data.customer_id.toString();
        $scope.vm.data.quantity = $scope.vm.data.quantity.toString();

        $scope.vm.data.vendor_id = $scope.vendorData.id;
        $scope.vm.data.created_by = UserProfile.getUserProfile().id;
        PurchaseOrderService.editPO($scope.vm).then(function(response) {
            $log.debug("Edit Purchase Order Response ", response);
            if (!response.data.error)
                $state.go($state.current, {}, { reload: true });
            else
                AlertService.showErrorAlert('Error', 'An error occured while updating purchase order. Please try again', false);
        }, function(error) {AlertService.showErrorNotification("Sorry!! Something went wrong.") });
    }




    $scope.showItemSearch = function() {
        var modalInstance = $modal.open({
            templateUrl: 'partials/purchase-order-item-search.html',
            controller: ['$scope', '$uibModalInstance', function($scope, $modalInstance) {
                $scope.item = {};
                $scope.searchItem = function() {
                    $scope.itemSearchData = {};
                    $scope.itemSearchData.customer = 'Test';
                    $scope.itemSearchData.description = 'Test description';
                    $scope.itemSearchData.sku_code = $scope.item.sku_code;
                }
                $scope.selectItem = function() {
                    $modalInstance.close();
                    $scope.vm.data.sku = $scope.itemSearchData.sku_code;
                }
            }],
            windowClass: 'top-model',
            backdrop: 'static',
            scope: $scope
        });
    }

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };

}]);
