"use strict";

 /* Controllers */
app.controller("LocationController", [
  "$scope",
  "$stateParams",
  "$cookies",
  "$uibModal",
  "$compile",
  "$state",
  "$log",
  "UserProfile",
  "locationCount",
  "LocationService",
  "AlertService",
  "$http",
  "$filter",

  function(
    $scope,
    $stateParams,
    $cookies,
    $modal,
    $compile,
    $state,
    $log,
    UserProfile,
    locationCount,
    LocationService,
    AlertService,
    $http,
    $filter
  ) {
    $scope.vm = {};
    var locationTypes = [
      {
        type_id: 1,
        type_name: "Pallets",
        status: 1
      },
      {
        type_id: 2,
        type_name: "Shelf",
        status: 1
      },
      {
        type_id: 5,
        type_name: "Floor",
        status: 1
      },
      {
        type_id: 4,
        type_name: "Pick Cart",
        status: 1
      },
      {
        type_id: 3,
        type_name: "Pick Location",
        status: 1
      }
    ];
    $scope.locationTypes = locationTypes;
    //  $scope.type = 'Pallets';
    $scope.warehouse_id = $stateParams.warehouseId;
    // console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5",$stateParams.warehouseId);
    $scope.locationList = [];
    $scope.warehouse_name = $cookies.get("warehouse_name");
    $scope.locationCount = [];
    angular.forEach(
      locationCount.data,
      function(value, key) {
        this.push(value);
      },
      $scope.locationCount
    );
    console.log($scope.locationCount);

    $scope.addLocation = function(type) {
      if ($stateParams.warehouseId && type != 3 && type != 4 && type != 5) {
        var modalInstance = $modal.open({
          templateUrl: "partials/location-add.html",
          controller: "LocationModalCtrl",
          backdrop: "static",
          size: "lg",
          resolve: {
            deps: [
              "uiLoad",
              function(uiLoad) {
                return uiLoad.load(["js/directives/file-upload.js"]);
              }
            ],
            warehouseId: function() {
              return $stateParams.warehouseId;
            },
            locationType: function() {
              var location_type;
              angular.forEach($scope.locationTypes, function(_locationType) {
                if (_locationType.type_id == type)
                  location_type = _locationType;
              });
              return location_type;
            },
            locationData: null,
            locationId: null
          }
        });
      } else if (
        $stateParams.warehouseId &&
        type != 2 &&
        type != 1 &&
        type != 3 &&
        type != 4
      ) {
        var modalInstance = $modal.open({
          templateUrl: "partials/floor-add.html",
          controller: "FloorModalCtrl",
          size: "md",
          backdrop: "static",
          resolve: {
            warehouseId: function() {
              return $stateParams.warehouseId;
            },
            locationType: function() {
              var location_type;
              angular.forEach($scope.locationTypes, function(_locationType) {
                if (_locationType.type_id == type)
                  location_type = _locationType;
              });
              return location_type;
            },
            locationData: null
          }
        });
      } else {
        var modalInstance = $modal.open({
          templateUrl: "partials/pick-cart-add.html",
          controller: "PickCartCtrl",
          size: "md",
          backdrop: "static",
          resolve: {
            warehouseId: function() {
              return $stateParams.warehouseId;
            },
            locationType: function() {
              var location_type;
              angular.forEach($scope.locationTypes, function(_locationType) {
                if (_locationType.type_id == type)
                  location_type = _locationType;
              });
              return location_type;
            },
            locationData: null
          }
        });
      }
    };

    $scope.deleteAllLocations = function(type) {
      if ($stateParams.warehouseId) {
        var modalInstance = $modal.open({
          templateUrl: "partials/alert-warning.html",
          controller: "WarningModalCtrl",
          backdrop: "static",
          resolve: {
            data: function() {
              var warning = {};
              warning.title = "Delete";
              warning.body =
                "Are you sure you want to delete this location as whole?";
              warning.showCancel = true;
              return warning;
            }
          }
        });
        modalInstance.result.then(
          function(selection) {
            // Warning accepted
            $scope.vm.warehouse_id = $stateParams.warehouseId;
            $scope.vm.location_type = type;
            $log.debug("Delete All Locations POST Data ", $scope.vm);
            LocationService.deleteLocation($scope.vm).then(function(response) {
              $log.debug("Delete Location Response ", response);
              if (response.data.error) {
                if (
                  response.data.message.indexOf(
                    "Location have some occupied locations"
                  ) != -1
                )
                  AlertService.showErrorNotification(
                    "Error!!. It seems like some of the locations are occupied. So you cannot delete the location wholly"
                  );
                else
                  AlertService.showErrorNotification(
                    "Error",
                    "An error occured while trying to delete the locations as a whole. Please try again"
                  );
              } else {
                AlertService.showSuccessNotification(
                  "All Locations has been deleted successfully",
                  3000
                );
                $state.go(
                  $state.current,
                  {},
                  {
                    reload: true
                  }
                );
              }
            });
          },
          function() {
            //  Warning dismissed
          }
        );
      }
    };

    $scope.deleteSingleLocation = function(_locationId) {
      var modalInstance;
      modalInstance = $modal.open({
        templateUrl: "partials/alert-warning.html",
        controller: "WarningModalCtrl",
        backdrop: "static",
        resolve: {
          data: function() {
            var warning = {};
            warning.title = "Delete";
            warning.body = "Are you sure you want to delete this location?";
            warning.showCancel = true;
            return warning;
          }
        }
      });

      modalInstance.result.then(
        function(selection) {
          // Warning accepted
          $scope.vm.location_type = $stateParams.typeId;
          $scope.vm.location_id = _locationId;
          $log.debug("Send Data ", $scope.vm);
          LocationService.deleteSingleLocation($scope.vm).then(function(
            response
          ) {
            $log.debug("Delete single location server response ", response);
            if (response.data.error) {
              $modal.open({
                templateUrl: "partials/alert-warning.html",
                controller: [
                  "$uibModalInstance",
                  "$scope",
                  function($modalInstance, $scope) {
                    $scope.warning = {};
                    $scope.warning.title = "Locations Occupied";
                    $scope.warning.body =
                      "It seems like this location is occupied. You cannot delete this location.";
                    $scope.showCancel = false;
                    $scope.ok = function() {
                      $modalInstance.close();
                    };
                  }
                ]
              });
            } else {
              AlertService.showSuccessNotification(
                "Location has been deleted successfully",
                3000
              );
              $state.go(
                $state.current,
                {},
                {
                  reload: true
                }
              );
            }
          });
        },
        function() {
          //  Warning dismissed
        }
      );
    };

    $scope.editLocation = function(_locationId) {
      var modalInstance = $modal.open({
        templateUrl: "partials/location-edit.html",
        controller: "LocationModalCtrl",
        backdrop: "static",
        size: "lg",
        resolve: {
          warehouseId: function() {
            return $stateParams.warehouseId;
          },
          locationData: function() {
            return $scope.locationList[_locationId];
          },
          locationType: function() {
            return $scope.type;
          },
          locationId: function() {
            return _locationId;
          }
        }
      });
    };

    $scope.listLocations = function(typeId, status) {
      $scope.typeId = typeId;
      $state.go("app.warehouse.location.list", {
        warehouseId: $stateParams.warehouseId,
        typeId: typeId,
        status: status
      });
    };

    $scope.printBarcode = function() {
      var printContent = "";

      var winAttr =
        "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";
      var popupWin = window.open("", "_blank", winAttr);
      popupWin.document.open();
      $http
        .post(__env.apiUrl + "/location-print", {
          api_key: UserProfile.getApiKey(),
          warehouse_id: $stateParams.warehouseId,
          location_type: $stateParams.typeId
        })
        .then(function(data) {
          angular.forEach(data.data, function(value, key) {
            printContent +=
              '<div style="padding:10px;margin-left:5px;"><h6>' +
              value.location_name +
              "</h6>";
            printContent +=
              '<barcode id="barcode" type = "code39" string = "' +
              value.location_name +
              '" options="options" render="img"> </barcode>';
            printContent += "</div>";
          });
          popupWin.document.write(
            "<html><head>" +
              '<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>' +
              '<script type="text/javascript" src="../../../../../bower_components/angular-barcode/dist/angular-barcode.js"></script>' +
              '<style type="text/css">@media print { body { -webkit-print-color-adjust: exact; } }</style>' +
              "</head><body >" +
              printContent +
              "</body></html>"
          );
          popupWin.document.close();
          popupWin.focus();
        });
    };

    $scope.vm.options = {
      width: 2,
      height: 100,
      quite: 10,
      displayValue: true,
      font: "monospace",
      textAlign: "center",
      fontSize: 12,
      backgroundColor: "",
      lineColor: "#000"
    };

    $scope.dataTableOpt = {
      processing: true,
      serverSide: true,
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      pagingType: "simple_numbers",

      ajax: {
        url: __env.apiUrl + "/location-list-type",
        type: "POST",
        data: {
          warehouse_id: $stateParams.warehouseId,
          type_id: $stateParams.typeId,
          status: $stateParams.status,
          api_key: UserProfile.getApiKey()
        }
      },
      columnDefs: [
        {
          className: "dt-center",
          targets: "_all"
        },
        {
          targets: [0],
          render: function(data, type, row) {
            if (row[1] == 1)
              return (
                '<span class="cursor-pointer" ng-click="viewItemDetails(' +
                row[4] +
                ')" >' +
                data +
                "</span>"
              );
            else return data;
          }
        },
        {
          targets: [1],
          render: function(data, type, row) {
            if (data == 1)
              return (
                '<span class="label label-success cursor-pointer" ng-click=viewItemDetails(' +
                row[4] +
                ")> Ocupied</span>"
              );
            else return '<span class="label label-default">Vaccant</span>';
          }
        },
        {
          targets: [2],
          render: function(data, type, row) {
            if (data) return $filter("date")(new Date(data), "longDate");
            else return "";
          }
        },
        {
          targets: [3],
          sortable: false,
          render: function(data, type, row) {
            return (
              '<barcode id="barcode" type = "code39" string = "' +
              data +
              '" options="options" render="img"> </barcode>'
            );
          }
        },
        {
          targets: [4],
          sortable: false,
          render: function(data, type, full) {
            if (
              full[0].indexOf("Receiving") !== -1 ||
              full[0].indexOf("Quarantine") !== -1 ||
              full[0].indexOf("Put Away") !== -1 ||
              full[0].indexOf("Fullfillment") !== -1
            )
              return (
                '<span uib-tooltip="Sorry,Cannot edit this location name" class="btn-disabled btn-xs "><i class="fa fa-pencil hidden-xs"></i> </span>&nbsp <br>' +
                '<br><span uib-tooltip="Sorry,Cannot delete this location" class="btn-disabled btn-xs "><i class="fa fa-trash hidden-xs"></i> </span> '
              );
            else if ($stateParams.typeId == 4)
              return (
                '<span class="btn-danger btn-xs cursor-pointer" ng-click="deleteSingleLocation(' +
                data +
                ')"><i class="fa fa-trash hidden-xs"></i></span> &nbsp'
              );
            else
              return (
                '<span class="btn-success btn-xs cursor-pointer" ng-click="editLocation(' +
                data +
                ')"><i class="fa fa-pencil hidden-xs center" style="padding-right: 5px;"></i> </span>&nbsp<br>' +
                '<br><span class="btn-danger btn-xs cursor-pointer" ng-click="deleteSingleLocation(' +
                data +
                ')"><i class="fa fa-trash hidden-xs center"style="padding-right: 5px;"></i> </span> &nbsp'
              );
          }
        }
      ],
      rowCallback: function(row, data, index) {
        $compile(row)($scope);
        $scope.locationList[data[4]] = data;
        return row;
      }
    };

    $scope.init = function() {
      $scope.locationTypes.forEach(function(type) {
        if (type.type_id == $stateParams.typeId) $scope.type = type;
      }, this);
      $scope.options = {
        width: 1,
        height: 50,
        quite: 10,
        displayValue: false,
        font: "monospace",
        textAlign: "center",
        fontSize: 12,
        backgroundColor: "",
        lineColor: "#000"
      };
    };
    $scope.viewItemDetails = function(_locationId) {
      // console.log($scope);
      var modalInstance = $modal.open({
        templateUrl: "partials/location-item-details.html",
        size: "lg",
        backdrop: "static",
        resolve: {
          itemDetails: function() {
            return LocationService.getItemDetails(_locationId,$scope.type.type_id).then(function(
              response
            ) {
              return response.data.data;
            });
          },
          locationtype:function(){
            return $scope.type.type_id;
          }
        },
        controller: [
          "$scope",
          "$uibModalInstance",
          "itemDetails",
          "locationtype",
          function($scope, $modalInstance, itemDetails,type_id

          ) {
            $scope.pickCart=false;
            if(type_id==4){
              $scope.pickCart=true;
              console.log(JSON.parse(itemDetails));
                $scope.orderDetails=JSON.parse(itemDetails);
              }
              else{
            $scope.itemDetails = itemDetails.item_details;
             $scope.item_test= itemDetails.item_details;
            console.log($scope.item_test);
            $scope.palletDetails = itemDetails.pallet_details;
            $scope.itemsByPage = 5;
            $scope.palletByPage = 5;
            $scope.itemCount = itemDetails.item_details.length;
            $scope.palletCount = itemDetails.pallet_details.length;
            console.log("itemdetails:", $scope.itemDetails);
            $scope.lot_flag = 0;
            $scope.exp_flag = 0;
            $scope.sr_flag = 0;
            $scope.rfid_flag = 0;
            // $scope.itemDetails.forEach(function(val, index) {
            //   if (val.lot.length != 0) $scope.lot_flag++;
            //   if (val.expiry_date.length != 0) $scope.exp_flag++;
            //   if (val.serial_number.length != 0) $scope.sr_flag++;
            //   if (val.rfid_number.length != 0) $scope.rfid_flag++;
            // });



            $scope.itemDetails.forEach(function(val,index){
              // console.log($scope.item_test)
              $scope.itemDetails[index].pic=__env.uploadUrl + '/items/'+val.picture;
              $scope.itemDetails[index].clicked=false;
              $scope.item_test[index].lots=[];
              $scope.item_test[index].rfid=[];
              $scope.item_test[index].exp=[];
              $scope.item_test[index].sno=[];
              $scope.item_test[index].sno.push($scope.item_test[index].serial_number)
              $scope.item_test[index].exp.push(new Date($scope.item_test[index].expiry_date))
              $scope.item_test[index].rfid.push($scope.item_test[index].rfid_number)
              $scope.item_test[index].lots.push($scope.item_test[index].lot)
              })
              $scope.item_test.forEach(function(val,index){
              $scope.item_test[index].lots=[];
              $scope.item_test[index].rfid=[];
              $scope.item_test[index].exp=[];
              $scope.item_test[index].sno=[];
              $scope.item_test[index].serial_number!=""? $scope.item_test[index].sno.push($scope.item_test[index].serial_number):$scope.item_test[index].sno.push("Nill")
              // $scope.item_test[index].exp.push($scope.item_test[index].expiry_date)
              $scope.item_test[index].expiry_date ? $scope.item_test[index].exp.push(new Date($scope.item_test[index].expiry_date)):$scope.item_test[index].exp.push("Nill")
              // $scope.item_test[index].rfid.push($scope.item_test[index].rfid_number)
              $scope.item_test[index].rfid_number!=""? $scope.item_test[index].rfid.push($scope.item_test[index].rfid_number):$scope.item_test[index].rfid.push("Nill")
              // $scope.item_test[index].lots.push($scope.item_test[index].lot)
              $scope.item_test[index].lot!=""? $scope.item_test[index].lots.push($scope.item_test[index].lot):$scope.item_test[index].lots.push("Nill")
              })
              // console.log("test item",$scope.item_test);
            var i,j;
              var x=[]
            for(i=$scope.item_test.length-1;i>=0;i--){
            for(j=i-1;j>=0;j--){
              // console.log(i,j,$scope.item_test.length)
            if($scope.item_test[i]!=undefined&&$scope.item_test[j]!=undefined){
              if($scope.item_test[i].sku==$scope.item_test[j].sku){
              $scope.item_test[i].location_quantity+=$scope.item_test[j].location_quantity;
              $scope.item_test[j].serial_number!=""? $scope.item_test[i].sno.push($scope.item_test[j].serial_number):$scope.item_test[i].sno.push("Nill")
              $scope.item_test[j].expiry_date ? $scope.item_test[i].exp.push(new Date($scope.item_test[j].expiry_date)):$scope.item_test[j].exp.push("Nill")
              $scope.item_test[j].rfid_number!=""? $scope.item_test[i].rfid.push($scope.item_test[j].rfid_number):$scope.item_test[i].rfid.push("Nill")
              $scope.item_test[j].lot!=""? $scope.item_test[i].lots.push($scope.item_test[j].lot):$scope.item_test[i].lots.push("Nill")
              $scope.item_test.splice(j,1);
              }
            }
            }
            }
            // console.log("buliding:",$scope.item_test)

            // console.log(itemDetails)
            $scope.check=function(ind){
              $scope.itemDetails.forEach(function(val,id){
                if(id!=ind){
                  // console.log(id,ind)
                  $scope.itemDetails[id].clicked=false;
                  // console.log($scope.itemDetails[ind])
                }

              })
            }
            console.log($scope.lot_flag);
            $scope.picturePath = [];
            for (var i = 0; i < $scope.itemDetails.length; i++)
              if ($scope.itemDetails[i].picture)
                $scope.picturePath[i] =
                  __env.uploadUrl + "/$scope.item_test/" + $scope.itemDetails[i].picture;
            $scope.cancel = function() {
              $modalInstance.close();
            };



          }
          }
        ]
      });
    };
  }
]);
app.controller("FloorModalCtrl", [
  "$scope",
  "$uibModalInstance",
  "$log",
  "$state",
  "warehouseId",
  "locationType",
  "locationData",
  "UserProfile",
  "LocationService",
  "AlertService",
  "$http",
  function(
    $scope,
    $modalInstance,
    $log,
    $state,
    warehouseId,
    locationType,
    locationData,
    UserProfile,
    LocationService,
    AlertService,
    $http
  ) {
    $scope.cancel = function() {
      $modalInstance.close();
    };
    var formData = {};
    formData.warehouse_id = warehouseId;
    formData.api_key = UserProfile.getApiKey();

    $http.post(__env.apiUrl + "/warehouse-edit", formData).then(function(res) {
      $scope.warehouse_plan = res.data.data.plan;
    });

    $scope.location_type = locationType;
    $scope.locationData = locationData;

    $scope.saveSection = function() {
      $scope.vm = {};
      $scope.vm.warehouse_id = warehouseId;
      $scope.vm.location_type = locationType.type_id;
      $scope.vm.updated_by = UserProfile.getUserProfile().id;
      $scope.vm.status = 0;
      $scope.vm.location_name = $scope.section_name;
      $modalInstance.close();
      LocationService.createLocation($scope.vm).then(
        function(response) {
          $log.debug("Add Location Response ", response);
          if (!response.data.error) {
            AlertService.showSuccessNotification(
              "Location has been created successfully",
              3000
            );
            $state.go(
              $state.current,
              {},
              {
                reload: true
              }
            );
          } else {
            if (
              response.data.message.indexOf("Location name already exists") !=
              -1
            )
              AlertService.showErrorNotification(
                "Warning!! There is already a location with specified name. Please check again"
              );
          }
        },
        function(error) {
          AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        }
      );
    };
  }
]);
app.controller("PickCartCtrl", [
  "$scope",
  "$uibModalInstance",
  "$log",
  "$state",
  "warehouseId",
  "locationType",
  "locationData",
  "UserProfile",
  "LocationService",
  "AlertService",
  "$http",
  function(
    $scope,
    $modalInstance,
    $log,
    $state,
    warehouseId,
    locationType,
    locationData,
    UserProfile,
    LocationService,
    AlertService,
    $http
  ) {
    $scope.cancel = function() {
      $modalInstance.close();
    };
    var formData = {};
    formData.warehouse_id = warehouseId;
    formData.api_key = UserProfile.getApiKey();

    $scope.location_type = locationType;
    $scope.locationData = locationData;

    $scope.savepickcart = function() {
      $scope.vm = {};
      $scope.vm.warehouse_id = warehouseId;
      $scope.vm.location_type = locationType.type_id;
      $scope.vm.updated_by = UserProfile.getUserProfile().id;
      $scope.vm.status = 0;
      $scope.vm.location_name = $scope.pick_size;
      $modalInstance.close();
      LocationService.createLocation($scope.vm).then(
        function(response) {
          $log.debug("Add Location Response ", response);
          if (!response.data.error) {
            AlertService.showSuccessNotification(
              "Location has been created successfully",
              3000
            );
            $state.go(
              $state.current,
              {},
              {
                reload: true
              }
            );
          } else {
            if (
              response.data.message.indexOf("Location name already exists") !=
              -1
            )
              AlertService.showErrorNotification(
                "Warning!!  There is already a location with specified name. Please check again"
              );
          }
        },
        function(error) {
          AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        }
      );
    };
  }
]);

app.controller("LocationModalCtrl", [
  "$http",
  "$scope",
  "$uibModalInstance",
  "$log",
  "$state",
  "warehouseId",
  "locationType",
  "locationData",
  "UserProfile",
  "LocationService",
  "AlertService",
  "locationId",
  "WarehouseService",
  function(
    $http,
    $scope,
    $modalInstance,
    $log,
    $state,
    warehouseId,
    locationType,
    locationData,
    UserProfile,
    LocationService,
    AlertService,
    locationId,
    WarehouseService
  ) {
    $scope.editform = {};
    $scope.vm = {};
    $scope.warning = {};
    $scope.warning.title;
    $scope.warning.body;
    $scope.showCancel = true;
    $scope.location_type = locationType;
    if (locationData != null) {
      var locationDataa = locationData[0];
      console.log("type:",locationType.type_id)
      if (locationType.type_id == 5) $scope.locationData = locationDataa;
      else $scope.locationData = locationDataa.substring(2);
      console.log($scope.locationData);
      $scope.vm.locationName = $scope.locationData;
      var initail_data = $scope.locationData.split("-");
      $scope.editform.isle_number = Number(initail_data[0]);
      $scope.editform.section_number = Number(initail_data[1]);
      $scope.editform.pickcartsize = Number(initail_data[1]);
      $scope.editform.level_number = Number(initail_data[2]);
      $scope.editform.location_number = Number(initail_data[3]);
      $scope.editform.pickCartStyle = Number(initail_data[0]);
    }
    // if(locationType.type_id==5){
    // console.log("data:",locationData);
    // }

    $scope.vm.csv_file = "";
    WarehouseService.listWarehouse(warehouseId).then(function(res) {
      $scope.warehouse_plan = res.data.data.plan;
      console.log(res);
    });
    $scope.saveLocation = function() {
      $scope.vm.warehouse_id = warehouseId;
      $scope.vm.location_type = locationType.type_id;
      $scope.vm.updated_by = UserProfile.getUserProfile().id;
      $scope.vm.status = 0;
      if ($scope.vm.isle_number && $scope.vm.isle_number.toString().length == 1)
        $scope.vm.location_name = "0" + $scope.vm.isle_number + "-";
      else $scope.vm.location_name = $scope.vm.isle_number + "-";
      if (
        $scope.vm.section_number &&
        $scope.vm.section_number.toString().length == 1
      )
        $scope.vm.location_name =
          $scope.vm.location_name + "0" + $scope.vm.section_number + "-";
      else
        $scope.vm.location_name =
          $scope.vm.location_name + $scope.vm.section_number + "-";
      if (
        $scope.vm.level_number &&
        $scope.vm.level_number.toString().length == 1
      )
        $scope.vm.location_name =
          $scope.vm.location_name + "0" + $scope.vm.level_number + "-";
      else
        $scope.vm.location_name =
          $scope.vm.location_name + $scope.vm.level_number + "-";
      if (
        $scope.vm.location_number &&
        $scope.vm.location_number.toString().length == 1
      )
        $scope.vm.location_name =
          $scope.vm.location_name + "0" + $scope.vm.location_number;
      else
        $scope.vm.location_name =
          $scope.vm.location_name + $scope.vm.location_number;

      $modalInstance.close();
      LocationService.createLocation($scope.vm).then(
        function(response) {
          $log.debug("Add Location Response ", response);
          if (!response.data.error) {
            AlertService.showSuccessNotification(
              "Location has been created successfully",
              3000
            );
            $state.go(
              $state.current,
              {},
              {
                reload: true
              }
            );
          } else {
            if (
              response.data.message.indexOf("Location name already exists") !=
              -1
            )
              AlertService.showErrorNotification(
                "Warning!!, There is already a location with specified name. Please check again"
              );
          }
        },
        function(error) {
          AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        }
      );
    };

    $scope.updateLocation = function() {
      var formData = {};
      formData.location_type = $scope.location_type.type_id;
      formData.location_id = locationId;
      if ($scope.location_type.type_id == 5) {
        formData.name = $scope.vm.locationName;
      } else if ($scope.location_type.type_id == 3) {
        formData.name = $scope.editform.pickcartsize;
      } else {
        formData.name =
          $scope.editform.isle_number +
          "-" +
          $scope.editform.section_number +
          "-" +
          $scope.editform.level_number +
          "-" +
          $scope.editform.location_number;
      }
      // formData.name = $scope.locationData;
      console.log("formdata:", formData);
      // formData.name = formData.name.replace(/^(\d{2})(\d{2})(\d{2})(\d{2})/, '$1-$2-$3-$4');
      formData.warehouse_id = warehouseId;

      LocationService.editLocation(formData).then(
        function(response) {
          $log.debug("Update Location Response , ", response);
          $modalInstance.close();
          if (!response.data.error) {
            AlertService.showSuccessNotification(
              "Location name has been edited successfully"
            );
            $state.go(
              $state.current,
              {},
              {
                reload: true
              }
            );
          } else
            AlertService.showErrorNotification(
              "Sorry!! Something went wrong. "
            );
        },
        function(response) {
          $modalInstance.close();
          AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        }
      );
    };
    $scope.init = function() {
      if (locationData) {
        $scope.locationData = locationData;
      }
    };

    $scope.cancel = function() {
      $modalInstance.dismiss("cancel");
    };
  }
]);
