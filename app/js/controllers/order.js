"use strict";

/*  var app = angular.module('examples', ['chart.js', 'ui.bootstrap']);*/

app.config(function(ChartJsProvider) {
  // Configure all charts
  ChartJsProvider.setOptions({
    colours: ["#FF6E40", "#FBC02E", "#673AB7", "#66bd78", "#f05050"],
    responsive: true
  });
  // Configure all doughnut charts
  ChartJsProvider.setOptions("Doughnut", {
    animateScale: true
  });
});

app.controller("DashboardCtroller", [
  "$scope",
  "$timeout",
  function($scope, $timeout) {
    $scope.labels = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep"
    ];
    $scope.series = ["Page Views", "Visitors"];
    $scope.data = [
      [123, 110, 113, 124, 112, 121, 119, 110, 124],
      [117, 113, 118, 110, 118, 111, 117, 119, 117]
    ];
    $scope.onClick = function(points, evt) {
      console.log(points, evt);
    };
    $scope.onHover = function(points) {
      if (points.length > 0) {
        console.log("Point", points[0].value);
      } else {
        console.log("No point");
      }
    };
    $scope.color = "#FF6E40";
    $scope.colours = [
      {
        // grey
        fillColor: "rgba(241,126,86,1)",
        strokeColor: "rgba(241,126,86,1)",
        pointColor: "rgba(241,126,86,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(241,126,86,1)"
      },
      {
        // dark grey
        fillColor: "rgba(105,95,86,1)",
        strokeColor: "rgba(105,95,86,1.0)",
        pointColor: "rgba(105,95,86,1.0)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(105,95,86,1.0)"
      }
    ];

    $scope.options = {
      scaleShowVerticalLines: false,
      scaleShowLabels: true,
      scaleLineWidth: 1,
      scaleLineColor: "rgba(0,0,0,0.1)",
      scaleShowHorizontalLines: false,
      scaleGridLineWidth: 1,
      scaleShowGridLines: false,
      scaleGridLineColor: "rgba(0,0,0,0)",
      pointDotRadius: 5,
      pointHitDetectionRadius: 10
    };
  }
]);

app.controller("DashboardBarCtrl", [
  "$scope",
  "$timeout",
  function($scope, $timeout) {
    $scope.options = {
      scaleShowVerticalLines: false
    };
    $scope.labels = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep"
    ];
    $scope.series = ["Page Views", "Visitors"];
    $scope.data = [
      [23, 12, 16, 28, 10, 21, 19, 12, 24],
      [7, 13, 8, 10, 18, 11, 17, 9, 17]
    ];
    $scope.colours = [
      {
        // grey
        fillColor: "rgba(241,126,86,1)",
        strokeColor: "rgba(241,126,86,1)",
        highlightFill: "rgba(241,126,86,1)",
        highlightStroke: "rgba(241,126,86,1)"
      },
      {
        // dark grey
        fillColor: "rgba(105,95,86,1.0)",
        strokeColor: "rgba(105,95,86,1)",
        highlightFill: "rgba(105,95,86,1)",
        highlightStroke: "rgba(105,95,86,1.0)"
      }
    ];

    $scope.options = {
      scaleShowVerticalLines: false,
      scaleShowLabels: true,
      scaleLineWidth: 1,
      scaleLineColor: "rgba(0,0,0,0.1)",
      scaleShowHorizontalLines: false,
      scaleGridLineWidth: 1,
      scaleShowGridLines: false,
      scaleGridLineColor: "rgba(0,0,0,0)",
      pointDotRadius: 5,
      pointHitDetectionRadius: 10
    };
  }
]);

function getRandomValue(data) {
  var l = data.length,
    previous = l ? data[l - 1] : 50;
  var y = previous + Math.random() * 10 - 5;
  return y < 0 ? 0 : y > 100 ? 100 : y;
}

/**
 * Order Listing Controller
 *
 */
app.controller("OrderListController", [
  "$scope",
  "$stateParams",
  "$state",
  "$compile",
  "UserProfile",
  "$http",
  "$uibModal",
  "$filter",

  function(
    $scope,
    $stateParams,
    $state,
    $compile,
    UserProfile,
    $http,
    $modal,
    $filter
  ) {
    $scope.vm = {};
    $scope.vm.rowsPerPage = 10;
    $scope.currentPage = 1;
    $scope.tabIndex = 0;
    $scope.rows = [10, 50, 100];
    $scope.age = 0;
    $scope.customer_name = "";

    $scope.pageChanged = function() {
      var orderStatus;
      if ($scope.tabIndex == 0) {
        orderStatus = "awaiting_shipment";
      } else if ($scope.tabIndex == 1) {
        orderStatus = "shipped";
      } else if ($scope.tabIndex == 2) {
        orderStatus = "on_hold";
      } else if ($scope.tabIndex == 3) {
        orderStatus = "cancelled";
      }
      console.log("current page", $scope.current_page);
      $http
        .post(__env.apiUrl + "/shipstation-saved-order-list", {
          current_page: ($scope.currentPage - 1) * $scope.vm.rowsPerPage,
          per_page: $scope.vm.rowsPerPage,
          order_status: orderStatus,
          // days:$scope.age,
          // customer_name:$scope.customer_name,
          api_key: UserProfile.getApiKey(),
          store_id: ""
        })
        .then(function(response) {
          console.log("List Reponse ", response);
          $scope.vm.orderCount = 0;
          if (!response.data.error) {
            $scope.vm.orderCount = response.data.data.total_count;
            $scope.orderList = response.data.data.orders;
            var today = new Date();
            $scope.orderList.forEach(function(single_orderdata, index) {
              // val.orders.forEach(function(single_orderdata, id) {
              // var date = new Date(single_orderdata.orderDate);
              // var msMinute = 60 * 1000,
              //   msDay = 60 * 60 * 24 * 1000
              // var dateDiff = Math.floor((today - date) / msDay)
              // var timeDiff = Math.floor(((today - date) % msDay) / msMinute)
              // var totDiff = dateDiff + "d  " + Math.floor(timeDiff / 60) + "H";
              // $scope.orderList[index].age = totDiff;
              $scope.orderList[index].tags = [];
              if (single_orderdata.tagIds)
                for (var i = 0; i < single_orderdata.tagIds.length; i++) {
                  var tagData = $filter("filter")(
                    response.data.data.tag_detils,
                    {
                      tag_id: single_orderdata.tagIds[i]
                    }
                  );
                  $scope.orderList[index].tags.push(tagData);
                }
              // })
            });
            console.log("order list", $scope.orderList);
            // $scope.orderList.forEach(function(eachOrder,index){
            // $scope.orderList.binId="Bin"+index+1
            // })
            // console.log("bin orderlist:", $scope.orderList,"bin"+1+1)
          }
        });
    };

    $scope.pageChanged();

    $scope.viewOrderDetails = function(orderId) {
      console.log(orderId);
      $scope.id = orderId;
      $modal.open({
        templateUrl: "partials/order-detail.html",
        controller: [
          "$scope",
          "orderDetail",
          "$uibModalInstance",
          "$uibModal",
          function($scope, orderDetail, $modalInstance, $modal) {
            $scope.orderDetail = orderDetail;
            console.log("Order detail", $scope.orderDetail);
            $scope.cancel = function() {
              $modalInstance.close();
            };
            $scope.options = {
              width: 2,
              height: 30,
              quite: 10,
              displayValue: false,
              font: "monospace",
              textAlign: "center",
              fontSize: 12,
              backgroundColor: "",
              lineColor: "#000"
            };
            $scope.showPickList = function() {
              $modalInstance.close();
              $modal.open({
                templateUrl: "partials/pick-cart-print.html",
                size: "lg",
                backdrop: false,
                controller: [
                  "$scope",
                  "$uibModalInstance",
                  "UserProfile",
                  function($scope, $mInstance, UserProfile) {
                    // $scope.$apply();
                    // console.log($scope);
                    $scope.closetab = function() {
                      // console.log("hai")
                    };
                    $scope.date = new Date();
                    $scope.RequerdTot = 0;
                    var orderData = $scope.$parent.$resolve.orderDetail;
                    // console.log("parent",$scope.$parent.$resolve)
                    $scope.orderID =
                      $scope.$parent.$resolve.orderDetail.orderId;
                    // console.log($scope.$parent.$resolve.orderDetail.orderId)
                    $scope.orderItems = orderData.items;
                    var selected = [];
                    selected.push($scope.orderID);
                    $scope.orderItems.forEach(function(val) {
                      $scope.RequerdTot = $scope.RequerdTot + val.quantity;
                    });
                    $http
                      .post(
                        __env.apiUrl + "/shipstation-order-details-by-ids",
                        {
                          api_key: UserProfile.getApiKey(),
                          orderid_array: selected
                        }
                      )
                      .then(function(res) {
                        var dt = res.data.message;
                        // console.log("hai:",dt)
                        $scope.orderItems.forEach(function(val, index) {
                          dt.forEach(function(pair) {
                            pair.items.forEach(function(dat) {
                              if (val.sku == dat.sku) {
                                // console.log(val.sku,dat.sku)
                                $scope.orderItems[index].location_db =
                                  dat.location_name;
                                // console.log($scope.orderItems[index])
                              }
                            });
                            // console.log(pair)
                          });
                        });
                      });
                    // dt.forEach(function(loc){
                    //     if(loc.items!=undefined){
                    //       console.log("loc:",loc.items)
                    //       loc.items.forEach(function(itemLoc){
                    //         $scope.orderItems.forEach(function(locationInDb,index){
                    //           if(locationInDb.sku==itemLoc.sku){
                    //             console.log("prrrrrr:",locationInDb);
                    //             $scope.orderItems[index].location_db=itemLoc.location_name;
                    //           }

                    //         })
                    //       console.log(itemLoc.sku);
                    //   })

                    //     }
                    // else $scope.allOrderDetails[index].location_db="item not fount in location";

                    // })

                    // console.log($scope.orderItems)

                    // $scope.time=new Date().getTime()
                  }
                ],
                scope: $scope
              });
            };
          }
        ],
        size: "xlg",
        resolve: {
          orderDetail: function() {
            return $http
              .post(__env.apiUrl + "/shipstation-saved-order-list-by-id", {
                api_key: UserProfile.getApiKey(),
                order_id: orderId
              })
              .then(function(response) {
                console.log("Location Count Response ", response.data.data);
                return response.data.data.detils[0];
              });
          }
        }
      });
    };
  }
]);

/**
 * Pick List Controller
 */

app.controller("orderPickListController", [
  "$scope",
  "$http",
  "$stateParams",
  "$state",
  "$compile",
  "UserProfile",
  "$uibModal",
  "$filter",
  "AlertService",
  "OrderService",
  function(
    $scope,
    $http,
    $stateParams,
    $state,
    $compile,
    UserProfile,
    $modal,
    $filter,
    AlertService,
    OrderService
  ) {
    $scope.createPickList = function() {
      $modal.open({
        size: "xlg",
        templateUrl: "partials/order-pick-list-create.html",
        controller: "PickListCreateController",
        backdrop: "static",
        resolve: {
          CarrierList: function() {
            return $http
              .post(__env.apiUrl + "/shipstation-list-carrier", {
                api_key: UserProfile.getApiKey()
              })
              .then(function(res) {
                return res.data;
              });
          }
        }
      });
    };
    OrderService.getPickList().then(response => {
      if (!response.data.error) {
        console.log("Get Pick List ", response.data.data);
        $scope.pickListDetails = [];
        response.data.data.forEach(data => {
          // console.log(JSON.parse(data.data))
          $scope.pickListDetails.push(JSON.parse(data.data));
        });
        console.log("picklist edited", $scope.pickListDetails);
      }
    });
  }
]);

/**
 * Controller for pick list creation
 */
app.controller("PickListCreateController", [
  "$scope",
  "$stateParams",
  "$state",
  "$compile",
  "UserProfile",
  "$http",
  "$filter",
  "AlertService",
  "$uibModalInstance",
  "OrderService",
  "CarrierList",
  function(
    $scope,
    $stateParams,
    $state,
    $compile,
    UserProfile,
    $http,
    $filter,
    AlertService,
    $modalInstance,
    OrderService,
    CarrierList
  ) {
    $scope.example1data = [
      { id: 1, label: "David" },
      { id: 2, label: "Jhon" },
      { id: 3, label: "Danny" }
    ];
    $scope.showOrder = false;
    $scope.idNotValid = false;
    $scope.date_sort_flag = 0;
    $scope.order_sort_flag = 0;
    $scope.name_sort_flag = 0;
    $scope.store_sort_flag = 0;
    $scope.age_sort_flag = 0;
    $scope.carrier_sort_flag = 0;
    $scope.service_sort_flag = 0;
    $scope.order_sort = 0;
    $scope.store_sort = 0;
    $scope.age_sort = 0;
    $scope.date_sort = 0;
    $scope.carrier_sort = 0;
    $scope.service_sort = 0;
    $scope.vm = {};
    $scope.lessItem = false;

    $scope.shippingMethods = CarrierList;
    $scope.Service_code = [];
    console.log("methord", $scope.shippingMethods);
    $http
      .post(__env.apiUrl + "/shipstation-store-namelist", {
        api_key: UserProfile.getApiKey()
      })
      .then(function(res) {
        $scope.storeList = res.data.data;
        console.log("storelist:", $scope.storeList);
      });
    $http
      .post(__env.apiUrl + "/item-skulist", {
        api_key: UserProfile.getApiKey()
      })
      .then(function(res) {
        $scope.skuList = res.data.data;
        console.log("skulist:", $scope.skuList);
      });
    $scope.select2 = function() {
      console.log("model:", $scope.package_code);
      console.log("list:", $scope.Services);
    };
    $scope.getLocationName = function(val) {
      return $http
        .post(__env.apiUrl + "/search-picklist-locationname", {
          api_key: UserProfile.getApiKey(),
          location_name: val
        })
        .then(function(response) {
          // $log.debug('Response Location Search ', response);
          if (!response.data.error) return response.data.data;
        });
    };
    $scope.agefilterchange = function(val) {
      console.log(val);
      if (val == 1) {
        $scope.age_sort = 1;
        $scope.age_sort_flag = 1;
      } else if (val == 2) {
        $scope.age_sort = 2;
        $scope.age_sort_flag = 0;
      }
      $scope.order_sort = 0;

      $scope.store_sort = 0;
      $scope.date_sort = 0;
      $scope.carrier_sort = 0;
      $scope.service_sort = 0;
      $scope.pageChanged();
    };

    $scope.storefilterchange = function(val) {
      console.log(val);
      if (val == 1) {
        $scope.store_sort = 1;
        $scope.store_sort_flag = 1;
      } else if (val == 2) {
        $scope.store_sort = 2;
        $scope.store_sort_flag = 0;
      }
      $scope.order_sort = 0;
      $scope.age_sort = 0;
      $scope.date_sort = 0;
      $scope.carrier_sort = 0;
      $scope.store_sort = 0;
      $scope.service_sort = 0;
      $scope.pageChanged();
    };
    $scope.orderfilterchange = function(val) {
      console.log(val);
      if (val == 1) {
        $scope.order_sort = 1;
        $scope.order_sort_flag = 1;
      } else if (val == 2) {
        $scope.order_sort = 2;
        $scope.order_sort_flag = 0;
      }
      $scope.store_sort = 0;
      $scope.age_sort = 0;
      $scope.date_sort = 0;
      $scope.carrier_sort = 0;
      $scope.service_sort = 0;
      $scope.pageChanged();
    };
    $scope.carrierfilterchange = function(val) {
      console.log(val);
      if (val == 1) {
        $scope.carrier_sort = 1;
        $scope.carrier_sort_flag = 1;
      } else if (val == 2) {
        $scope.carrier_sort = 2;
        $scope.carrier_sort_flag = 0;
      }
      $scope.store_sort = 0;
      $scope.age_sort = 0;
      $scope.date_sort = 0;
      $scope.order_sort = 0;
      $scope.service_sort = 0;
      $scope.pageChanged();
    };
    $scope.datefilterchange = function(val) {
      console.log(val);
      if (val == 1) {
        $scope.date_sort = 1;
        $scope.date_sort_flag = 1;
      } else if (val == 2) {
        $scope.date_sort = 2;
        $scope.date_sort_flag = 0;
      }
      $scope.store_sort = 0;
      $scope.age_sort = 0;
      $scope.carrier_sort = 0;
      $scope.order_sort = 0;
      $scope.service_sort = 0;
      $scope.pageChanged();
    };
    $scope.servicefilterchange = function(val) {
      console.log(val);
      if (val == 1) {
        $scope.service_sort = 1;
        $scope.service_sort_flag = 1;
      } else if (val == 2) {
        $scope.service_sort = 2;
        $scope.service_sort_flag = 0;
      }
      $scope.store_sort = 0;
      $scope.date_sort = 0;
      $scope.age_sort = 0;
      $scope.carrier_sort = 0;
      $scope.order_sort = 0;
      $scope.pageChanged();
    };
    var servic_array = [];

    $scope.close = function() {
      $modalInstance.close();
    };
    var selectedCarrierArray = [];
    $scope.select3 = function() {
      servic_array = [];
      $scope.service_code.forEach(function(val) {
        servic_array.push(val.code);
      });
      $scope.pageChanged();
    };
    $scope.select = function() {
      selectedCarrierArray = [];
      console.log("multi select working", $scope.selectcarrier);

      $scope.selectcarrier.forEach(function(val) {
        selectedCarrierArray.push(val.code);
      });

      console.log("selectedCarrierArray", selectedCarrierArray);
      if (selectedCarrierArray.length > 0) {
        selectedCarrierArray.forEach(function(valu, index) {
          $http
            .post(__env.apiUrl + "/shipstation-service-list", {
              api_key: UserProfile.getApiKey(),
              carrier_code: valu
            })
            .then(function(res) {
              //  (res.data.message);
              $scope.Service_code = [];
              JSON.parse(res.data.message).forEach(function(element) {
                $scope.Service_code.push(element);
              });
              $scope.pageChanged();
            });
          console.log($scope.selectcarrier, "||||||", $scope.Service_code);
        });
      } else {
        $scope.pageChanged();
      }
    };
    $scope.instockFlag = false;
    // var instock=false
    // $scope.inStockFlagChange=function(){
    //   if($scope.instockFlag==true){

    //   }

    $scope.nextData = function(value) {
      $scope.pageChanged();

      // console.log("valueeee:", UserProfile.getUserProfile());
      var check = $scope.PcId.location_name.split("-");
      $scope.count = check[1];
      if ($scope.PcId.location_name.indexOf("PC") > -1) {
        // $scope.idNotvalid=false
        $scope.showOrder = true;
        $scope.showSelected = true;
      } else {
        $scope.idNotValid = true;
        console.log("not valid");
      }
    };
    var checkAvailability = function(orders) {
      console.log("inside availability");
      orders.forEach(function(order, ind) {
        $scope.orderList[ind].available = true;
        if (selected.indexOf(order.orderId) == -1)
          order.items.forEach(function(item) {
            if (inStockItems.indexOf(item.sku) > -1) {
              console.log(inStockArray[item.sku].quantity,inStockArray[item.sku].quantityInDb)
                if (inStockArray[item.sku].quantity >= inStockArray[item.sku].quantityInDb) {
                  orders[ind].available = false;
                  console.log(item.sku, "not awailable");
                  console.log(orders[ind]);
                }
              // }
            }
          });
      });
      $scope.orderList = orders;
    };

    $scope.rowsPerPage = 10;
    $scope.customer_name = "";
    $scope.currentPage = 1;
    $scope.store_name = "";
    $scope.pageChanged = function() {
      // console.log($scope)
      var a = $scope.age ? $scope.age : 0;
      console.log("SERVICE ARRAY == ", $scope.selectcarrier);
      $http
        .post(__env.apiUrl + "/shipstation-order-list-filtered", {
          current_page: ($scope.currentPage - 1) * $scope.rowsPerPage,
          per_page: $scope.rowsPerPage,
          days: a,
          // store_name: $scope.store_name,
          store_id: $scope.store_id ? $scope.store_id : "",
          sku: $scope.search_sku ? $scope.search_sku : "",
          service_code: $scope.service_code ? servic_array : "",
          carrier_code: $scope.selectcarrier ? selectedCarrierArray : "",
          order_status: "awaiting_shipment",
          api_key: UserProfile.getApiKey(),
          store_id: "",
          date_sort: $scope.date_sort,
          store_sort: $scope.store_sort,
          age_sort: $scope.age_sort,
          date_sort: $scope.date_sort,
          order_sort: $scope.order_sort,
          order_number: $scope.get_order_num ? $scope.get_order_num : "",
          service_sort: $scope.service_sort,
          carrier_sort: $scope.carrier_sort,
          instock: $scope.instockFlag == true ? 1 : 0,
          location_name: $scope.PcId.location_name,
          warehouse_id: $scope.PcId.warehouse_id,
          user_id: UserProfile.getUserProfile().id
        })
        .then(function(response) {
          console.log(
            "current_page ",
            $scope.currentPage,
            $scope.vm.rowsPerPage
          );
          console.log("List Reponse ", response);
          $scope.vm.orderCount = 0;
          if (!response.data.error) {
            $scope.vm.orderCount = response.data.data.total_count;
            console.log("count", $scope.vm.orderCount);
            $scope.orderList = response.data.data.orders;
            var today = new Date();
            $scope.orderList.forEach(function(single_orderdata, index) {
              $scope.orderList[index].available = true;
              console.log("single order data:", single_orderdata);
              var date = new Date(single_orderdata.orderDate);
              var msMinute = 60 * 1000,
                msDay = 60 * 60 * 24 * 1000;
              var dateDiff = Math.floor((today - date) / msDay);
              var timeDiff = Math.floor(((today - date) % msDay) / msMinute);
              var totDiff = dateDiff + "d  " + Math.floor(timeDiff / 60) + "H";
              console.log("age:", totDiff);
              $scope.orderList[index].age = totDiff;
              $scope.orderList[index].tags = [];
              if (single_orderdata.tagIds)
                for (var i = 0; i < single_orderdata.tagIds.length; i++) {
                  var tagData = $filter("filter")(
                    response.data.data.tag_detils,
                    {
                      tag_id: single_orderdata.tagIds[i]
                    }
                  );
                  $scope.orderList[index].tags.push(tagData);
                }
              // })
            });
            console.log("new orderlist:", $scope.orderList);
            if (changeFlag) {
              checkAvailability($scope.orderList);
            }
          }
        });
    };

    //   $scope.reset=function(id){

    // if(selected.length>0 && selected.indexOf(id)==-1){
    // // document.getElementById(id).checked=false;
    // console.log("called",selected.indexOf(id),document.getElementById(id))
    // }
    //   }

    $scope.test = function(id) {
      if (selected.length > 0) {
        if (selected.indexOf(id) > -1) return true;
        else return false;
      } else return false;
    };
    var inStockItems = [];
    var inStockArray = [];
    var itemsInDb;
    var changeFlag = false;
    $scope.checkboxselection = function(val, oid) {
      changeFlag = true;
      $http
        .post(__env.apiUrl + "/get-item-quantity", {
          api_key: UserProfile.getApiKey(),
          order_id: oid,
          warehouse_id: $scope.PcId.warehouse_id
        })
        .then(function(res) {
          itemsInDb = res.data.data;
          console.log("items in db:", itemsInDb);

          // checkAvailability($scope.orderList)
          if (!document.getElementById(oid).checked) {
            document.getElementById(oid).checked = true;

            $scope.checkCount(oid);
            var temp = {};
            $scope.orderList[val].items.forEach(function(item) {
              if (inStockItems.indexOf(item.sku) < 0) {
                inStockItems.push(item.sku);
                var temps = $filter("filter")(itemsInDb, {
                  sku: item.sku
                });
                temp.sku = item.sku;
                temp.quantity = item.quantity;
                temp.quantityInDb = parseInt(temps[0].quantity);
                inStockArray[item.sku] = temp;
                temp = {};
              } else {
                inStockArray[item.sku]
                  ? (inStockArray[item.sku].quantity += item.quantity)
                  : "";
              }
            });
            console.log("instock array", inStockArray);
            checkAvailability($scope.orderList);
          } else {
            document.getElementById(oid).checked = false;

            $scope.checkCount(oid);
            $scope.orderList[val].items.forEach(function(item) {
              if (inStockArray[item.sku] != undefined) {
                inStockArray[item.sku].quantity -= item.quantity;
                if (inStockArray[item.sku].quantity <= 0) {
                  inStockItems.splice(inStockItems.indexOf(item.sku), 1);
                  delete inStockArray[item.sku];
                }
              }
              console.log("instock array:", inStockArray);
            });
            checkAvailability($scope.orderList);
          }
        });

      console.log("in stock items:", inStockItems);

      console.log($scope.orderList[val]);
      if (inStockItems.length <= 0) {
        console.log("!!!!!!!!!!!!!!!!!!!!");
        changeFlag = false;
      }
    };

    var selected = [];
    $scope.checkCount = function(orderID) {
      // console.log("index", index)
      console.log(orderID);
      console.log("selected.lenth:", selected.length, "count:", $scope.count);
      console.log(selected);

      if (selected.indexOf(orderID) > -1) {
        selected.splice(selected.indexOf(orderID), 1);
      } else {
        selected.push(orderID);
      }
      if (selected.length > 0) {
        $scope.lessItem = true;
      } else {
        $scope.lessItem = false;
      }
      console.log(selected);
    };

    $scope.listAllItems = function() {
      if (selected.length > 0 && selected.length <= $scope.count) {
        $scope.loadorderDetails();
      } else {
        console.log("alert needed");
        AlertService.showErrorNotification(
          "Sorry Please Choose " + $scope.count + " or less Orders"
        );
      }
    };

    $scope.allOrderDetails = [];
    $scope.RequerdTot = 0;
    // $scope.orderID=id;
    $scope.vm = {};
    // console.log($scope.selected)
    $scope.locArray = [];
    $scope.or;

    $scope.load = function() {
      selected.forEach((id, index) => {
        $http
          .post(__env.apiUrl + "/shipstation-saved-order-list-by-id", {
            api_key: UserProfile.getApiKey(),
            order_id: id
          })
          .then(response => {
            console.log(response);
            if (response.data.data != undefined) {
              response.data.data.detils[0].items.forEach(itemDat => {
                $scope.RequerdTot = $scope.RequerdTot + itemDat.quantity;
                var cart_loc = index + 1;
                itemDat.cart_loc =
                  itemDat.quantity +
                  " in" +
                  itemDat.pick_cart.split("-")[1] +
                  cart_loc;
                $scope.allOrderDetails.push(itemDat);
                console.log("xxxxxx", $scope.allOrderDetails);
              });
            }
            $scope.showSelected = false;
          });
        // console.log("fffffffffffffffffffffff",$scope.allOrderDetails);
      });
      $http
        .post(__env.apiUrl + "/shipstation-order-details-by-ids", {
          api_key: UserProfile.getApiKey(),
          orderid_array: selected
        })
        .then(function(res) {
          var dt = res.data.message;
          dt.forEach(function(loc) {
            if (loc.items != undefined) {
              loc.items.forEach(itemLoc => {
                $scope.allOrderDetails.forEach((locationInDb, index) => {
                  if (locationInDb.sku == itemLoc.sku) {
                    $scope.allOrderDetails[index].location_db =
                      itemLoc.location_name;
                  }
                });
              });
            }
          });

          console.log("first list:", $scope.allOrderDetails);
          for (var j = 0; j < $scope.allOrderDetails.length; j++) {
            for (var i = 1; i < $scope.allOrderDetails.length; i++) {
              console.log("i=", i, "j=", j);
              if (i != j)
                if ($scope.allOrderDetails[i] && $scope.allOrderDetails[j]) {
                  if (
                    $scope.allOrderDetails[i].sku ==
                    $scope.allOrderDetails[j].sku
                  ) {
                    console.log(
                      "i.sku=",
                      $scope.allOrderDetails[i].sku,
                      "i.quantity:",
                      $scope.allOrderDetails[i].quantity,
                      "j.sku=",
                      $scope.allOrderDetails[j].sku,
                      "j.quantity:",
                      $scope.allOrderDetails[j].quantity
                    );
                    // console.log("1 occure")
                    var item = $scope.allOrderDetails[i];
                    console.log(item, $scope.allOrderDetails[i]);
                    var total_item =
                      $scope.allOrderDetails[i].quantity +
                      $scope.allOrderDetails[j].quantity;
                    var cart_loc =
                      $scope.allOrderDetails[i].cart_loc +
                      "," +
                      $scope.allOrderDetails[j].cart_loc;
                    // var cart_loc=["ininin","jdjd"]
                    // console.log($scope.allOrderDetails[i].sku, $scope.allOrderDetails[j].sku)
                    console.log($scope.allOrderDetails[j]);
                    $scope.allOrderDetails[j].quantity = total_item;
                    $scope.allOrderDetails[j].cart_loc = cart_loc;
                    $scope.allOrderDetails.splice(i, 1);
                    console.log("final list:", $scope.allOrderDetails);
                  }
                }
            }
          }
        });
      $scope.date = new Date();
    };

    $scope.loadorderDetails = function() {
      console.log("test5:", $scope.PcId);
      OrderService.getOrderDetailsByIds({
        orderid_array: selected,
        warehouse_id: $scope.PcId.warehouse_id
      }).then(response => {
        console.log("Response is ", response);
        if (response.data.error) {
          const message = $filter("lowercase")(response.data.message);
          console.log("Message ", message);
          if (message.indexOf("Transfer Item ") != -1) {
            var start = message.indexOf("");
            var end = response.data.message.length;
            const itemSku = response.data.message.substring(start + 4, end);
            AlertService.showErrorNotification(
              "Couldn't find the required amount of item with sku  " +
                itemSku +
                " on the warehouse",
              5000
            );
            $modalInstance.close();
          }
          if (message.indexOf("item is low") != -1) {
            var start = message.indexOf("low ");
            var end = response.data.message.length;
            const itemSku = response.data.message.substring(start + 4, end);
            AlertService.showErrorNotification(
              "Quantity of item with sku " +
                itemSku +
                " is not enough for the order picking. Please transfer some items from pallet",
              5000
            );
            $modalInstance.close();
          }
        } else {
          var cart_loc = 0;
          $scope.BinOrderArray = [];
          $scope.pickListData = [];
          $scope.allOrderDetails = response.data.data.order_details;
          $scope.allOrderDetails.forEach(element => {
            console.log("element:", element);
            cart_loc++;
            var itemArray = element.items;
            itemArray.forEach(item => {
              $scope.RequerdTot = $scope.RequerdTot + item.quantity;
              var existingData = $filter("filter")($scope.pickListData, {
                sku: item.sku
              });
              console.log("Exisintg data ", existingData);
              item.cart_loc = [];

              // console.log("$$$$$$$$$$$$$$$$$$$$4",item)
              var x = {};
              item.cart_loc[0] =
                item.quantity +
                " in " +
                $scope.PcId.location_name.split("-")[0] +
                "-" +
                cart_loc;
              x.bin_cart_location =
                $scope.PcId.location_name.split("-")[0] + "-" + cart_loc;
              x.order_id = element.orderId;
              x.cart_location = $scope.PcId.location_name;
              x.location_id = $scope.PcId.location_id;
              if (existingData.length == 0) {
                var itemLocation = $filter("filter")(
                  response.data.data.location_details,
                  {
                    sku: item.sku
                  }
                );
                item.item_location = itemLocation;
                item.pick_cart = $scope.PcId.location_name;
                $scope.pickListData.push(item);
              } else {
                for (var i = 0; i < $scope.pickListData.length; i++) {
                  if ($scope.pickListData[i].sku == item.sku) {
                    $scope.pickListData[i].cart_loc.push(
                      item.quantity +
                        " in " +
                        $scope.PcId.location_name.split("-")[0] +
                        "-" +
                        cart_loc
                    );
                    $scope.pickListData[i].quantity += item.quantity;
                  }
                }
              }
              // console.log("item is:",item);
              $scope.BinOrderArray.push(x);
              console.log("binorder", $scope.BinOrderArray);
              // console.log("cart loc:",item.cart_loc);
            });
          });
          console.log("Pick List Data ", $scope.pickListData);
          $scope.showSelected = false;
          $scope.showOrder = true;
          console.log("all order details:", $scope.allOrderDetails);
          $scope.orderArrayByBin = [];
          // $scope.allOrderDetails.forEach
        }
      });
    };

    $scope.savePickList = function() {
      $modalInstance.close();
      console.log("saved data:", $scope.pickListData);
      var total_count = 0;
      $scope.pickListData.forEach(val => {
        total_count += val.quantity;
      });
      console.log("pcid", $scope.PcId);
      OrderService.savePickList({
        data: $scope.pickListData,
        order_id: selected,
        total_count: total_count,
        location_id: $scope.PcId.location_id
      }).then(response => {
        console.log("Save Pick List Response ", response);
        if (response.data.error) {
          AlertService.showErrorNotification(
            "An error occured while saving the pick list. Please try again"
          );
        } else {
          AlertService.showSuccessNotification("Pick list saved successfully");
          $http
            .post(__env.apiUrl + "/shipstation-pick-list-binadd", {
              data: JSON.stringify($scope.BinOrderArray),
              api_key: UserProfile.getApiKey()
            })
            .then(response => {
              if (!response.data.error) {
                console.log(response.data);

                $state.go(
                  $state.current,
                  {},
                  {
                    reload: true
                  }
                );
              }
            });
        }
      });
    };
  }
]);

app.controller("ShippingControllers", [
  "$state",
  "$scope",
  "$uibModal",
  "UserProfile",
  "$http",
  "AlertService",
  "$compile",
  function($state, $scope, $modal, UserProfile, $http, AlertService, $compile) {
    $scope.clicked = false;

    // $scope.shippingMethods = [{
    //     value: 1,
    //     name: 'UPS'
    //   },
    //   {
    //     value: 2,
    //     name: 'FedEx'
    //   },
    //   {
    //     value: 3,
    //     name: 'DHL'
    //   },
    //   {
    //     value: 4,
    //     name: 'USPS'
    //   },
    //   {
    //     value: 5,
    //     name: 'LTL'
    //   },
    //   {
    //     value: 6,
    //     name: 'Pickup'
    //   }
    // ];

    $scope.dataTableOpt = {
      processing: true,
      language: {
        processing: "<img src='img/loading.gif' width='50px' height='50px'>"
      },
      serverSide: true,
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      pagingType: "simple_numbers",

      ajax: {
        url: __env.apiUrl + "/shipstation-shipped-orders",
        type: "POST",
        data: {
          api_key: UserProfile.getApiKey()
        }
      },
      columnDefs: [
        {
          targets: [0],
          className: "col-lg-3 col-md-3 col-sm-3",
          render: function(data, type, row) {
            return (
              '<span class="cursor-pointer" ng-click="viewOrderDetails(' +
              row[0] +
              "," +
              row[6] +
              ')" >' +
              data +
              "</span>"
            );
          }
        },
        {
          targets: [1],
          className: "col-lg-1 col-md-1 col-sm-1"
        },
        {
          targets: [2],
          className: "col-lg-1 col-md-1 col-sm-1"
        },
        {
          targets: [3],
          className: "col-lg-1 col-md-1 col-sm-1"
        },
        {
          targets: [4],
          className: "col-lg-2 col-md-2 col-sm-2"
        },
        {
          targets: [5],
          className: "col-lg-2 col-md-2 col-sm-2"
        }
      ],
      rowCallback: function(row, data, index) {
        $compile(row)($scope);
        return row;
      }
    };
    $scope.viewOrderDetails = function(orderiD, id) {
      console.log(orderiD, id);
      $state.go("app.order.shiped-order-details", { orderiD: orderiD, id: id });
    };
    $scope.openModel = function() {
      $modal.open({
        backdrop: "static",
        scope: $scope,
        templateUrl: "partials/take-orderId.html",
        controller: [
          "$scope",
          "$http",
          "$uibModalInstance",
          function($scope, $http, $modalInstance) {
            $scope.errorFlag = false;
            $scope.passId = function() {
              $http
                .post(
                  __env.apiUrl + "/shipstation-pick-list-binlist-cartlocation",
                  {
                    api_key: UserProfile.getApiKey(),
                    bin_cart_location: $scope.order_id
                  }
                )
                .then(function(response) {
                  console.log(
                    "Location Count Response ",
                    response.data.data.length == 0
                  );
                  if (response.data.data.length == 0) $scope.errorFlag = true;
                  else {
                    $modalInstance.close();
                    $scope.$parent.clicked = true;
                    $scope.$parent.orderDetail =
                      response.data.data[0].order_details;
                    $scope.$parent.allOrderDetails = response.data.data[0];
                  }
                });
              $scope.$parent.order_id = $scope.order_id;
            };
          }
        ]
      });
    };
    $scope.shipment = function() {
      $scope.openModel();
      $http
        .post(__env.apiUrl + "/shipstation-list-carrier", {
          api_key: UserProfile.getApiKey()
        })
        .then(function(res) {
          console.log("carriercode:", res.data);
          $scope.shippingMethods = res.data;
        });
    };
    $scope.listItem = function() {
      console.log($scope.itemList);
    };

    $scope.getItemCodes = function(val) {
      return $http
        .post(__env.apiUrl + "/purchase-searchsku", {
          api_key: UserProfile.getApiKey(),
          sku: val
        })
        .then(function(response) {
          console.log("Response SKU Search ", response);
          if (!response.data.error) return response.data.data;
        });
    };
    $scope.carrierSelected = false;
    $scope.vd = {};
    $scope.getServiceCode = function(val) {
      // console.log("test",$scope.shippingMethods)
      console.log("code", $scope.vd.carrier_code);
      $http
        .post(__env.apiUrl + "/shipstation-service-list", {
          api_key: UserProfile.getApiKey(),
          carrier_code: $scope.vd.carrier_code.code
        })
        .then(function(data) {
          console.log("servicecode", JSON.parse(data.data.message));
          $scope.serviceList = JSON.parse(data.data.message);
          $scope.carrierSelected = true;
        });
      $http
        .post(__env.apiUrl + "/shipstation-package-list", {
          api_key: UserProfile.getApiKey(),
          carrier_code: $scope.vd.carrier_code.code
        })
        .then(function(response) {
          $scope.packageList = response.data.message;
          console.log("packagecode", response.data.message);
        });
    };
    $scope.vs = {};
    var solution1 = function(base64Data) {
      var arrBuffer = base64ToArrayBuffer(base64Data);

      // It is necessary to create a new blob object with mime-type explicitly set
      // otherwise only Chrome works like it should
      var newBlob = new Blob([arrBuffer], { type: "application/pdf" });

      // IE doesn't allow using a blob object directly as link href
      // instead it is necessary to use msSaveOrOpenBlob
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
      }
      var data = window.URL.createObjectURL(newBlob);

      var link = document.createElement("a");
      document.body.appendChild(link); //required in FF, optional for Chrome
      link.href = data;
      // link.download = "file.pdf";
      link.click();
      window.URL.revokeObjectURL(data);
      link.remove();
    };
    var base64ToArrayBuffer = function(data) {
      var binaryString = window.atob(data);
      var binaryLen = binaryString.length;
      var bytes = new Uint8Array(binaryLen);
      for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
      }
      return bytes;
    };
    $scope.ship = function() {
      // console.log("all details:",$scope.allOrderDetails);
      var vm = {};
      vm.order_id = $scope.allOrderDetails.order_id;
      vm.carrierCode = "fedex";
      vm.serviceCode = "fedex_home_delivery";
      vm.packageCode = "cubic";
      vm.confirmation = $scope.confirm;
      vm.shipDate = new Date();
      vm.weight_value = $scope.weight;
      vm.weight_units = "ounces";
      vm.dimensions_units = null;
      vm.dimensions_length = null;
      vm.dimensions_width = null;
      vm.dimensions_height = null;
      vm.shipFrom_name = "Jason Hodges";
      vm.shipFrom_company = "ShipStation";
      vm.shipFrom_street1 = "2815 Exposition Blvd";
      vm.shipFrom_street2 = "Ste 2353242";
      vm.shipFrom_street3 = null;
      vm.shipFrom_city = "Austin";
      vm.shipFrom_state = "TX";
      vm.shipFrom_postalCode = "78703";
      vm.shipFrom_country = "US";
      vm.shipFrom_phone = null;
      vm.shipFrom_residential = false;
      vm.shipTo_name = $scope.orderDetail.shipTo.name;
      vm.shipTo_company = $scope.orderDetail.shipTo.company;
      vm.shipTo_street1 = $scope.orderDetail.shipTo.street1;
      vm.shipTo_street2 = $scope.orderDetail.shipTo.street2;
      vm.shipTo_street3 = null;
      vm.shipTo_city = $scope.orderDetail.shipTo.city;
      vm.shipTo_state = $scope.orderDetail.shipTo.state;
      vm.shipTo_postalCode = $scope.orderDetail.shipTo.postalCode;
      vm.shipTo_country = $scope.orderDetail.shipTo.country;
      vm.shipTo_phone = $scope.orderDetail.shipTo.phone;
      vm.shipTo_residential = null;
      vm.insuranceOptions = null;
      vm.internationalOptions = null;
      vm.advancedOptions = null;
      vm.testLabel = null;
      console.log(vm);
      // console.log($scope.vs.service_code.code);
      $http
        .post(__env.apiUrl + "/shipstation-create-shipment-label", vm)
        .then(function(res) {
          console.log(res.data.message);
          if (res.data.error) {
            AlertService.showErrorNotification(
              "An error occured while saving the pick list. Please try again"
            );
          } else {
            AlertService.showSuccessAlert(
              "order shipped sucessfully, Tracking number is" +
                res.data.message.trackingNumber,
              3000
            );
            $state.go($state.current, {}, { reload: true });
            solution1(res.data.message.lableData);
          }
        });
    };
  }
]);
app.controller("shipedOrderController", [
  "$scope",
  "$http",
  "UserProfile",
  "orderiD",
  "box_id",
  "$window",
  function($scope, $http, UserProfile, orderiD, box_id, $window) {
    console.log("modaal oppened and controller is working", orderiD);
    $scope.close = function() {
      $uibModalInstance.close();
    };

    $http
      .post(__env.apiUrl + "/shipstation-shipment-label-details", {
        order_id: orderiD,
        api_key: UserProfile.getApiKey(),
        box_id: box_id
      })
      .then(function(res) {
        console.log(res.data.message);
        $scope.shipping_details = res.data.message[0];
        $scope.boxNo =
          $scope.shipping_details.box_id +
          1 +
          " of " +
          $scope.shipping_details.box_count;
        getstoreName();
      });
    var getstoreName = function() {
      console.log($scope.shipping_details.box_content.items[0].store_id);
      $http
        .post(__env.apiUrl + "/shipstation-get-storename-by-id", {
          api_key: UserProfile.getApiKey(),
          store_id: $scope.shipping_details.box_content.items[0].store_id
        })
        .then(function(res) {
          $scope.storeName = res.data.data[0].store_name;
        });
    };
    $scope.ship = function() {
      var myBase64string = $scope.shipping_details.data.labelData;
      console.log("tttttttttttttttttttteeeeeeeee:", myBase64string);
      var objbuilder = "";
      objbuilder +=
        '<object width="100%" height="100%" data="data:application/pdf;base64,';
      objbuilder += myBase64string;
      objbuilder += '" type="application/pdf" class="internal">';
      objbuilder += '<embed src="data:application/pdf;base64,';
      objbuilder += myBase64string;

      objbuilder += '" type="application/pdf"  />';
      objbuilder += "</object>";
      objbuilder += '<object width="100%" height="100%" ';
      objbuilder += "<br><h1>haiin</h1>";
      objbuilder += "</object>";

      var win = $window.open("#", "_blank");
      var title = "Order Label";

      setTimeout(function() {
        win.document.write(
          "<html><title>" +
            title +
            '</title><body style="margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;">'
        );
        win.document.write(objbuilder);
        win.document.write("</body></html>");
      }, 1000);
      // layer = jQuery(win.document);
    };

    $scope.createBoxLabel = function() {
      setTimeout(() => {
        var printSection = document.getElementById("printElement").innerHTML;
        var pakageLabelWindow = $window.open(
          "",
          "",
          "left:100,width=1000,height=1000"
        );
        pakageLabelWindow.document.write("<html><head><title></title>");
        pakageLabelWindow.document.write(
          '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" />'
        );
        pakageLabelWindow.document.write(
          '<link href="../../../bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />'
        );
        pakageLabelWindow.document.write("</head><body >");
        pakageLabelWindow.document.write("</body></html>");

        pakageLabelWindow.document.write(printSection);
        setTimeout(function() {
          // necessary for Chrome

          pakageLabelWindow.print();
          return true;
        }, 3000);
      }, 2000);

      // $window.document.body.appendChild()
    };
  }
]);
