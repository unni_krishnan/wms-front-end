    'use strict';

    app.controller('DashboardLineCtrl', ['$scope','$uibModal', '$timeout','$http','UserProfile', function($scope,$modal, $timeout,$http,UserProfile) {

        // $http.post(__env.apiUrl+'/dashboard-orders-count',{api_key: UserProfile.getApiKey()}).then(function(res){
        //   console.log(res.data.data);
        //   $scope.shipment_data= res.data.data;

        // })
        // $http.post(__env.apiUrl+'/dashboard-location-count',{api_key: UserProfile.getApiKey()}).then(function(res){
        //   console.log(res.data.data);
        //   $scope.warehouse_data= res.data.data;
        //   $scope.whole_data=res.data.whole_data;
        // })
              $scope.home=true;

            $scope.clickFun=function(){
              console.log("haaaaaaaaaaaai");
            }


                $http.post(__env.apiUrl+'/dashboard-orders-count',{api_key: UserProfile.getApiKey()}).then(function(res){
                  // console.log(res.data.data);
                  $scope.shipment_data= res.data.data;

                })
                $http.post(__env.apiUrl+'/dashboard-location-count',{api_key: UserProfile.getApiKey()}).then(function(res){
                  // console.log("CHANGE:",res.data.data);
                  $scope.warehouse_data= res.data.data;
                  $scope.whole_data=res.data.whole_data;
                  // $scope.data=[]
                })
                $http.post(__env.apiUrl+'/dashboard-orders-days-count',{api_key:UserProfile.getApiKey()}).then(function(res){
                console.log('#############',res.data.data,res.data);
                  var data=res.data.data.awaiting_shipment;
                  var dataShipped=res.data.data.shipped;

                $scope.barData = [
                  { y: "1 Day", a: data[1].count},
                  { y: "2 Day", a: data[2].count},
                  { y: "3 Day", a: data[3].count},
                  { y: "4 Day", a: data[4].count},
                  { y: "5+ day", a: data[5].count}

              ];
              $scope.barDatashipped=[
                { y: "1 Day", a: dataShipped[1].count},
                { y: "2 Day", a: dataShipped[2].count},
                { y: "3 Day", a: dataShipped[3].count},
                { y: "4 Day", a: dataShipped[4].count},
                { y: "5+ day", a: dataShipped[5].count}

            ];
              $scope.labelData=['Count'];
                })
                //sparkline data for Orders Picked Per Hour
                $http.post(__env.apiUrl+'/dashboard-picked-count',{api_key: UserProfile.getApiKey()}).then(function(res){
                  // console.log("CHANGE:",res.data.data);
                  $scope.pick_sparkine_data=res.data.data;
                })
                $http.post(__env.apiUrl+'/dashboard-shipped-count',{api_key: UserProfile.getApiKey()}).then(function(res){
                  // console.log("CHANGE:",res.data.data);
                  $scope.shiped_per_hour_sparkine_data=res.data.data;
                })

                $scope.close=function(){
                  $scope.home=true;
                  $scope.show=[true,true,true];
                }
                $scope.show=[true,true,true];
                $scope.currentChart=0;
              $scope.homepageToogle=function(type){
                console.log("working")
                $scope.currentChart=type;
                //1: awaiting shipment
                //2: sipped
                //3: on-Hold
                if(type==1){
                  $scope.home=false;
                  $scope.getOrders(1)
                }
                if(type==2){
                  $scope.home=false;
                  $scope.getOrders(2)
                }
                if(type==3){
                  $scope.home=false;
                  $scope.getOrders(3)
                }

              }
              $scope.show_all=function(val){
                $scope.page=val
                $modal.open({
                  // backdrop: "static",
                  scope: $scope,
                  size:'lg',
                  templateUrl: "partials/reportDetails.html",
                  controller: [
                    "$scope",
                    "$http",
                    "$uibModalInstance",
                    function($scope, $modalInstance) {
                      $scope.cancel=function(){

                       console.log($scope)
                       $scope.$dismiss();
                      }
                    }
                  ]
                });
              }
              $scope.vm={}
              $scope.vm.rowsPerPage = 10;
              $scope.vm.currentPage = 1;
              $scope. getOrders= function(val) {
                var orderStatus;
                if (val == 1) {
                  $scope.show=[true,false,false];
                  orderStatus = "awaiting_shipment";
                } else if (val == 2) {
                  $scope.show=[false,true,false];
                  orderStatus = "shipped";
                } else if (val == 3) {
                  $scope.show=[false,false,true];
                  orderStatus = "on_hold";
                }
                console.log("current page", $scope.current_page);
                $http
                  .post(__env.apiUrl + "/shipstation-saved-order-list", {

                    current_page: ($scope.vm.currentPage - 1) * $scope.vm.rowsPerPage,
                    per_page: $scope.vm.rowsPerPage,
                    order_status: orderStatus,
                    api_key: UserProfile.getApiKey(),
                    store_id: ""
                  })
                  .then(function(response) {
                    console.log("List Reponse ", response);
                    $scope.vm.orderCount = 0;
                    if (!response.data.error) {
                      $scope.vm.orderCount = response.data.data.total_count;
                      $scope.orderList = response.data.data.orders;
                      var today = new Date();
                      $scope.orderList.forEach(function(single_orderdata, index) {
                        var date = new Date(single_orderdata.orderDate);
                        var msMinute = 60 * 1000,
                          msDay = 60 * 60 * 24 * 1000
                        var dateDiff = Math.floor((today - date) / msDay)
                        var timeDiff = Math.floor(((today - date) % msDay) / msMinute)
                        var totDiff = dateDiff + "d  " + Math.floor(timeDiff / 60) + "H";
                        $scope.orderList[index].age = totDiff;

                        $scope.orderList[index].tags = [];
                        if (single_orderdata.tagIds)
                          for (var i = 0; i < single_orderdata.tagIds.length; i++) {
                            var tagData = $filter("filter")(
                              response.data.data.tag_detils,
                              {
                                tag_id: single_orderdata.tagIds[i]
                              }
                            );
                            $scope.orderList[index].tags.push(tagData);
                          }
                        // })
                      });
                      console.log("order list", $scope.orderList);
                      // $scope.orderList.forEach(function(eachOrder,index){
                      // $scope.orderList.binId="Bin"+index+1
                      // })
                      // console.log("bin orderlist:", $scope.orderList,"bin"+1+1)
                    }
                  });
              };

            }]);



