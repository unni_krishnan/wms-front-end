'use strict';

/* Controllers */
app.controller('ItemController', ['$scope', '$stateParams', '$cookies', '$uibModal', '$compile', '$state', '$log', '$filter', 'UserProfile', 'ItemService', 'userData', 'AlertService',
  function ($scope, $stateParams, $cookies, $modal, $compile, $state, $log, $filter, UserProfile, ItemService, userData, AlertService) {

    $scope.vm = {};

    $scope.createItem = function () {
      var modalInstance = $modal.open({
        templateUrl: 'partials/item-add.html',
        controller: 'ItemModalController',
        size: 'lg',
        backdrop: 'static',
        resolve: {
          userData: function () {
            return userData;
          }
        }
      });
    }

    $scope.listItem = function (_itemId) {
      $state.go('app.item-detail', { itemId: _itemId });
    }

    $scope.dataTableOpt = {
      "processing": true,
      "language": {
        "processing": "<img src='img/loading.gif' width='50px' height='50px'>"
      },
      "serverSide": true,
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      "pagingType": "simple_numbers",

      "ajax": {
        "url": __env.apiUrl + '/item-list',
        "type": "POST",
        "data": {
          api_key: UserProfile.getApiKey()
        }
      },
      "columnDefs": [{
        "targets": [0],
        "className": "col-lg-3 col-md-3 col-sm-3",
      },
      {
        "targets": [1],
        "className": "col-lg-1 col-md-1 col-sm-1",
        render: function (data, type, full) {
          if (data == 1)
            return 'Customer';
          else if (data == 2)
            return 'Vendor';
        }
      },
      {
        "targets": [2],
        "className": "col-lg-2 col-md-2 col-sm-2",
      },
      {
        "targets": [3],
        "className": "col-lg-2 col-md-2 col-sm-2",
      },
      {
        "targets": [4],
        "className": "col-lg-1 col-md-1 col-sm-1",
      },
      {
        "targets": [5],
        "className": "col-lg-1 col-md-1 col-sm-1",
      },
      {
        "targets": [6],
        "sortable": false,
        "className": "col-lg-3 col-md-3 col-sm-3",
        render: function (data, type, full) {
          return '<span class="btn-primary btn-xs cursor-pointer" ng-click="listItem(' + data + ')"><i class="fa fa-eye hidden-xs" data-toggle="tooltip" data-placement="left" title="View"></i></span> &nbsp' +
            '<span class="btn-danger btn-xs cursor-pointer" ng-click="deleteItem(' + data + ')"><i class="fa fa-trash-o hidden-xs" data-toggle="tooltip" data-placement="left" title="Delete"></i></span> &nbsp';
        }
      }

      ],
      "rowCallback": function (row, data, index) {
        $compile(row)($scope);
        return row;

      }
    }

    $scope.deleteItem = function (itemId) {
      var modalInstance = $modal.open({
        templateUrl: 'partials/alert-warning.html',
        controller: 'WarningModalCtrl',
        backdrop: 'static',
        resolve: {
          data: function () {
            var warning = {};
            warning.title = 'Delete';
            warning.body = 'Are you sure you want to delete this item?';
            warning.showCancel = true;
            return warning;
          }
        }
      });

      modalInstance.result.then(function (selection) {
        // Warning accepted
        ItemService.deleteItem(itemId).then(function (response) {
          $log.debug("Delete Item Response ", response);
          if (!response.data.error)
            $state.go($state.current, {}, { reload: true });
          else
          AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        }, function (error) { AlertService.showErrorAlert(); });
      }, function () {
        //  Warning dismissed
      });
    }


  }
]);


/**
 * Controller for Modals in vendor page
 */
app.controller('ItemModalController', ['$scope', '$uibModalInstance', '$log', '$state', '$uibModal', 'UserProfile', 'ItemService', 'userData', 'AlertService', '$filter',
  function ($scope, $modalInstance, $log, $state, $modal, UserProfile, ItemService, userData, AlertService, $filter) {

    $scope.lotChecked = false;
    $scope.expDateChecked = false;
    $scope.serialChecked = false;
    $scope.rfidChecked = false;
    $scope.vm = {};
    $scope.vm.data = {};
    $scope.vm.data.upc = null;
    $scope.vm.data.sku = '';
    $scope.vm.data.ownership_transfer = new Date();
    $scope.vm.status = 1;
    $scope.vm.picture = '';
    $scope.userList = userData;
    $scope.userList.forEach(function (user) {
      if (user.type == 1) {
        user.id = user.customer_id;
        user.type_name = 'cus';
        user.name = user.customer_name;
      } else if (user.type == 2) {
        user.type_name = 'ven';
        user.name = user.vendor_name;
      }
    });
    $scope.vm.data.name_id = userData[0].id;
    $scope.vm.data.name_type = userData[0].type;
    $scope.ownershipDateOption = {
      maxDate: new Date(new Date().getFullYear() + 100, new Date().getMonth()),
      minDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 1),
    };

    $scope.saveItem = function () {
      $log.debug("POST Vendor data ", $scope.vm);
      if ($scope.ownershipTransfer && !$scope.vm.csv_file) {
        $scope.vm.data.owner_id = 0;
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate() + $scope.ownershipDays);
        $scope.vm.data.ownership_transfer = new Date(currentDate);
      } else if (!$scope.vm.csv_file) {
        $scope.vm.data.owner_id = $scope.vm.user.id;
      }
      if (!$scope.vm.csv_file) {
        $scope.vm.data.ownership_transfer = $filter('date')($scope.vm.data.ownership_transfer, "yyyy-MM-dd HH:mm:ss ");
        $scope.vm.data.name_id = $scope.vm.user.id;
        $scope.vm.data.name_type = $scope.vm.user.type;
        $scope.vm.data.name = $scope.vm.user.name;
      }

      $scope.vm.data.created_by = UserProfile.getUserProfile().id;
      $scope.vm.data.qty_added_by = UserProfile.getUserProfile().id;

      ItemService.createItem($scope.vm).then(function (response) {
        $log.debug("Add Vendor Response ", response);
        if (!response.data.error){
          AlertService.showSuccessNotification('Items have been added successfully');
          $state.go($state.current, {}, { reload: true });
        }
        else
          AlertService.showErrorNotification('Error!!. '+response.data.message);
      }, function (error) { AlertService.showErrorNotification("Sorry!! Something went wrong. "); });

      $modalInstance.close();

    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }
]);

app.controller('ItemDetailController', ['$scope', '$http', 'editableOptions', '$log', '$state', '$uibModal', '$filter', 'editableThemes', 'UserProfile', 'ItemService', 'itemData', 'userData', 'AlertService', '$sce', '$localStorage',
  function ($scope, $http, editableOptions, $log, $state, $modal, $filter, editableThemes, UserProfile, ItemService, itemData, userData, AlertService, $sce, $localStorage) {

    editableThemes.bs3.inputClass = 'input-sm';
    editableThemes.bs3.buttonsClass = 'btn-sm';
    editableOptions.theme = 'bs3';
    $scope.itemData = itemData[0];
    // console.log("ddddddddddd:",itemData)
    console.log(itemData[0].lot_flag);
    if(itemData[0].lot_flag==1){
      // console.log("flag is one")
      $scope.itemData.lot_flag=true
    }
    if(itemData[0].expiration_date_flag==1){
      // console.log("flag is one")
      $scope.itemData.expiration_date_flag=true
    }
    if(itemData[0].rfid_number_flag==1){
      // console.log("flag is one")
      $scope.itemData.rfid_number_flag=true
    }
    if(itemData[0].serial_number_flag==1){
      // console.log("flag is one")
      $scope.itemData.serial_number_flag=true
    }
    if ($scope.itemData.expiration_date)
      $scope.itemData.expiration_date = new Date($scope.itemData.expiration_date);

    if ($scope.itemData.name_type == 1)
      $scope.userData = $filter('filter')(userData, {
        customer_id: $scope.itemData.name_id,
        type: $scope.itemData.name_type
      }, true);
    else if ($scope.itemData.name_type == 2)
      $scope.userData = $filter('filter')(userData, {
        id: $scope.itemData.name_id,
        type: $scope.itemData.name_type
      }, true);

    $scope.itemTabIndex = $localStorage.itemDetailTabIndex ? $localStorage.itemDetailTabIndex : 0;

    $scope.setSelectedTab = function (index) {
      $scope.itemTabIndex = index;
      $localStorage.itemDetailTabIndex = index;

    }

    $scope.userData = $scope.userData[0];
    $scope.userList = userData;
    $scope.userList.forEach(function (user) {
      user.name = '';
      if (user.type == 1) {
        user.type_name = 'cus';
        user.name = user.customer_name;
      } else if (user.type == 2) {
        user.type_name = 'ven';
        user.name = user.vendor_name;
      }
    });

    if ($scope.itemData.picture)
      $scope.picturePath = __env.uploadUrl + '/items/' + $scope.itemData.picture;
    else
      $scope.picturePath = '';
    $scope.unitMeasure = [{ id: 1, text: 'Each' }, { id: 2, text: 'Case' }, { id: 3, text: 'Pallet' }];
    $scope.pickMethod = [{ id: 1, text: 'FIFO' }, { id: 2, text: 'FEFO' }, { id: 3, text: 'LIFO' }];
    $scope.cycleCount = [{ id: 1, text: 'Weekly' }, { id: 2, text: 'Monthly' }, { id: 3, text: 'Quarterly' }, { id: 4, text: 'Semi-Annual' }, { id: 5, text: 'Annual' }];

    $scope.updateItem = function () {
      var formData = {};
      formData.data = {};
      formData.data = $scope.itemData;
      formData.data.name_id = $scope.userData.id;
      formData.data.name_type = $scope.userData.type;
      formData.data.name = $scope.userData.name;
      formData.item_id = $scope.itemData.id;
      formData.picture = $scope.itemData.picture;
      $log.debug("User Data ", $scope.userData);
      ItemService.editItem(formData).then(function (response) {
        $log.debug("Update Item Response ", response);
        if (!response.data.error){
          AlertService.showSuccessNotification('Items have been changed successfully');
          $state.go($state.current, {}, { reload: true });}
        else
          AlertService.showErrorNotification( 'An error occured while updating item details. Please try again');
      }, function (error) { AlertService.showErrorNotification("Sorry!! Something went wrong. "); });
    }

    $scope.trustAsHtml = function (string) {
      return $sce.trustAsHtml(string);
    };


  }
]);
