'use strict';

/* Controllers */
app.controller('VendorController', ['$scope', '$stateParams', '$cookies', '$uibModal', '$compile', '$state', '$log', 'AlertService', 'UserProfile', 'VendorService',
  function ($scope, $stateParams, $cookies, $modal, $compile, $state, $log, AlertService, UserProfile, VendorService) {
    $scope.vm = {};

    $scope.addVendor = function () {
      var modalInstance = $modal.open({
        templateUrl: 'partials/vendor-add.html',
        controller: 'VendorModalCtrl',
        backdrop: 'static',
        size: 'lg',
        resolve: {
          vendorData: null
        }
      });

    }

    $scope.deleteVendor = function (vendorId) {
      var modalInstance = $modal.open({
        templateUrl: 'partials/alert-warning.html',
        controller: 'WarningModalCtrl',
        backdrop: 'static',
        resolve: {
          data: function () {
            var warning = {};
            warning.title = 'Delete';
            warning.body = 'Are you sure you want to delete this user?';
            warning.showCancel = true;
            return warning;
          }
        }
      });

      modalInstance.result.then(function (selection) {
        // Warning accepted
        VendorService.deleteVendor({
          vendor_id: vendorId
        }).then(function (response) {
          $log.debug("Delete Vendor Response ", response);
          if (!response.data.error)
            $state.go($state.current, {}, {
              reload: true
            });
          else
            AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        }, function (error) {
          AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        });
      }, function () {
        //  Warning dismissed
      });
    }

    $scope.editVendor = function (vendorId) {
      var modalInstance = $modal.open({
        templateUrl: 'partials/vendor-add.html',
        controller: 'VendorModalCtrl',
        backdrop: 'static',
        size: 'lg',
        resolve: {
          vendorData: function (VendorService) {
            return VendorService.listVendor({
              vendor_id: vendorId
            }).then(function (response) {
              if (response.data.error)
                AlertService.showErrorNotification("Sorry!! Something went wrong. ");
              else

                return response.data.data;
            }, function (error) {
              AlertService.showErrorNotification("Sorry!! Something went wrong. ");
            });
          }
        }
      });

    }

    $scope.viewVendorDetail = function (vendorId) {
      var modalInstance = $modal.open({
        templateUrl: 'partials/vendor-detail.html',
        controller: 'VendorModalCtrl',
        backdrop: 'static',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load('xeditable');
          }],
          vendorData: function (VendorService) {
            return VendorService.listVendor({
              vendor_id: vendorId
            }).then(function (response) {
              if (response.data.error)
                AlertService.showErrorNotification("Sorry!! Something went wrong. ");
              else {
                response.data.data.view = true;
                return response.data.data;
              }
            }, function (error) {
              AlertService.showErrorNotification("Sorry!! Something went wrong. ");
            });
          }
        }
      });
    }

    $scope.dataTableOpt = {
      "processing": true,
      "language": {
        "processing": "<img src='img/loading.gif' width='50px' height='50px'>" //add a loading image,simply putting <img src="loader.gif" /> tag.
      },
      "serverSide": true,
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      "pagingType": "simple_numbers",

      "ajax": {
        "url": __env.apiUrl + '/vendor-list',
        "type": "POST",
        "data": {
          api_key: UserProfile.getApiKey()
        },
      },
      "columnDefs": [{
          "targets": [0],
          // "className": "col-lg-2 col-md-2 col-sm-2",

        },
        {
          "targets": [1],
          // "className": "col-lg-2 col-md-2 col-sm-2",

        },
        {
          "targets": [2],
          // "className": "col-lg-2 col-md-2 col-sm-2",

        },
        {
          "targets": [3],
          // "className": "col-lg-1 col-md-1 col-sm-1",

        },
        {
          "targets": [4],
          // "className": "col-lg-1 col-md-1 col-sm-1",

        },
        {
          "targets": [5],
          // "className": "col-lg-1 col-md-1 col-sm-1",
          render: function (data, type, row) {
            if (data == 1)
              return '<span class="label label-success">Active</span>';
            else return '<span class="label label-default">Inactive</span>';
          }
        },
        {
          "targets": [6],
          "sortable": false,
          // "className": "col-lg-3 col-md-3 col-sm-3",
          render: function (data, type, full) {
            return '<span class="btn-primary btn-xs cursor-pointer data-toggle="tooltip" data-placement="left" title="View" ng-click="viewVendorDetail(' + data + ')"><i class="fa fa-info hidden-xs"></i> </span> &nbsp' +
              '<span class="btn-success btn-xs cursor-pointerdata-toggle="tooltip" data-placement="left" title="Edit" ng-click="editVendor(' + data + ')"><i class="fa fa-pencil hidden-xs"></i> </span> &nbsp' +
              '<span class="btn-danger btn-xs cursor-pointer data-toggle="tooltip" data-placement="left" title="Delete" ng-click="deleteVendor(' + data + ')"><i class="fa fa-trash hidden-xs"></i></span>';
          }
        }

      ],
      "rowCallback": function (row, data, index) {
        $compile(row)($scope);
        return row;

      }
    }
    $scope.init = function () {
      $scope.locationTypes.forEach(function (type) {
        if (type.type_id == $stateParams.typeId)
          $scope.type = type;
      }, this);
    }

  }
]);

/**
 * Controller for Modals in vendor page
 */
app.controller('VendorModalCtrl', ['$scope', '$uibModalInstance', '$log', '$state', '$uibModal', 'editableThemes', 'UserProfile', 'VendorService', 'vendorData', 'AlertService',
  function ($scope, $modalInstance, $log, $state, $modal, editableThemes, UserProfile, VendorService, vendorData, AlertService) {

    $scope.vm = {};
    $scope.vm.status = 1;
    $scope.active = [{
      value: 1,
      text: 'Active'
    }, {
      value: 0,
      text: 'Inactive'
    }];

    $scope.saveVendor = function (show) {
      $log.debug("POST Vendor data ", $scope.vm);

      if ($scope.vm.vendor_id) {

        // Update the data
        VendorService.editVendor($scope.vm).then(function (response) {
          $log.debug("Update Vendor Response ", response);
          if (!response.data.error)
            $state.go($state.current, {}, {
              reload: true
            });
          else
            AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        }, function (error) {
          AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        });
      } else {
        // Create new vendor
        $scope.vm.address_2 = $scope.vm.address_2 ? $scope.vm.address_2 : "";
        $scope.vm.created_by = UserProfile.getUserProfile().id;
        VendorService.createVendor($scope.vm).then(function (response) {
          $log.debug("Add Vendor Response ", response);
          if (!response.data.error)
            $state.go($state.current, {}, {
              reload: true
            });
          else
            AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        }, function (error) {
          AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        });
      }

      if (!show)
        $modalInstance.close();

    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    $scope.init = function () {
      if (vendorData) {
        $scope.vm.vendor_id = vendorData.vendor_id;
        $scope.vm.vendor_name = vendorData.vendor_name;
        $scope.vm.address_1 = vendorData.address_1;
        $scope.vm.address_2 = vendorData.address_2;
        $scope.vm.city = vendorData.city;
        $scope.vm.zip_code = vendorData.zip_code;
        $scope.vm.email = vendorData.email;
        $scope.vm.phone = vendorData.phone;
        $scope.vm.contact_person = vendorData.contact_person;
        if (vendorData.view)
          $scope.isView = true;
        else
          $scope.isView = false;
      }
    }
  }
]);
