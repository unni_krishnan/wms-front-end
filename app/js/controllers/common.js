//  Common Controller For Warning Modal 
app.controller('WarningModalCtrl', ['$scope', '$uibModalInstance', 'data', function ($scope, $modalInstance, data) {
  $scope.warning = data;
  $scope.ok = function () {
    $modalInstance.close('ok');
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}]);

app.controller('SuccessModalCtrl', ['$scope', '$uibModalInstance', '$timeout', 'data', function ($scope, $modalInstance, $timeout, data) {
  $scope.successMessage = data.message;

  $timeout(function () { $modalInstance.close('ok'); }, data.timeout * 1000);

  $scope.closeAlert = function () {
    $modalInstance.dismiss('cancel');
  };
}]);
