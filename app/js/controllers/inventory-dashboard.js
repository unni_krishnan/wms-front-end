'use strict';

/* Controllers */

app.controller('InventoryDashboardController', ['$scope', '$log', '$uibModal', function($scope, $log, $modal) {

    $scope.importInventory = function() {
        $modal.open({
            size: 'md',
            templateUrl: 'partials/inventory-add.html',
            controller: 'InventoryImportController'
        });
    }

}]);

app.controller('InventoryImportController', ['$scope', '$log', '$uibModalInstance', 'InventoryService', function($scope, $log, $modalInstance, InventoryService) {
    $scope.vm = {};
    $scope.cancel = function() {
        $modalInstance.close();
    }

    $scope.import = function() {
        InventoryService.importInventory($scope.vm).then(function(response) {
            $log.debug("Add Inventory ", response);
            if (!response.data.error)
                $state.go($state.current, {}, { reload: true });
            else
                AlertService.showErrorAlert('Error', 'An error occured while saving item details. Please try again', false);
        }, function(error) { AlertService.showErrorAlert(); });
    }

}])