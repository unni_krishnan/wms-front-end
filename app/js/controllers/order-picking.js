app.controller("orderPickingController", [
  "$scope",
  "$interval",
  "$stateParams",
  "$state",
  "$compile",
  "UserProfile",
  "$uibModal",
  "$filter",
  "AlertService",
  "OrderService",
  "$http",
  function(
    $scope,
    $interval,
    $stateParams,
    $state,
    $compile,
    UserProfile,
    $modal,
    $filter,
    AlertService,
    OrderService,
    $http
  ) {
    $scope.show_ = false;
    console.log("user", UserProfile.getUserProfile());
    console.log(
      "user",
      UserProfile.getUserProfile().first_name +
        " " +
        UserProfile.getUserProfile().last_name
    );
    var vf={};
    $scope.showPickOrder = false;
    $scope.pickCompletion = 0;
    $scope.showLocationflag = false;
    $scope.showBinflag = false;
    $scope.showSkuflag = false;
    $scope.scanItem = false;
    $scope.endflag = false;
    $scope.locationCount = 0;

    var sku_array = {};
    var location_array = {};
    var sorted_loc_array = {};
    var bin_array = {};
    var tot_array = {};
    var all_loc = [];
    $scope.picklistStatus = [];
    var sku;
    $scope.readloc;
    var locationName;
    $scope.locMsg;
    $scope.skuMsg;
    $scope.locMsg;

    var itemCount = 0;
    $scope.vm = {};
    $scope.labelName = "Scan Location";
    // $scope.locationList = ['A-01-02-03-04', 'A-01-02-03-05', 'A-01-02-03-06'];

    var callInterval = function() {
      // console.log("interval occuerd");
      $http
        .post(__env.apiUrl + "/shipstation-pick-list-count-list", {
          api_key: UserProfile.getApiKey()
        })
        .then(function(responce) {
          $scope.picklistStatus = responce.data.data;
          console.log("order status:",$scope.picklistStatus);
        });
    };
    var promis;
    // $scope.$on()
    promis = $interval(callInterval, 5000);

    $scope.$on("$destroy", function() {
      $interval.cancel(promis);
    });

    $scope.showPickListDetail = function(pc_id, data_id) {
      $scope.pc_id=pc_id;
      // $scope.orderDetails = [{
      //   "orderItemId": 453027848,
      //   "lineItemKey": "12916195354",
      //   "sku": "BPU-ADV-WMG-L",
      //   "name": "Women's Adventure Tee - Large / Military Green",
      //   "imageUrl": "https://cdn.shopify.com/s/files/1/1419/2870/products/BPU_Adventure_Tee_Military_Green.png?v=1476654508",
      //   "weight": {
      //     "value": 8,
      //     "units": "ounces",
      //     "WeightUnits": 1
      //   },
      //   "quantity": 3,
      //   "unitPrice": 25,
      //   "taxAmount": null,
      //   "shippingAmount": null,
      //   "warehouseLocation": null,
      //   "options": [],
      //   "productId": 29728559,
      //   "fulfillmentSku": null,
      //   "adjustment": false,
      //   "upc": "",
      //   "createDate": "2017-07-26T00:14:55.427",
      //   "modifyDate": "2017-07-26T00:14:55.427",
      //   "cart_loc": ["2 in Bin 1", "1 in Bin 2"],
      //   "item_location": [{
      //       "sku": "BPU-ADV-WMG-L",
      //       "location_name": "A-12-34-56-67",
      //       "quantity": 2
      //     },
      //     {
      //       "sku": "BPU-ADV-WMG-L",
      //       "location_name": "A-12-34-56-10",
      //       "quantity": 9
      //     }
      //   ],
      //   "pick_cart": "PCA73-10"
      // }, {
      //   "orderItemId": 453027849,
      //   "lineItemKey": "12916195355",
      //   "sku": "BPU-AAA-WMG-L",
      //   "name": "Women's Adventure Tee - Large / Military blue",
      //   "imageUrl": "https://cdn.shopify.com/s/files/1/1419/2870/products/BPU_Adventure_Tee_Military_Green.png?v=1476654508",
      //   "weight": {
      //     "value": 10,
      //     "units": "ounces",
      //     "WeightUnits": 2
      //   },
      //   "quantity": 2,
      //   "unitPrice": 25,
      //   "taxAmount": null,
      //   "shippingAmount": null,
      //   "warehouseLocation": null,
      //   "options": [],
      //   "productId": 29728559,
      //   "fulfillmentSku": null,
      //   "adjustment": false,
      //   "upc": "",
      //   "createDate": "2017-07-26T00:14:55.427",
      //   "modifyDate": "2017-07-26T00:14:55.427",
      //   "cart_loc": ["1 in Bin 1", "1 in Bin 2"],
      //   "item_location": [{
      //     "sku": "BPU-AAA-WMG-L",
      //     "location_name": "A-12-34-56-67",
      //     "quantity": 1
      //   }, {
      //     "sku": "BPU-AAA-WMG-L",
      //     "location_name": "A-12-34-56-00",
      //     "quantity": 5
      //   }],
      //   "pick_cart": "PCA74-1"
      // }]

      $scope.dataBaseId = data_id;

      $scope.orderDetails = [];
      $http.post(__env.apiUrl+'/shipstation-get-picklist',{api_key:UserProfile.getApiKey(),pickcart_id:pc_id}).then(function(res)
      {
        console.log("responseeee",res);
      if(res.data.error)
      {
      $scope.total_item_required = 0;
      console.log(
        "llllllllllllllllllllllllllllllllllllll",
        $scope.pickListDetails,
        pc_id
      );
      $scope.pickListDetails.forEach(function(element) {
        if (element[0].pick_cart === pc_id) {
          orderDetails = element;
        }
      }, this);

      $scope.pickCompletion = 0;
      $scope.orderDetails.push(orderDetails);
      // console.log("order det", $scope.orderDetails);
      var totQuantity;
      // var count=0;
      console.log("check1:", $scope.orderDetails[0]);
      $scope.orderDetails[0].forEach(function(element, index) {
        $scope.total_item_required += element.quantity;
        var q_loc;
        totQuantity = element.quantity;
        // console.log("chack2:", element.item_location);
        element.item_location.forEach(function(loc_q_array, key) {
          // console.log("chack3:", loc_q_array);
          if (totQuantity <= loc_q_array.quantity) {
            $scope.orderDetails[0][index].item_location[
              key
            ].quantity = totQuantity;

            $scope.orderDetails[0][index].item_location.splice(
              key + 1,
              $scope.orderDetails[0][index].item_location.length - index - 1
            );
            // console.log("total item:",totQuantity,"item in curent loc:",loc_q_array.quantity);
            // console.log("spliced")

          } else{
            console.log("index:",index,"current order",$scope.orderDetails[index]);
             totQuantity = totQuantity - $scope.orderDetails[0][index].item_location[key].quantity;
            }
        });
      });
      // console.log("array before process:", $scope.orderDetails);
      ///////////////////////////////////////////////////////
      //creating modified array for order picking process

      $scope.orderDetails[0].forEach(function(element, index) {
        ////sku_array creation
        sku_array[element.sku] = {};
        sku_array[element.sku].quantity_picked = 0;
        sku_array[element.sku].quantity_required = element.quantity;
        element.item_location.forEach(function(item_loc, key) {
          //location array

          if (location_array[item_loc.location_name]) {
            location_array[item_loc.location_name].push(element.sku);
          } else {
            all_loc.push(item_loc.location_name);
            location_array[item_loc.location_name] = [];
            location_array[item_loc.location_name].push(element.sku);
          }
          var eachdata = {};
          // eachdata.sku = item_loc.sku;
          eachdata[item_loc.sku] = [];
          eachdata[item_loc.sku].quantity = item_loc.quantity;
          if (tot_array[item_loc.location_name]) {
            tot_array[item_loc.location_name][item_loc.sku] = {
              quantity: item_loc.quantity
            };
          } else {
            tot_array[item_loc.location_name] = {};
            tot_array[item_loc.location_name][item_loc.sku] = {
              quantity: item_loc.quantity
            };
          }
        });
        // console.log(,"#####################")
        //bin array
        bin_array[element.sku] = [];
        var i = 0;
        for (i = 0; i < element.cart_loc.length; i++) {
          var temp = {};
          var splited = element.cart_loc[i].split(" ");
          temp.quantity_needed = splited[0];
          var tempcart = element.pick_cart.split("-");
          // console.log("tempcart:", tempcart);
          temp.cart_loc = splited[2];
          bin_array[element.sku].push(temp);
        }
      });
      //location array sorting

      var sorted = [];
      sorted = all_loc.sort();
      for (i = 0; i < sorted.length; i++) {
        sorted_loc_array[sorted[i]] = location_array[sorted[i]];
      }

      location_array = sorted_loc_array;
      // console.log("location array:", location_array);
      // console.log("all_loc array:", all_loc);
      // console.log("sorted array:", sorted_loc_array);
      // console.log("sku array:", sku_array);
      // console.log("bin array:", bin_array);
      // console.log("tot array", tot_array);
      ///////////////////////////////////test
      // var test_array=location_array;
      var tst = "BPU-AAA-WMG-L";
      // console.log(test_array)
      // var newary=[]
      // for (var k in test_array) {
      //   var i;
      // console.log(k[i])
      //   for (i = 0; i < k.length; i++) {
      //     if (k[i] === tst) {
      //       console.log("match fount")
      //       k.splice(tst, 1);
      //       newary.push(k)
      //     }

      //   }
      // };

      ////////////////////////////////////////////////////

      var pickListDetail = $modal.open({
        size: "xlg",
        // pickListDetails:$scope.pickListDetail,
        templateUrl: "partials/order-pick-list-detail.html",
        controller: [
          "$scope",
          "$uibModalInstance",
          function($scope, $modalInstance, OrderService) {
            $scope.close = function() {
              $modalInstance.close(false);
            };
            $scope.pickOrder = function() {
              $modalInstance.close(true);
            };

            // console.log($scope.$parent)
            var orderDetails;

            $scope.orderDetails = $scope.$parent.orderDetails;
            $scope.date = new Date();

            // console.log($scope.orderDetails)
          }
        ],
        scope: $scope
      });
      pickListDetail.result.then(function(showPickOrder) {
        $scope.showPickOrder = showPickOrder;
        $scope.showLocationflag = true;
        console.log("rrrrrrrrrrrrrr", $scope.pickListDetails);
      });
    }
    else{
      console.log("this area need to bee completed:",res.data.data[0]);
      var data=res.data.data[0];
       bin_array=JSON.parse(data.bin_data);
       tot_array=JSON.parse(data.total_data);
       sku_array=JSON.parse(data.sku_data);
       location_array=JSON.parse(data.location_data);
       var dat=JSON.parse(data.data);

      ///////////////////////////////////
      $scope.binMsg=dat.binMsg
      $scope.dataBaseId=dat.dataBaseId
      $scope.endflag=dat.endflag
      $scope.imageUrl=dat.imageUrl
      $scope.labelName=dat.labelName
      $scope.locMsg=dat.locMsg
      $scope.locationCount=dat.locationCount
      $scope.locationName=dat.locationName;
      $scope.orderDetails=dat.orderDetails
      $scope.pickCompletion=dat.pickCompletion
      $scope.pickListDetails=dat.pickListDetails
      $scope.picklistStatus=dat.picklistStatus
      $scope.scanItem=dat.scanItem
      $scope.showBinflag=false
      $scope.showLocationflag=true
      $scope.showPickOrder=true
      $scope.showSkuflag=dat.showSkuflag
      $scope.total_item_required=dat.total_item_required
      $scope.total_item_required=dat.vm
      all_loc=dat.all_loc;
      // console.log("sku array:", sku_array);
      // console.log("bin array:", bin_array);
      // console.log("tot array", tot_array);
      // console.log("location array",location_array)
      // console.log("all loc", all_loc)
      // console.log($scope)
    }
    });
  }
    $scope.displayloc = function() {
      $scope.locMsg = all_loc[0];
    };
    $scope.checkLocation = function(loc) {
      $scope.locationName = loc;
      // console.log("hai",location_array[$scope.location_name]);
      if (Object.keys(location_array).length === 0) {
        var vm = {};
        vm.pick_id = $scope.dataBaseId;
        vm.api_key = UserProfile.getApiKey();
        vm.user_name =
          UserProfile.getUserProfile().first_name +
          " " +
          UserProfile.getUserProfile().last_name;
        vm.user_id = UserProfile.getUserProfile().id;
        console.log("sending data:",$scope.vm)
        $http
          .post(__env.apiUrl + "/shipstation-pick-list-status-change", vm)
          .then(function(responce) {
            console.log(responce.data);
            if (responce.data.error == true) {
              $scope.showLocationflag = false;
              $scope.showSkuflag = false;
              $scope.showBinflag = false;
              $scope.errflag = true;
            } else {
              $scope.showLocationflag = false;
              $scope.showSkuflag = false;
              $scope.showBinflag = false;
              $scope.endflag = true;
            }
          });

        console.log(location_array);
        // $scope.location_name = all_loc[0];
      } else {
        console.log(
          "Is Location finished",
          !location_array[$scope.locationName]
        );
        if (!location_array[$scope.locationName]) {
          console.log("Location Finished");
          all_loc.splice(all_loc.indexOf($scope.locationName), 1);
          $scope.locMsg = all_loc[0];
          $scope.showLocationflag = true;
          $scope.showSkuflag = false;
          $scope.showBinflag = false;
        }
        else if (location_array[$scope.locationName].length === 0) {
          all_loc.splice(all_loc.indexOf($scope.locationName), 1);
          // console.log("all loc:", all_loc);

          // console.log("splicing all_loc:", all_loc, "--", $scope.locationName);
          $scope.locMsg = all_loc[0];
          // $scope.locationName = all_loc[0]; //remove for avoiding show in tb
          $scope.showLocationflag = true;
          $scope.showSkuflag = false;
          $scope.showBinflag = false;
        } else {
          $scope.skuMsg = location_array[$scope.locationName][0];
          console.log("img url", $scope.orderDetails.imageUrl);
          $scope.imageUrl = $scope.orderDetails.imageUrl;
          $scope.showSkuflag = true;
          $scope.showLocationflag = false;
          $scope.showBinflag = false;
        }
      }
    };
    $scope.goback = function() {
      $state.go(
        $state.current,
        {},
        {
          reload: true
        }
      );
    };

    $scope.checkSku = function(itemSku) {
      sku = itemSku;
      if (sku_array[sku].quantity_required === 0) {
        // console.log("location array in if:", location_array);
        Object.keys(location_array).forEach(function(key) {
          for (var i = 0; i < location_array[key].length; i++) {
            if (location_array[key][i] === sku) {
              location_array[key].splice(sku, 1);
            }
          }
          if (location_array[key].length == 0) {
            delete location_array[key];
          }
        });
        console.log("location array", location_array);
        $scope.checkLocation($scope.locationName);
      } else {
        console.log(tot_array)
        if (tot_array[$scope.locationName][sku].quantity === 0) {
          // console.log(tot_array[$scope.locationName][sku]);
          location_array[$scope.locationName].splice(
            [location_array[$scope.locationName].indexOf(sku)],
            1
          );
          // console.log("location array", location_array)
          ////splice tot array and asign locname
          $scope.checkLocation($scope.locationName);
        } else {
          $scope.binMsg = $filter("filter")(bin_array[sku], function(value) {
            if (value.quantity_needed != 0) return value;
          })[0].cart_loc;
          // console.log("bin array:", bin_array[sku])
          // console.log($scope.binMsg)
          $scope.showSkuflag = false;
          // $scope.showLocationflag = false;
          $scope.showBinflag = true;
        }
      }
    };

    $scope.checkBin = function() {
      sku_array[sku].quantity_required -= 1;
      sku_array[sku].quantity_picked += 1;
      tot_array[$scope.locationName][sku].quantity -= 1;
      bin_array[sku][0].quantity_needed -= 1;

      if (bin_array[sku][0].quantity_needed === 0) {
        // bin_array.splice[sku](0, 1);
        console.log("in spliced");
        bin_array[sku].splice(0, 1);
      }
      $scope.pickCompletion += 1;
      var vm = {};
      vm.pick_id = $scope.dataBaseId;
      vm.api_key = UserProfile.getApiKey();
      vm.count = $scope.pickCompletion;
      vm.location_name=$scope.locMsg;
      vm.sku=$scope.skuMsg;
      vm.user_name =
        UserProfile.getUserProfile().first_name +
        " " +
        UserProfile.getUserProfile().last_name;
      vm.user_id = UserProfile.getUserProfile().id;
      $http
        .post(__env.apiUrl + "/shipstation-pick-list-count-change", vm)
        .then(function(responce) {
          console.log("response from updating:", responce);
          $http.post(__env.apiUrl+'/location-change-count',{location_name:$scope.locMsg,
            sku:$scope.skuMsg,api_key:UserProfile.getApiKey()}).then(function(res){
              console.log(res);
            })
        });
      if (sku_array[sku].quantity_required === 0) {
        Object.keys(location_array).forEach(function(key) {
          for (var i = 0; i < location_array[key].length; i++) {
            if (location_array[key][i] === sku) {
              location_array[key].splice(sku, 1);
            }
          }
          if (location_array[key].length == 0) {
            delete location_array[key];
          }
        });
      }

      if (Object.keys(location_array).length === 0) {
        var vm = {};
        vm.pick_id = $scope.dataBaseId;
        vm.api_key = UserProfile.getApiKey();
        vm.user_name =
          UserProfile.getUserProfile().first_name +
          " " +
          UserProfile.getUserProfile().last_name;
        vm.user_id = UserProfile.getUserProfile().id;
        $http
          .post(__env.apiUrl + "/shipstation-pick-list-status-change", vm)
          .then(function(responce) {
            console.log(responce.data);
            if (responce.data.error == true) {
              $scope.showLocationflag = false;
              $scope.showSkuflag = false;
              $scope.showBinflag = false;
              $scope.errflag = true;
            } else {
              $scope.showLocationflag = false;
              $scope.showSkuflag = false;
              $scope.showBinflag = false;
              $scope.endflag = true;
            }
          });
      }
      console.log("////////////////////////////////////")
      console.log("$scope",$scope);
      vf.binMsg=$scope.binMsg
      vf.dataBaseId=$scope.dataBaseId
      vf.endflag=$scope.endflag
      vf.imageUrl=$scope.imageUrl
      vf.labelName=$scope.labelName
      vf.locMsg=$scope.locMsg
      vf.locationCount=$scope.locationCount
      vf.locationName=$scope.locationName
      vf.orderDetails=$scope.orderDetails
      vf.pickCompletion=$scope.pickCompletion
      vf.pickListDetails=$scope.pickListDetails
      vf.picklistStatus=$scope.picklistStatus
      vf.scanItem=$scope.scanItem;
      vf.showBinflag=$scope.showBinflag
      vf.showLocationflag=$scope.showLocationflag
      vf.showPickOrder=$scope.showPickOrder
      vf.showSkuflag=$scope.showSkuflag
      // vf.show_
      vf.total_item_required=$scope.total_item_required
      vf.vm=$scope.total_item_required
      vf.bin_array=bin_array;
      vf.all_loc=all_loc;
      var formData={};
      formData.data=vf;
      formData.pickcart_id=$scope.pc_id;
      formData.sku_data=sku_array;
      formData.bin_data=bin_array;
      formData.total_data=tot_array;
      formData.location_data=location_array;
      formData.api_key=UserProfile.getApiKey();
      // formData.all_data=all_loc
      $http({
        method: "POST",
        url: __env.apiUrl + "/shipstation-add-picklist",
        headers: {
          "Content-Type": undefined
        },
        data: formData,
        transformRequest: function(data, headersGetter) {
          var formData = new FormData();
          angular.forEach(data, function(value, key) {
            if (key == "data" ||key=="vf"||key=="sku_data"||key=="bin_data"||key=="total_data"||key=="location_data") {
              formData.append(key, JSON.stringify(value));
            } else formData.append(key, value);
          });
          return formData;
        }
      }).then(function(res){

      });
      $scope.checkLocation($scope.locationName);
    };

    OrderService.getPickList().then(response => {
      if (!response.data.error) {
        // console.log("Get Pick List ", response.data.data);
        $scope.pickListDetails = [];
        $scope.idArray = [];
        response.data.data.forEach(data => {
          $scope.pickListDetails.push(JSON.parse(data.data));
          $scope.idArray.push(JSON.parse(data.id));
        });

      }
    });
  }
]);
