app.controller('ShipStationController', ['$scope', '$state', '$http', '$compile', '$uibModal', 'UserProfile', 'ShipstationService', 'AlertService', function ($scope, $state, $http, $compile, $modal, UserProfile, ShipstationService, AlertService) {
        $scope.createItem = function () {
          var modalInstance = $modal.open({
            templateUrl: 'partials/shop-add.html',
            controller: 'ShipModalController',
            size: 'md',
            backdrop: 'static'
          });
        }
        // $scope.shipStation[];
        $scope.dataTableOpt = {
          "processing": true,
          "language": {
            "processing": "<img src='img/loading.gif' width='50px' height='50px'>"
          },
          "serverSide": true,
          "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
          ],
          "pagingType": "simple_numbers",

          "ajax": {
            "url": __env.apiUrl + '/shipstation-store-list',
            "type": "POST",
            "data": {
              api_key: UserProfile.getApiKey()
            }
          },
          "columnDefs": [{
              "targets": [0],
              render: function (data, type, row) {
                return '<span class="cursor-pointer" ng-click="viewItemDetails(' + row[1] + ')" >' + data + '</span>';
              },
              "className": "col-lg-3 col-md-3 col-sm-3",
            },
            {
              "targets": [1],
              "className": "col-lg-1 col-md-1 col-sm-1",

            },
            {
              "targets": [2],
              "className": "col-lg-2 col-md-2 col-sm-2",
            },
            {
              "targets": [3],
              "className": "col-lg-2 col-md-2 col-sm-2",
            },
            {
              "targets": [4],
              "className": "col-lg-1 col-md-1 col-sm-1",
            },
            {
              "targets": [5],
              "className": "col-lg-1 col-md-1 col-sm-1",
            },
            {
              "targets": [6],
              "sortable": false,
              // "className": "col-lg-3 col-md-3 col-sm-3",
              render: function (data, type, full) {
                return '<span class="btn-danger btn-xs cursor-pointer" data-toggle="tooltip" data-placement="left" title="Delete" ng-click="deleteItem(' + full[1] + ')"><i class="fa fa-trash-o hidden-xs"></i></span> &nbsp';
              }
            }

          ],
          "rowCallback": function (row, data, index) {
            $compile(row)($scope);
            return row;

          }
        }
        $scope.viewItemDetails = function (sId) {

          $state.go('app.store-detail', {
            storeId: sId
          });
        }
        $scope.deleteItem = function (id) {
          var formData = {}
          formData.store_id = id


          var modalInstance = $modal.open({
            templateUrl: 'partials/alert-warning.html',
            controller: 'WarningModalCtrl',
            backdrop: 'static',
            resolve: {
              data: function () {
                var warning = {};
                warning.title = 'Delete';
                warning.body = 'Are you sure ?, Do you want to delete this store ?';
                warning.showCancel = true;
                return warning;
              }
            }
          });

          modalInstance.result.then(function (selection) {
              // Warning accepted
              ShipstationService.deleteStore(formData).then(function (res) {
                  if (!res.data.error) {
                    AlertService.showSuccessNotification("Store Deleted Successfully")
                    $state.go($state.current, {}, {
                      reload: true
                    });
                  } else {
                    AlertService.showErrorNotification("Srorry, something went wrong!!! please try again");
                  }
                }, function (error) {
                  AlertService.showErrorNotification("Sorry!! Something went wrong !!! ");
                })},
                function () {
                  //  Warning dismissed
                });
              }
          }

        ])

      app.controller('ShipModalController', ['$scope', '$state', '$stateParams', '$http', '$uibModal', '$uibModalInstance', 'UserProfile', 'AlertService', 'filterFilter', function ($scope, $state, stateParams, $http, $modal, $uibModalInstance, UserProfile, AlertService, filterFilter) {

        api_key = UserProfile.getApiKey()
        $scope.customerList = [];
        $scope.cus = [];
        $scope.idlist = []
        $scope.namelist = []
        $http.post(__env.apiUrl + '/customer-list', {
          api_key: api_key
        }).then(function (res) {
          res.data.data.forEach(function (dat, index) {
            $scope.customerList.push(res.data.data[index][0])
            $scope.idlist.push({
              "name": res.data.data[index][0],
              "id": res.data.data[index][6]
            })
          }, this);
        })

        var formData = {};
        $scope.cus.id = "";
        $scope.errorFlag = false;

        console.log($scope.customerList, $scope.idlist);
        $scope.save = function () {
          $uibModalInstance.dismiss('cancel');
          $scope.idlist.forEach(function (val, index) {
            if ($scope.cus.id == val.name) {
              formData.customer_id = val.id;
              // $scope.errorFlag=false

            }
          })

          // console.log($scope.cus.id, $scope.storeId)

          formData.api_key = UserProfile.getApiKey();

          formData.store_id = $scope.storeId;
          // console.log(formData)
          $http.post(__env.apiUrl + '/shipstation-store-add', formData).then(function (res) {
            // console.log(res.data.error);
            if (!res.data.error) {
              AlertService.showSuccessNotification('Store has been added successfully', 3000);
              // $uibModalInstance.dismiss('cancel');
              $state.go($state.current, {}, {
                reload: true
              });

            } else {
              AlertService.showErrorNotification("Error during store creation!!");
              // $uibModalInstance.dismiss('cancel');
            }
          }, function (error) {
            AlertService.showErrorNotification("Sorry!! Something went wrong. ");
          })
        }
        $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
        };



        var count = false
        var keepLoop = true
        $scope.check = function () {
          $scope.idlist.forEach(function (val, index) {
            if (keepLoop) {
              if ($scope.cus.id != val.name)
                count = true;

              else {
                count = false
                keepLoop = false
              }
            }
          })
          if (count == false) {
            $scope.save();
          } else {
            $scope.cus.id = "";
            $uibModalInstance.dismiss('cancel');
            AlertService.showErrorNotification("Please choose a valid customer name");
          }


        }
      }])
      app.controller('StoreController', ['$scope', '$stateParams', '$state', '$compile', 'UserProfile', '$http', '$uibModal', '$filter',

        function ($scope, $stateParams, $state, $compile, UserProfile, $http, $modal, $filter) {


          console.log($stateParams.storeId, 'hai');

          $scope.store_Id = $stateParams.storeId;
          $scope.vm = {};
          $scope.vm.rowsPerPage = 10;
          $scope.currentPage = 1;
          $scope.tabIndex = 0;
          $scope.rows = [10, 50, 100];

          $scope.pageChanged = function () {
            var orderStatus;
            if ($scope.tabIndex == 0) {
              orderStatus = "awaiting_shipment";
            } else if ($scope.tabIndex == 1) {
              orderStatus = "shipped";
            } else if ($scope.tabIndex == 2) {
              orderStatus = "on_hold";
            } else if ($scope.tabIndex == 3) {
              orderStatus = "cancelled";
            }
            console.log('Store Id ', $scope.store_Id);
            console.log('Rows Page == ', $scope)

            $http.post(__env.apiUrl + '/shipstation-saved-order-list', {
              current_page: ($scope.currentPage-1) * $scope.vm.rowsPerPage,
              per_page: $scope.vm.rowsPerPage,
              order_status: orderStatus,
			                      api_key: UserProfile.getApiKey(),

              store_id: $scope.store_Id
            }).then(function (response) {
              console.log('List Reponse ', response);
              $scope.orderCount = 0;
              if (!response.data.error) {
                $scope.orderCount = response.data.data.total_count;
                $scope.orderList = response.data.data.orders;
                var today = new Date();
                $scope.orderList.forEach(function (single_orderdata, index) {
                  // val.orders.forEach(function(single_orderdata, id) {
                  var date = new Date(single_orderdata.orderDate);
                  var msMinute = 60 * 1000,
                    msDay = 60 * 60 * 24 * 1000
                  var dateDiff = Math.floor((today - date) / msDay)
                  var timeDiff = Math.floor(((today - date) % msDay) / msMinute)
                  var totDiff = dateDiff + "d  " + Math.floor(timeDiff / 60) + "H";
                  $scope.orderList[index].age = totDiff;
                  $scope.orderList[index].tags = [];
                  if (single_orderdata.tagIds)
                    for (var i = 0; i < single_orderdata.tagIds.length; i++) {
                      var tagData = $filter('filter')(response.data.data.tag_detils, {
                        tag_id: single_orderdata.tagIds[i]
                      })
                      $scope.orderList[index].tags.push(tagData);
                    }
                  // })

                })
                console.log("new orderlist:", $scope.orderList)
              }
            })
          };

          $scope.pageChanged();

          $scope.viewOrderDetails = function (orderId) {
            console.log(orderId);
            $modal.open({
              templateUrl: 'partials/order-detail.html',
              controller: ['$scope', 'orderDetail', '$uibModalInstance','UserProfile', function ($scope, orderDetail, $modalInstance,UserProfile) {
                $scope.orderDetail = orderDetail;
                console.log('Order detail', $scope.orderDetail);
                $scope.cancel = function () {
                  $modalInstance.close();
                }
                $scope.options = {
                  width: 2,
                  height: 30,
                  quite: 10,
                  displayValue: false,
                  font: "monospace",
                  textAlign: "center",
                  fontSize: 12,
                  backgroundColor: "",
                  lineColor: "#000"
                };
                $scope.showPickList = function () {
                  $modalInstance.close()
                  $modal.open({
                    templateUrl: 'partials/pick-cart-print.html',
                    size: 'lg',
                    backdrop: false,
                    controller: ['$scope', '$uibModalInstance', function ($scope, $mInstance) {
                      // $scope.$apply();
                      // console.log($scope);
                      $scope.closetab = function () {
                      }
                      $scope.date = new Date();
                      $scope.RequerdTot = 0
                      var orderData = $scope.$parent.$resolve.orderDetail
                      // console.log("ordr details",orderData)
                      $scope.orderID=orderData.orderId
                      $scope.orderItems = orderData.items
                      $scope.orderItems.forEach(function (val) {

                        $scope.RequerdTot = $scope.RequerdTot + val.quantity
                      })
                      var selected=[];
                      selected.push($scope.orderID)
                      $http.post(__env.apiUrl + '/shipstation-order-details-by-ids', {
                        api_key: UserProfile.getApiKey(),
                        orderid_array: selected
                      }).then(function (res) {
                        var dt=res.data.message;
                            // console.log("hai:",dt)
                          $scope.orderItems.forEach(function(val,index){
                            dt.forEach(function(pair){
                              pair.items.forEach(function(dat){
                                if(val.sku==dat.sku){
                                  // console.log(val.sku,dat.sku)
                                  $scope.orderItems[index].location_db=dat.location_name
                                // console.log($scope.orderItems[index])
                                }
                              })
                              // console.log(pair)

                            })
                          })
                        })

                      console.log($scope.orderItems)

                      // $scope.time=new Date().getTime()
                    }],
                    scope: $scope
                  })

                }
              }],
              size: 'xlg',
              resolve: {
                orderDetail: function () {
                  return $http.post(__env.apiUrl + '/shipstation-saved-order-list-by-id', {
                    api_key: UserProfile.getApiKey(),
                    order_id: orderId
                  }).then(function (response) {
                    console.log('Location Count Response ', response.data.data)
                    return response.data.data.detils[0];
                  });
                }
              }
            });
          }
        }
      ])
