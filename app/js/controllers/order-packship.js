app.controller("packShipController", [
  "$window",
  "$scope",
  "$interval",
  "$stateParams",
  "$state",
  "$compile",
  "UserProfile",
  "$uibModal",
  "$filter",
  "AlertService",
  "OrderService",
  "$http",
  function(
    $window,
    $scope,
    $interval,
    $stateParams,
    $state,
    $compile,
    UserProfile,
    $modal,
    $filter,
    AlertService,
    OrderService,
    $http
  ) {
    $scope.noitem = false;
    // $scope.
    $scope.shipping_test = false;
    $scope.picking = false;
    $scope.readbin = true;
    $scope.shipping = false;
    $scope.vm = {};
    $scope.vs = {};
    $scope.clicked = false;
    $scope.startDate;
    $scope.endDate;
    $scope.errorFlag = false;
    $scope.weWantPack = 0;
    $scope.passId = function() {
      console.log($scope.vs.cartloc);
      $http
        .post(__env.apiUrl + "/shipstation-list-verify-picklist", {
          api_key: UserProfile.getApiKey(),
          picklist_id: $scope.vs.cartloc
        })
        .then(function(res) {
          if (res.data.data.length == 0) {
            $http
              .post(
                __env.apiUrl + "/shipstation-pick-list-binlist-cartlocation",
                {
                  api_key: UserProfile.getApiKey(),
                  bin_cart_location: $scope.vs.cartloc
                }
              )
              .then(function(response) {
                console.log("Location Count Response ", response.data);
                if (response.data.data.length == 0) {
                  $scope.eFlag = true;
                  $scope.vm.cartloc = "";
                  AlertService.showErrorNotification(
                    "Entered Pick Cart is not valid!!"
                  );
                } else {
                  $scope.startDate = new Date();
                  $scope.picking = true;
                  $scope.readbin = false;
                  $scope.clicked = true;
                  $scope.orderDetail = response.data.data[0].order_details;
                  $scope.allOrderDetails = response.data.data[0];
                  $scope.orderDetail.items.forEach(function(item, $index) {
                    console.log("index:", $index);
                    $scope.orderDetail.items[$index].pickedCount = 0;
                    $scope.orderDetail.items[$index].verified = false;
                    $scope.orderDetail.items[$index].required_quantity =
                      $scope.orderDetail.items[$index].quantity;
                  }, this);
                }
              });
          } else {
            var temp = JSON.parse(res.data.data[0].data);
            console.log("temp:", temp);
            $scope.ContainerContents = temp.ContainerContents;
            $scope.allOrderDetails = temp.allOrderDetails;
            $scope.carrierSelected = temp.carrierSelected;
            $scope.clicked = temp.clicked;
            $scope.container = temp.container;
            $scope.containerList = temp.containerList;
            $scope.endFlag = temp.endFlag;
            $scope.exist = temp.exist;
            $scope.errorFlag = temp.errorFlag;
            $scope.noitem = temp.noitem;
            $scope.orderDetail = temp.orderDetail;
            $scope.packageCodeLoaded = temp.packageCodeLoaded;
            $scope.readsku = temp.readsku;
            $scope.picking = temp.picking;
            $scope.readbin = temp.readbin;
            $scope.serviceCodeLoaded = temp.serviceCodeLoaded;
            $scope.shipping = temp.shipping;
            $scope.startDate = temp.startDate;
            $scope.total_quantity = temp.total_quantity;
            $scope.savingData = temp.savingData;
            $scope.weWantPack = temp.weWantPack;
            $scope.packFlag = temp.packFlag;
            index = temp.index;
            pack = temp.pack;
            console.log("$scope:", $scope);
            packedItems = temp.packedItems;
          }
        });
    };

    var packedItems = [];
    var pack = {};
    $scope.packFlag = false;
    var Box;
    pack.items = [];
    $scope.container = false;
    $scope.ContainerContents = [];
    $scope.weWantPack = 0;

    $scope.savePacking = function() {
      // console.log("scope:", $scope);
      var savingData = {};
      savingData.ContainerContents = $scope.ContainerContents;
      savingData.allOrderDetails = $scope.allOrderDetails;
      savingData.carrierSelected = $scope.carrierSelected;
      savingData.clicked = $scope.clicked;
      savingData.container = $scope.container;
      savingData.containerList = $scope.containerList;
      savingData.endFlag = $scope.endFlag;
      savingData.exist = $scope.exist;
      savingData.errorFlag = $scope.errorFlag;
      savingData.noitem = $scope.noitem;
      savingData.orderDetail = $scope.orderDetail;
      savingData.packageCodeLoaded = $scope.packageCodeLoaded;
      savingData.readsku = $scope.readsku;
      savingData.picking = $scope.picking;
      savingData.readbin = $scope.readbin;
      savingData.serviceCodeLoaded = $scope.serviceCodeLoaded;
      savingData.shipping = $scope.shipping;
      savingData.startDate = $scope.startDate;
      savingData.total_quantity = $scope.total_quantity;
      savingData.vs = $scope.savingData;
      savingData.weWantPack = $scope.weWantPack;
      savingData.packFlag = $scope.packFlag;
      savingData.index = index;
      savingData.packedItems = packedItems;
      savingData.pack = pack;
      var formData = {};

      formData.data = savingData;
      formData.api_key = UserProfile.getApiKey();
      formData.picklist_id = $scope.vs.cartloc;
      console.log("formdata:", formData);
      $http({
        method: "POST",
        url: __env.apiUrl + "/shipstation-save-verify-picklist",
        headers: {
          "Content-Type": undefined
        },
        data: formData,
        transformRequest: function(data, headersGetter) {
          var formData = new FormData();
          angular.forEach(data, function(value, key) {
            if (key == "data") {
              formData.append(key, JSON.stringify(value));
            } else formData.append(key, value);
          });
          return formData;
        }
      }).then(function(res) {
        AlertService.showSuccessNotification("Picking saved!!");
      });
    };

    $scope.checkSku = function() {
      var vm = {};
      vm.sku = $scope.vm.readSku;
      vm.api_key = UserProfile.getApiKey();
      vm.pick_cart = $scope.vs.cartloc;
      $http
        .post(__env.apiUrl + "/shipstation-check-packing", vm)
        .then(function(res) {
          console.log(res.data);
          if ($scope.weWantPack == 0 && res.data.data != 1) {
            AlertService.showErrorNotification("Please choose a BOX first!!");
          } else if (res.data.data != 1) {
            $scope.weWantPack = 1;
            // console.log("packing box", $scope.packFlag);
            if (!$scope.packFlag) {
              pack.package = 0;
            }
            // pack.package = 0;
            var i;
            $scope.total_quantity = 0;
            console.log($scope.orderDetail);
            for (i = 0; i < $scope.orderDetail.items.length; i++) {
              $scope.total_quantity += $scope.orderDetail.items[i].quantity;
              console.log($scope.orderDetail.items[i].quantity);
            }
            console.log($scope.total_quantity);
            if ($scope.quantity == 0) {
              $scope.readSku = "";
              $scope.endFlag = true;
              $scope.endDate = new Date();
              var diff = $scope.startDate.valueOf() - $scope.endDate.valueOf();
              // var diffInHours = diff / 1000 / 60 / 60;
              // console.log("kooooooooooooooooooi", diffInHours);
              // console.log("finished:", packedItems);
              // if (pack.items.length > 0) {
              //   packedItems.push(pack);
              //   console.log("packedItems:", packedItems);
              // }
              $scope.clicked = false;
            } else {
              console.log("packedItedddddsgdms:", packedItems);
              for (i = 0; i < $scope.orderDetail.items.length; i++) {
                if ($scope.orderDetail.items[i].sku == $scope.vm.readSku) {
                  $scope.errorFlag = false;
                  if ($scope.orderDetail.items[i].quantity > 0) {
                    $scope.exist = true;
                    $scope.orderDetail.items[i].quantity -= 1;
                    $scope.orderDetail.items[i].pickedCount += 1;
                    if ($scope.orderDetail.items[i].quantity == 0) {
                      $scope.orderDetail.items[i].verified = true;
                    }
                    $scope.total_quantity -= 1;
                    console.log("item verified");
                    pack.items.push($scope.vm.readSku);

                    if ($scope.container) {
                      //if a conatiner isadded add this item into recent container
                      pushItem($scope.vm.readSku);
                    }
                    console.log("pack", pack);
                    $scope.vm.readSku = "";
                    console.log($scope.total_quantity);
                    if ($scope.total_quantity == 0) {
                      $scope.endFlag = true;
                      console.log("eekdkfd", pack);
                      if (pack.items.length > 0) {
                        packedItems.push(pack);
                        console.log("packedItems:", packedItems);
                      }
                      console.log("finished2:", packedItems);
                      $scope.orderDetail.items[i].verified = true;
                      savepaking(packedItems);
                      getCarrierCode();
                      $scope.endDate = new Date();
                      var msMinute = 60 * 1000,
                        msDay = 60 * 60 * 24 * 1000;
                      $scope.startDate = new Date($scope.startDate);
                      console.log($scope.endDate, $scope.startDate);

                      $scope.seconds = Math.floor(
                        (($scope.endDate - $scope.startDate) / 1000) % 60
                      );
                      $scope.minutes = Math.floor(
                        (($scope.endDate - $scope.startDate) / (1000 * 60)) % 60
                      );
                      $scope.hours = Math.floor(
                        (($scope.endDate - $scope.startDate) /
                          (1000 * 60 * 60)) %
                          24
                      );
                      console.log(
                        "new:",
                        $scope.seconds,
                        $scope.minutes,
                        $scope.hours
                      );
                      $scope.clicked = false;
                    } else {
                      $scope.clicked = true;
                      console.log("more item");
                      $scope.endFlag = false;
                      $scope.readSku = "";
                    }
                    break;
                  } else {
                    $scope.errorFlag = true;
                  }
                } else {
                  $scope.exist = false;
                }
              }
              if (!$scope.exist) {
                $scope.errorFlag = true;
                $scope.readSku = "";
              }
            }
          } else if (res.data.data == 1) {
            $scope.currentBox=res.data.location_item_id;
            $scope.weWantPack = 1;
            console.log("new box", pack.items.length);
            collectBoxInfo($scope.vm.readSku);
            if (pack.items.length > 0) {
              packedItems.push(pack);
              console.log("packedItemsss:", packedItems);
            }
            $scope.container = true;
            $scope.packFlag = true;
            pack = {};
            pack.items = [];
            pack.package = $scope.vm.readSku;
            $scope.vm.readSku = "";
          }
        });
    };
    var index = -1;
    var collectBoxInfo = function(sku) {
      $http
        .post(__env.apiUrl + "/purchase-searchsku", {
          api_key: UserProfile.getApiKey(),
          sku: sku
        })
        .then(function(res) {
          var testData = {};
          console.log("##################", res.data.data[0]);
          testData.items = [];
          testData.diamension =
            res.data.data[0].product_depth +
            " x " +
            res.data.data[0].product_height +
            " x " +
            res.data.data[0].product_weight;
          testData.name =
            res.data.data[0].sku + " - " + res.data.data[0].address;
          testData.visibility = false;
          testData.boxLength=res.data.data[0].product_weight;
          testData.boxHeight=res.data.data[0].product_height;
          testData.boxWidth=res.data.data[0].product_depth;
          testData.sku = res.data.data[0].sku;
          testData.shipTo = $scope.orderDetail.shipTo;
          testData.orderNumber = $scope.orderDetail.orderNumber;
          testData.shipDate = $scope.orderDetail.shipDate;
          testData.createDate = $scope.orderDetail.createDate;
          testData.orderNumber = $scope.orderDetail.orderNumber;
          testData.customerUsername = $scope.orderDetail.customerUsername;
          $scope.ContainerContents.push(testData);
          console.log("conatiner contets:",$scope.ContainerContents)
          index += 1;
        });
    };

    var pushItem = function(itemsku) {
      var find = false;
      if ($scope.ContainerContents[index].items.length == 0) {
        var testData = {};
        $scope.orderDetail.items.forEach(function(item) {
          if (item.sku == itemsku) {
            testData.sku = itemsku;
            testData.name = item.name;
            testData.count = 1;
            testData.image = item.imageUrl;
            testData.store_id = $scope.orderDetail.advancedOptions.storeId;
            $scope.ContainerContents[index].items.push(testData);
          }
        });
        // $scope.allContainerDeatils[index].items.push(itemsku)
      } else {
        $scope.ContainerContents[index].items.forEach(function(item, key) {
          if (item.sku == itemsku) {
            find = true;
            $scope.ContainerContents[index].items[key].count += 1;
          }
        });
        if (!find) {
          var testData = {};
          $scope.orderDetail.items.forEach(function(item) {
            if (item.sku == itemsku) {
              testData.sku = itemsku;
              testData.name = item.name;
              testData.count = 1;
              testData.image = item.imageUrl;
              testData.store_id = $scope.orderDetail.advancedOptions.storeId;
              $scope.ContainerContents[index].items.push(testData);
            }
          });
        }
      }
    };
    $scope.removeBox = function(id) {
      // index-=1
      console.log(id);
      if ($scope.ContainerContents[id] != undefined) {
        $http.post(__env.apiUrl+'/location-item-addition',{api_key:UserProfile.getApiKey(),location_item_id:$scope.currentBox}).then(function(res){
console.log("cover count increased by one");
        })
          console.log($scope.ContainerContents, "removed from container contets");

        if ($scope.ContainerContents[id].items.length > 0) {
          console.log($scope.ContainerContents[id].items);
          $scope.ContainerContents[id].items.forEach(function(val) {
            console.log("val:", val);
            $scope.orderDetail.items.forEach(function(item, ind) {
              if (item.sku == val.sku) {
                $scope.orderDetail.items[ind].quantity += val.count;
                $scope.orderDetail.items[ind].pickedCount -= val.count;
                $scope.orderDetail.items[ind].verified = false;
              }
            });
          });
        }
        packedItems = [];
        $scope.ContainerContents.splice(id, 1);
        if ($scope.ContainerContents.length == 0) {
          $scope.weWantPack = 0;
          $scope.container = false;
        }
        console.log("orderDetail", $scope.orderDetail);
        index -= 1;
      } else {
        console.log(pack, "removed from pack");
        pack = [];
      }
      console.log($scope);
    };
    $scope.removeItemFromBox = function(id, sku) {
      $scope.ContainerContents[id].items.forEach(function(val, ind) {
        if (val.sku == sku) {
          $scope.ContainerContents[id].items[ind].count -= 1;
          if ($scope.ContainerContents[id].items[ind].count == 0) {
            $scope.ContainerContents[id].items.splice(ind, 1);
          }
          $scope.orderDetail.items.forEach(function(dat, position) {
            if (dat.sku == sku) {
              $scope.orderDetail.items[position].quantity += 1;
              $scope.orderDetail.items[position].pickedCount -= 1;
              $scope.orderDetail.items[position].verified = false;
            }
          });
        }
      });
    };
    $scope.containerList = [];
    var savepaking = function(data) {
      //for making an array which contain list of sku of box
      console.log("data!!!!!!!", data);
      data.forEach(function(each) {
        if (each.package != 0) {
          $scope.containerList.push(each.package);
        }
      });
      console.log("container array", $scope.containerList);
      $scope.numberOfContainer = $scope.containerList.length;
      //savingitems and its corresponding continer
      formData = {};
      formData.data = data;
      formData.api_key = UserProfile.getApiKey();
      formData.order_id = $scope.allOrderDetails.order_id;
      console.log("saved packing data:", formData);
      $http
        .post(__env.apiUrl + "/shipstation-delete-verify-picklist", {
          api_key: UserProfile.getApiKey(),
          picklist_id: $scope.vs.cartloc
        })
        .then(function(res) {
          console.log("data deleted from temp store", res);
        });
      return $http({
        method: "POST",
        url: __env.apiUrl + "/shipstation-save-packeditems",
        headers: {
          "Content-Type": undefined
        },
        data: formData,
        transformRequest: function(data, headersGetter) {
          var formData = new FormData();
          angular.forEach(data, function(value, key) {
            if (key == "data") {
              formData.append(key, JSON.stringify(value));
            } else formData.append(key, value);
          });
          return formData;
        }
      });
    };
    $scope.changedBtn = [];
    $scope.back = function() {
      $scope.shipping = false;
      $scope.picking = true;
      $scope.readbin = false;
    };
    $scope.carrierSelected = false;
    $scope.vd = {};

    var getCarrierCode = function() {
      console.log("container contents:", $scope.ContainerContents);
      $scope.vm.weight = $scope.orderDetail.weight.value;
      $scope.vm.confirmation = $scope.orderDetail.confirmation;
      $http
        .post(__env.apiUrl + "/shipstation-get-storename-by-id", {
          api_key: UserProfile.getApiKey(),
          store_id: $scope.orderDetail.advancedOptions.storeId
        })
        .then(function(res) {
          $scope.storeName = res.data.data[0].store_name;
        });
      $http
        .post(__env.apiUrl + "/shipstation-list-carrier", {
          api_key: UserProfile.getApiKey()
        })
        .then(function(res) {
          console.log("carriercode:", res.data);
          $scope.shippingMethods = res.data;
          console.log("order detail", $scope.orderDetail);
          $scope.vd.carrier_code = $filter("filter")($scope.shippingMethods, {
            code: $scope.orderDetail.carrierCode
          })[0];
          console.log(
            "!!!!!!!!!!!!!!!!!!!!!!!!",
            $scope.vd.carrier_code,
            $scope.orderDetail.carrierCode
          );
          $scope.getServiceCode();
        });
    };
    $scope.serviceCodeLoaded = false;
    $scope.packageCodeLoaded = false;
    $scope.getServiceCode = function() {
      $scope.vm.unit = $scope.orderDetail.weight.units;
      // console.log("test",$scope.shippingMethods)
      console.log("code", $scope.vd.carrier_code);
      $http
        .post(__env.apiUrl + "/shipstation-service-list", {
          api_key: UserProfile.getApiKey(),
          carrier_code: $scope.vd.carrier_code.code
            ? $scope.vd.carrier_code.code
            : $scope.vd.carrier_code
        })
        .then(function(data) {
          console.log("servicecode", JSON.parse(data.data.message));
          $scope.carrierSelected = true;
          $scope.serviceList = JSON.parse(data.data.message);
          $scope.vs.service_code = $filter("filter")($scope.serviceList, {
            code: $scope.orderDetail.serviceCode
          })[0]
            ? $filter("filter")($scope.serviceList, {
                code: $scope.orderDetail.serviceCode
              })[0]
            : $scope.serviceList[0];
          $scope.serviceCodeLoaded = true;
          console.log(
            "#####",
            $scope.vs.service_code,
            $scope.orderDetail.serviceCode
          );
        });
      $http
        .post(__env.apiUrl + "/shipstation-package-list", {
          api_key: UserProfile.getApiKey(),
          carrier_code: $scope.vd.carrier_code.code
            ? $scope.vd.carrier_code.code
            : $scope.vd.carrier_code
        })
        .then(function(response) {
          $scope.packageList = JSON.parse(response.data.message);
          console.log("packagecode", $scope.packageList);
          $scope.vs.package = $filter("filter")($scope.packageList, {
            code: $scope.orderDetail.packageCode
          })[0];
          $scope.packageCodeLoaded = true;
          console.log($scope.vs.package);
        });
    };

    $scope.ship_order = function(id) {
      $scope.ContainerContents[id].visibility = true;
      $scope.PackingLabelInfo = $scope.ContainerContents[id];
      // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",$scope.PackingLabelInfo,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
      // $scope.shipping = true;
      // $scope.picking = false;
      // $scope.readbin = false;
      // $scope.vm.confirmation = $scope.orderDetail.confirmation;
      // $scope.vm.weight = $scope.orderDetail.weight.value;

      // $scope.allContainerDeatils = [];
      //     $http
      //       .post(__env.apiUrl + "/purchase-searchsku", {
      //         api_key: UserProfile.getApiKey(),
      //         sku:$scope.ContainerContents[id].sku
      //       })
      //       .then(function(res) {
      //         console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@2", res.data.data[0]);
      //         $scope.allContainerDeatils.push(res.data.data[0]);
      //       });
      $scope.vs = {};
    };
    $scope.getItemCodes = function(val) {
      return $http
        .post(__env.apiUrl + "/purchase-searchsku", {
          api_key: UserProfile.getApiKey(),
          sku: val
        })
        .then(function(response) {
          console.log("Response SKU Search ", response);
          if (!response.data.error) return response.data.data;
        });
    };
    var vm = {};
    $scope.vn = {};
    $scope.createBoxLabel = function(id) {
      console.log("id:", id);
      $scope.PackingLabelInfo = $scope.ContainerContents[id];
      $scope.PackingLabelInfo.boxNo =
        "Box " + (id + 1) + " of " + $scope.ContainerContents.length;
      setTimeout(() => {
        var printSection = document.getElementById("printElement").innerHTML;
        console.log("length of lable data:", $scope.ContainerContents.length);

        console.log("box no:", $scope.PackingLabelInfo.boxNo);
        var pakageLabelWindow = $window.open(
          "",
          "",
          "left:100,width=1000,height=1000"
        );
        pakageLabelWindow.document.write("<html><head><title></title>");
        pakageLabelWindow.document.write(
          '<link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" type="text/css" />'
        );
        pakageLabelWindow.document.write(
          '<link href="../../bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />'
        );
        pakageLabelWindow.document.write("</head><body >");
        pakageLabelWindow.document.write("</body></html>");

        pakageLabelWindow.document.write(printSection);
        setTimeout(function() {
          // necessary for Chrome

          pakageLabelWindow.print();
          return true;
        }, 1000);
      }, 1000);

      // $window.document.body.appendChild()
    };
    $scope.ship = function(id) {
      // console.log("all details:",$scope.allOrderDetails);
      console.log($scope.seconds, $scope.minutes, $scope.hours);
      console.log($scope)
      vm.duration =
        "H:" + $scope.hours + "-M:" + $scope.minutes + "-S:" + $scope.seconds;
      vm.company_name = $scope.storeName;
      vm.order_number = $scope.orderDetail.orderNumber;
      vm.employee =
        UserProfile.getUserProfile().first_name +
        " " +
        UserProfile.getUserProfile().last_name;
      console.log($scope.ContainerContents);
      console.log($scope.vm.confirmation);
      vm.box_content = $scope.ContainerContents[id];
      vm.box_sku = $scope.ContainerContents[id].sku;
      vm.box_id = id;
      vm.bin_name=$scope.vs.cartloc.split('-')[0];
      vm.bin_cart_location=$scope.vs.cartloc;
      vm.api_key = UserProfile.getApiKey();
      vm.order_id = $scope.allOrderDetails.order_id;
      vm.carrierCode = $scope.vd.carrier_code.code;
      vm.serviceCode = $scope.vs.service_code.code;
      // vm.packageCode = "cubic"
      vm.confirmation = $scope.vm.confirmation;
      vm.shipDate = new Date();
      vm.weight_value = $scope.vm.weight;
      vm.weight_units = $scope.vm.unit;
      vm.dimensions = {};
      vm.dimensions_length = $scope.ContainerContents[id].boxLength;
      vm.dimensions_width = $scope.ContainerContents[id].boxWidth;
      vm.dimensions_height =$scope.ContainerContents[id].boxHeight;
      vm.dimensions_units ='inches'
      // vm.shipFrom_name = "Jason Hodges"
      // vm.shipFrom_company = "ShipStation"
      // vm.shipFrom_street1 = "2815 Exposition Blvd"
      // vm.shipFrom_street2 = "Ste 2353242"
      // vm.shipFrom_street3 = null
      // vm.shipFrom_city = "Austin"
      // vm.shipFrom_state = "TX"
      // vm.shipFrom_postalCode = "78703"
      // vm.shipFrom_country = "US"
      // vm.shipFrom_phone = null;
      // vm.shipFrom_residential = false
      // vm.shipTo_name = $scope.orderDetail.shipTo.name
      // vm.shipTo_company = $scope.orderDetail.shipTo.company
      // vm.shipTo_street1 = $scope.orderDetail.shipTo.street1
      // vm.shipTo_street2 = $scope.orderDetail.shipTo.street2
      // vm.shipTo_street3 = null
      // vm.shipTo_city = $scope.orderDetail.shipTo.city
      // vm.shipTo_state = $scope.orderDetail.shipTo.state
      // vm.shipTo_postalCode = $scope.orderDetail.shipTo.postalCode
      // vm.shipTo_country = $scope.orderDetail.shipTo.country
      // vm.shipTo_phone = $scope.orderDetail.shipTo.phone
      // vm.shipTo_residential = null
      vm.insuranceOptions = null;
      vm.internationalOptions = null;
      vm.advancedOptions = null;
      vm.testLabel = true;
      console.log("this data is sending to create shipping label", vm);
      // console.log($scope.vs.service_code.code);
      $http
        .post(__env.apiUrl + "/shipstation-create-shipment-label-orders", vm)
        .then(function(res) {
          // console.log(res.data.message);
          if (res.data.error) {
            AlertService.showErrorNotification(
              "An error occured while saving the pick list. Please try again"
            );
          } else {
            $scope.back();
            AlertService.showSuccessNotification(
              "order label Obtained sucessfully, Tracking number is" +
                res.data.message.trackingNumber,
              20000
            );
            //for printing package lable in new window
            var myBase64string = res.data.message.labelData;
            console.log("tttttttttttttttttttteeeeeeeee:", myBase64string);
            var objbuilder = "";
            objbuilder +=
              '<object width="100%" height="100%" data="data:application/pdf;base64,';
            objbuilder += myBase64string;
            objbuilder += '" type="application/pdf" class="internal">';
            objbuilder += '<embed src="data:application/pdf;base64,';
            objbuilder += myBase64string;

            objbuilder += '" type="application/pdf"  />';
            objbuilder += "</object>";
            objbuilder += '<object width="100%" height="100%" ';
            objbuilder += "<br><h1>haiin</h1>";
            objbuilder += "</object>";

            var win = $window.open("#", "_blank");
            var title = "Order Label";

            setTimeout(function() {
              win.document.write(
                "<html><title>" +
                  title +
                  '</title><body style="margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;">'
              );
              win.document.write(objbuilder);
              win.document.write("</body></html>");
            }, 1000);
            layer = jQuery(win.document);
          }
        });
    };
  }
]);
