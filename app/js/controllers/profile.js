app.controller('ProfileController', ['$window', '$scope', '$log', '$state', '$uibModal', '$http', 'editableOptions', 'UserProfile', 'UserService', 'AlertService', 'companyDetail', function ($window, $scope, $log, $state, $modal, $http, editableOptions, UserProfile, UserService, AlertService, companyDetail) {

  $scope.profileData = UserProfile.getUserProfile();
  console.log($scope.profileData, "=====")

  var api_key = UserProfile.getApiKey()
  nameList = [];
  api_key = UserProfile.getApiKey()
  var data = $scope.profileData.user_warehouse_right.split(',');
  data.forEach(function (dat, index) {
    $http.post(__env.apiUrl + '/warehouse-edit', { 'api_key': api_key, 'warehouse_id': dat }).then(function (response) {
      nameList.push({ 'name': response.data.data.warehouse_name });
    })
  }, this);
  rightList = [];
  resp = [];
  var rights = $scope.profileData.user_right.split(',');
  $http.post(__env.apiUrl + '/user-getrights', { 'api_key': api_key }).then(function (res) {
    // resp.push(res.data.data);
    rights.forEach(function (r, index) {
      res.data.data.forEach(function (ri8, index) {
        if (r == ri8.id)
          rightList.push(ri8.permission);
      })
    })
  })
  $scope.rightList = rightList;
  $scope.nameList = nameList;
  $scope.profileImage;
  $scope.firstName = '';
  $scope.lastName = '';
  $scope.companyDetails = {};
  $scope.companyDetails.logo = "";
  $scope.companyDetails.email = "";
  $scope.companyDetails.website = "";
  if (companyDetail)
    $scope.companyDetails = companyDetail;
  editableOptions.theme = 'bs3';
  $log.debug('Comany Detaisl ', companyDetail);
  $log.debug("User Profile Data ", $scope.profileData);
  $scope.firstName = $scope.profileData.first_name;
  $scope.lastName = $scope.profileData.last_name;
  if ($scope.profileData.profile_image)
    $scope.profileImagePath = __env.uploadUrl + '/user/' + $scope.profileData.profile_image;
  if ($scope.companyDetails && $scope.companyDetails.logo)
    $scope.logoPath = __env.uploadUrl + '/user_companylogo/' + $scope.companyDetails.logo;

  $scope.updateCompanyProfile = function () {
    if ($scope.companyDetails && $scope.companyDetails.id) {
      $scope.companyDetails.company_id = $scope.companyDetails.id;
      $scope.companyDetails.user_id = $scope.profileData.id;
      $log.debug("Edit Company Post Data ", $scope.companyDetails)
      UserService.editCompany($scope.companyDetails).then(function (response) {
        $log.debug("Edit Company Response ", response);
        if (response.data.error) {
          AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        } else {
          AlertService.showSuccessNotification('Company Details have been updated successfully');
          $state.go($state.current, {}, { reload: true });
        }
      })
    } else {
      $scope.companyDetails.user_id = $scope.profileData.id;
      $log.debug("Create Company Post Data ", $scope.companyDetails)

      UserService.createCompany($scope.companyDetails).then(function (response) {
        $log.debug("Create Company Response ", response);
        if (response.data.error) {
          AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        } else {
          AlertService.showSuccessNotification('Company Details have been created successfully');
          $state.go($state.current, {}, { reload: true });
        }
      })
    }
  };

  $scope.updateUserProfile = function () {
    UserService.updateUserProfile({ user_id: $scope.profileData.id, first_name: $scope.firstName, last_name: $scope.lastName, profile_pic: $scope.profileImage }).then(function (response) {
      if (!response.data.error) {
        UserService.listUser({ user_id: $scope.profileData.id }).then(function (response) {
          if (!response.data.error) {

            $scope.profileData.first_name = response.data.data.first_name;
            $scope.profileData.last_name = response.data.data.last_name;
            $scope.profileData.profile_image = response.data.data.profile_image;
            var data = UserProfile.getUserProfile();
            console.log("current user profile data", data)
            data.first_name = response.data.data.first_name;
            data.last_name = response.data.data.last_name;
            data.profile_image = response.data.data.profile_image;
            UserProfile.setUserProfile(data);
            window.location.reload();
          }
        });

      }

    });
  }

  $scope.passwordChangeModel = function () {

    var modelInstance = $modal.open({
      templateUrl: 'partials/changepassword.html',
      controller: ['$scope', '$uibModalInstance', 'UserProfile', function ($scope, $modalInstance, UserProfile) {
        $scope.change = function () {
          $modalInstance.close();
          $scope.vm = {};
          $scope.profileData = UserProfile.getUserProfile();
          $scope.vm.user_id = $scope.profileData.id;
          $scope.vm.old_password = $scope.old_password;
          $scope.vm.new_password = $scope.new_password;
          UserService.resetPassword($scope.vm).then(function (res) {
            if (res.data.error) {
              AlertService.showErrorAlert("Wrong Password!!","Your current password is wrong!!");
            } else {
              AlertService.showSuccessNotification("Password changed successfully");
            }
          })
        };
        $scope.cancel = function () {
          $modalInstance.close();
        }
      }],
      backdrop: 'static',
    });
  }


}]);
