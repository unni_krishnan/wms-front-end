'use strict';
app.controller('RecevingController', ['$state', '$scope', '$uibModal', '$log', 'warehouseList', 'UserProfile', '$cookies', '$compile', '$filter', ReceivingCtrl])

/** @ngInject */
function ReceivingCtrl($state, $scope, $modal, $log, warehouseList, UserProfile, $cookies, $compile, $filter) {
    var vm = this;

    // $scope.warehouseList = warehouseList;

    $scope.addItemReceive = function(po_asn_number) {
        $state.go('app.inventory.ItemReceive', {
                asn_number: po_asn_number,
                warehouseList: warehouseList
            })
            //     $modal.open({
            //         templateUrl: 'partials/item-receive-add.html',
            //         controller: ['$scope', '$http', '$log', '$state', '$uibModalInstance', '$filter', 'UserProfile', '$parse', 'InventoryService', 'AlertService', 'warehouseList', 'id_number', 'notify',
            //             function($scope, $http, $log, $state, $modalInstance, $filter, UserProfile, $parse, InventoryService, AlertService, warehouseList, id_number, notify) {
            //                 $scope.files = {};
            //                 $scope.vm = {};
            //                 $scope.vm.data = {};
            //                 $scope.vm.data.re_boxing = false;
            //                 $scope.vm.data.re_palletizing = false;
            //                 $scope.vm.data.packaging = false;
            //                 $scope.showQntyError = false;
            //                 $scope.vm.data.time = false;
            //                 $scope.vm.close_po = 0;
            //                 $scope.warehouseList = warehouseList;
            //                 $scope.vm.data.po_number = id_number;
            //                 $scope.receiveByOptions = [{ value: 1, name: 'Close Out' }, { value: 2, name: 'Over Recieve' }, { value: 3, name: 'Receive In Full' }];
            //                 $scope.labelOptions = [{ value: 1, name: 'Box Label' }, { value: 2, name: 'Pallet Label' }, { value: 3, name: '  Item Labels' }];
            //                 $scope.vm.item_data = [];
            //                 $scope.vm.item_all_data = [];
            //                 $scope.quantityArray = [];
            //                 $scope.cancel = function() {
            //                     $modalInstance.close();
            //                 };

        //                 $scope.$watch('isClosePo', function(newVal) {
        //                     if (!newVal)
        //                         $scope.vm.close_po = 0;

        //                 }, true);
        //                 $scope.sendData = function() {
        //                     $scope.showQntyError = false;
        //                     var countDiff = false;
        //                     var closePo = false;
        //                     for (var i = 0; i < $scope.vm.item_all_data.length; i++) {
        //                         var diff = $scope.itemData[i].total_quantity - $scope.itemData[i].received_quantity;
        //                         if (!$scope.vm.item_all_data[i].quantity) {
        //                             $scope.vm.item_all_data[i].quantity = 0;
        //                         }
        //                         if ($scope.vm.item_all_data[i].quantity > diff) {
        //                             countDiff = true;
        //                             break;
        //                         } else
        //                             countDiff = false;
        //                     }
        //                     for (var i = 0; i < $scope.vm.item_all_data.length; i++) {
        //                         var totalDiff = parseInt($scope.itemData[i].total_quantity) - parseInt($scope.vm.item_all_data[i].quantity) - parseInt($scope.itemData[i].received_quantity);
        //                         if (totalDiff == 0) {
        //                             closePo = true;
        //                         } else {
        //                             closePo = false;
        //                             break;
        //                         }
        //                     }
        //                     if (countDiff) {
        //                         $scope.showQntyError = true;
        //                     } else {

        //                         // Entered Quantity is not greater than expected
        //                         if (closePo)
        //                             $scope.vm.close_po = 3;
        //                         if ($scope.files.bol)
        //                             $scope.vm.bill_count = $scope.files.bol.length;
        //                         if ($scope.files.packing_slip)
        //                             $scope.vm.packing_count = $scope.files.packing_slip.length;
        //                         if ($scope.files.qc_form)
        //                             $scope.vm.quality_count = $scope.files.qc_form.length;
        //                         if ($scope.files.pictures)
        //                             $scope.vm.pictures_count = $scope.files.pictures.length;

        //                         for (var i = 1; i <= $scope.vm.bill_count; i++) {
        //                             var model = $parse('bill_' + i);
        //                             model.assign($scope.vm, $scope.files.bol[i - 1]);
        //                         }

        //                         for (var i = 1; i <= $scope.vm.packing_count; i++) {
        //                             var model = $parse('packing_' + i);
        //                             model.assign($scope.vm, $scope.files.packing_slip[i - 1]);
        //                         }

        //                         for (var i = 1; i <= $scope.vm.quality_count; i++) {
        //                             var model = $parse('quality_' + i);
        //                             model.assign($scope.vm, $scope.files.qc_form[i - 1]);
        //                         }

        //                         for (var i = 1; i <= $scope.vm.pictures_count; i++) {
        //                             var model = $parse('pictures_' + i);
        //                             model.assign($scope.vm, $scope.files.pictures[i - 1]);
        //                         }

        //                         $scope.vm.generate_palletid = $scope.vm.generate_palletid == true ? 1 : 0;
        //                         $scope.vm.sku = '';
        //                         $scope.vm.quantity = '';

        //                         // angular.forEach($scope.itemData, function(value, key) {
        //                         // console.log('Key ', key);
        //                         // console.log('Value ', value);
        //                         // console.log('qnt array', $scope.quantityArray);
        //                         // $scope.vm.sku += value.sku;
        //                         // $scope.vm.quantity += $scope.quantityArray[key];
        //                         //     if (key != $scope.itemData.length - 1) {
        //                         //         $scope.vm.sku += ',';
        //                         //         $scope.vm.quantity += ',';
        //                         //     }
        //                         //     $scope.vm.item_data[key].quantity = $scope.quantityArray[key];
        //                         // });

        //                         InventoryService.receive($scope.vm).then(function(response) {
        //                             $log.debug("Item Receive Response ", response);
        //                             $modalInstance.close();
        //                             if (response.data.error)
        //                                 AlertService.showErrorAlert();
        //                             else {
        //                                 AlertService.showSuccessNotification('Items have been received successfully');
        //                                 $state.go($state.current, {}, { reload: true });

        //                                 if ($scope.vm.generate_palletid) {
        //                                     $modal.open({
        //                                         templateUrl: 'partials/pallet-id-print.html',
        //                                         controller: ['$scope', '$uibModalInstance', function($scope, $modalInstance) {
        //                                             $scope.responseData = response.data.data;
        //                                             for (var i = 0; i < response.data.data.length; i++) {
        //                                                 $scope.responseData[i].dateCreated = new Date(response.data.data[i].created_at);
        //                                             }
        //                                             $scope.options = {
        //                                                 width: 2,
        //                                                 height: 50,
        //                                                 quite: 10,
        //                                                 displayValue: false,
        //                                                 font: "monospace",
        //                                                 textAlign: "center",
        //                                                 fontSize: 12,
        //                                                 backgroundColor: "",
        //                                                 lineColor: "#000"
        //                                             };
        //                                             $scope.cancel = function() {
        //                                                 $modalInstance.close();
        //                                             }
        //                                         }]
        //                                     });
        //                                 }
        //                             }
        //                             // AlertService.showSuccessAlert('Item has been received successfully', 3);
        //                         }, function(response) {
        //                             $modalInstance.close();
        //                             AlertService.showErrorAlert();
        //                         });

        //                     }
        //                 };
        //                 $scope.printBoxLabel = function(item) {
        //                     console.log('Item ===== ', item);
        //                     var printContent = '';
        //                     var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";
        //                     var popupWin = window.open("", "_blank", winAttr);
        //                     printContent += '<div style="border-radius: 25px;background: #FFF;">';
        //                     printContent += '<table align="center" style="text-align:center;">';
        //                     printContent += '<tr><td colspan="3"><img src="../img/drive_logo.png"></td></tr>';
        //                     printContent += '<tr><td>' + $scope.vm.data.po_number + '</td><td></td><td>Date: ' + $filter('date')(new Date(), "MM/dd/yyyy") + '</td></tr>';
        //                     printContent += '<tr><td colspan="3">' + item.address + '</td></tr>';
        //                     printContent += '<tr><td colspan="3"><hr></td></tr>';
        //                     printContent += '<tr><td colspan="3">SKU# ' + item.sku + '</td></tr>';
        //                     printContent += '<tr><td colspan="3"><hr></td></tr>';
        //                     printContent += '<tr><td colspan="3">UPC#' + item.upc + '</td></tr>';
        //                     printContent += '<tr><td colspan="3"><hr></td></tr>';
        //                     printContent += '<tr><td colspan="3">QTY#' + item.quantity + '</td></tr>';
        //                     printContent += '</table></div>';

        //                     popupWin.document.open();
        //                     popupWin.document.write('<html><head>' +

        //                         '<style type="text/css">@media print {body { -webkit-print-color-adjust: exact;  -webkit-align-content: center; align-content: center; } }</style></head > <body onload="window.print();window.close();">' + printContent + '</body></html > ');
        //                     popupWin.document.close();
        //                     popupWin.focus();

        //                 };
        //                 $scope.printItemLabel = function(item) {
        //                     console.log('Print Item', item);
        //                     var printContent = '';
        //                     var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=10, height=auto, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";
        //                     var popupWin = window.open("", "_blank", winAttr);
        //                     printContent += '<div style="border-radius: 25px;background: #FFF;"><table align= "center" style="text-align:center;"><tr><td>' + item.address + '</td></tr > ';
        //                     printContent += '<tr><td>SKU# ' + item.sku + '</td></tr>';
        //                     printContent += '<tr><td>' + item.barcode + '</td></tr>';
        //                     printContent += '<tr><td>234234234234234234</td></tr></table></div>';

        //                     popupWin.document.open();
        //                     popupWin.document.write('<html><head><style type="text/css">@media print { body { -webkit-print-color-adjust: exact;  -webkit-align-content: center; align-content: center; background-color: #bfd3ea !important; } }</style></head><body onload="window.print();window.close();">' + printContent + '</body></html>');
        //                     popupWin.document.close();
        //                     popupWin.focus();


        //                 };


        //                 $scope.getItemDetails = function() {
        //                     console.log('Number  ===== ', $scope.vm.data.po_number)
        //                     if ($scope.vm.data.po_number)
        //                         $http.post(__env.apiUrl + '/getItemDetailsByponumber', { po_number: $scope.vm.data.po_number,api_key: UserProfile.getApiKey() }).then(function(response) {
        //                             $log.debug('Get Item Details Response Data ', response);
        //                             if (response.data.error) {} else {
        //                                 $scope.itemData = response.data.data.item_details;
        //                                 angular.forEach($scope.itemData, function(value, key) {
        //                                     console.log("val", value);
        //                                     $scope.vm.item_all_data[key] = {};
        //                                     $scope.vm.item_all_data[key].sku = value.sku;
        //                                     var qntyDiff = value.total_quantity - value.received_quantity;
        //                                     console.log('Diff ', qntyDiff);
        //                                     var _data = {};
        //                                     _data.id = value.id;
        //                                     _data.quantity = 0;
        //                                     _data.lot = '';
        //                                     _data.expiration_date = '';
        //                                     _data.serial_number = '';
        //                                     _data.rfid_number = '';
        //                                     $scope.vm.item_data.push(_data);
        //                                     if (value.lot_flag == 1 && qntyDiff != 0)
        //                                         $scope.showLot = true;
        //                                     if (value.expiration_date_flag == 1 && qntyDiff != 0)
        //                                         $scope.showExpirationDate = true;
        //                                     if (value.serial_number_flag == 1 && (qntyDiff != 0)) {
        //                                         $scope.showSerialNo = true;
        //                                     }
        //                                     if (value.rfid_number_flag == 1 && qntyDiff != 0)
        //                                         $scope.showRfid = true;
        //                                 });
        //                                 $log.debug('Item Data Array ', $scope.itemData);
        //                             }
        //                         });
        //                 };
        //                 if (id_number) {
        //                     $scope.getItemDetails();
        //                 }
        //             }
        //         ],
        //         resolve: {
        //             warehouseList: function() { return $scope.warehouseList; },
        //             id_number: function() { return po_asn_number; }
        //         },
        //         size: 'lg'
        //     });
    };


    $scope.purchaseOrderOpts = {
        dom: 'lrtip',
        serverSide: true,
        ajax: {
            "url": __env.apiUrl + '/purchase-list',
            "type": "POST",
            data: {
                // warehouse_id: $cookies.getObject("selectedWarehouse").warehouse_id,
                api_key: UserProfile.getApiKey(),
                show_open: 1

            }
        },
        columnDefs: [{
                "targets": [0],
                "className": "cursor-pointer",
            },
            {
                "targets": [1],
            },
            {
                "targets": [2],
                render: function(data, full, type) {
                    if (data == 1) return 'UPS';
                    else if (data == 2) return 'FedEx';
                    else if (data == 3) return 'DHL';
                    else if (data == 4) return 'USPS';
                    else if (data == 5) return 'LTL';
                    else if (data == 6) return 'Pickup';
                    else if (data == 7) return 'Other';
                }
            },
            {
                "targets": [3],
                render: function(data, full, type) {
                    data = new Date(data);
                    return $filter('date')(data, 'longDate');
                }
            },
            {
                targets: [4],
                render: function(data, full, type) {
                    var returnString = '';
                    for (var i = 0; i < data.length; i++) {
                        returnString += data[i].item_id;
                        if (i != data.length - 1)
                            returnString += ',';
                        else
                            returnString += '<br>';
                    }
                    return returnString;
                }
            }
        ],
        rowCallback: function(row, data, index) {
            $compile(row)($scope);
            $('td:nth-child(1)', row).bind('click', function() {
                $scope.$apply($scope.addItemReceive(data[0]));
            });
            return row;
        }
    };

    $scope.asnOpts = {
      dom: 'lrtip',
        "processing": true,
        "language": {
            "processing": "<img src='img/loading.gif' width='50px' height='50px'>"
        },
        "serverSide": true,
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        "pagingType": "simple_numbers",

        "ajax": {
            "url": __env.apiUrl + '/purchase-asnlist',
            "type": "POST",
            "data": {
                api_key: UserProfile.getApiKey(),
                show_open: 1
                    // warehouse_id: $cookies.getObject("selectedWarehouse").warehouse_id,
            }
        },
        "columnDefs": [{
                "targets": [0],
                "className": "cursor-pointer",
            },
            {
                "targets": [1],
            },
            {
                "targets": [2],
                render: function(data, full, type) {
                    if (data == 1) return 'UPS';
                    else if (data == 2) return 'FedEx';
                    else if (data == 3) return 'DHL';
                    else if (data == 4) return 'USPS';
                    else if (data == 5) return 'LTL';
                    else if (data == 6) return 'Pickup';
                    else if (data == 7) return 'Other';
                }
            },
            {
                "targets": [3],
                render: function(data, full, type) {
                    data = new Date(data);
                    return $filter('date')(data, 'longDate');
                }
            }, {
                targets: [4],
                render: function(data, full, type) {
                    var returnString = '';
                    for (var i = 0; i < data.length; i++) {
                        returnString += data[i].item_id;
                        if (i != data.length - 1)
                            returnString += ',';
                        else
                            returnString += '<br>';
                    }
                    return returnString;
                }
            }
        ],
        "rowCallback": function(row, data, index) {
            $compile(row)($scope);
            $('td:nth-child(1)', row).bind('click', function() {
                $scope.$apply($scope.addItemReceive(data[0]));
            });
            return row;

        }
    };

    $scope.refreshData = function() {
        $('#receiving').DataTable().ajax.reload(function(data) {
            console.log(data);
        });
    };

}
