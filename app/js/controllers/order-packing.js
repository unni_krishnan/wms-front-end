app.controller('orderPackingController', ['$scope', '$interval', '$stateParams', '$state', '$compile', 'UserProfile', '$uibModal', '$filter', 'AlertService', 'OrderService', '$http', function ($scope, $interval, $stateParams, $state, $compile, UserProfile, $modal, $filter, AlertService, OrderService, $http) {
  console.log("controller loaded");
  $http.post(__env.apiUrl + "/shipstation-pick-added-list", {
    api_key: UserProfile.getApiKey()
  }).then(function (responce) {
    if (!responce.data.error) {
      console.log('Get Pick List ', responce.data.data);
      $scope.pickListDetails = []
      $scope.idArray = []
      responce.data.data.forEach((data) => {
        $scope.pickListDetails.push(JSON.parse(data.data)[0]);
        $scope.idArray.push(JSON.parse(data.id));
      })
      console.log("picklist edited", $scope.pickListDetails);
      console.log("id array:", $scope.idArray)
    }
    $scope.showBin = false;

    $scope.showPickListDetail = function (pc_id, data_id) {
      //get savedcart location details with order details
      $http.post(__env.apiUrl + '/shipstation-pick-list-binlist', {
        api_key: UserProfile.getApiKey,
        cart_location: pc_id
      }).then(responce => {
        console.log("response is:", responce.data);
        $scope.savedBinList = responce.data.data;
        $scope.needed_progress = $scope.savedBinList.length;
        $scope.current_progress = 0
        console.log($scope.needed_progress, $scope.current_progress)
      })

      $scope.orderDetails = [];
      $scope.total_item_required = 0;

      $scope.rd.forEach(function (element) {
        if (element.pick_cart === pc_id) {
          orderDetails = element;

        }
      }, this);

      $scope.orderDetails.push(orderDetails);
      console.log("order det", $scope.orderDetails)
      var totQuantity;
      // var count=0;
      $scope.orderDetails.forEach(function (element, index) {
        $scope.total_item_required += element.quantity;
        var q_loc;
        totQuantity = element.quantity;
        element.item_location.forEach(function (loc_q_array, key) {
          if (totQuantity <= loc_q_array.quantity) {
            $scope.orderDetails[index].item_location[key].quantity = totQuantity;

            $scope.orderDetails[index].item_location.splice(key + 1, $scope.orderDetails[index].item_location.length - index - 1)
          } else
            totQuantity = totQuantity - $scope.orderDetails[index].item_location[key].quantity
        })

      })
      var pickListDetail = $modal.open({
        size: 'xlg',
        // pickListDetails:$scope.pickListDetail,
        templateUrl: 'partials/order-packing-details.html',
        controller: ['$scope', '$uibModalInstance', function ($scope, $modalInstance, OrderService) {
          $scope.close = function () {
            $modalInstance.close(false)
          }
          $scope.verify = function () {
            $modalInstance.close(true);
          }

          console.log($scope.$parent)
          var orderDetails;

          $scope.orderDetails = $scope.$parent.orderDetails
          $scope.date = new Date()
        }],
        scope: $scope

      });
      pickListDetail.result.then(function (showBin) {
        $scope.showBin = showBin;
        $scope.showBinFlag = true;
        console.log($scope.savedBinList)
      });
    };
    $scope.endFlag = false
    $scope.nobin = false;
    $scope.scanSkuFlag = false;
    $scope.notfound = false
    var currentOrder
    var sku;
    var currentItem

    $scope.progress = false;
    var i, j, currentOrder;
    $scope.found
    $scope.order_complete = false;
    $scope.binfount = false;
    $scope.vm = {}
    $scope.checkBin = function () {
      $scope.binfount = false;
      for (i = 0; i < $scope.savedBinList.length; i++) {
        console.log($scope.vm.readBin, $scope.savedBinList[i]);
        if ($scope.savedBinList[i].bin_cart_location == $scope.vm.readBin) {
          $scope.binfount = true;
          console.log("match");
          $scope.message = $scope.savedBinList[i].order_id;
          $scope.progress = true
          $scope.order_complete = false;
          // currentOrder = $scope.savedBinList[i].order_details.items;
          break;
        }
      }
      if ($scope.binfount) {
        $scope.scanSkuFlag = true;
        $scope.showBinFlag = false;
        $scope.nobin = false;
      } else {
        $scope.nobin = true;
        // $scope.showBinFlag = false;
        $scope.scanSkuFlag = false
      }
    }

    $scope.checkSku = function () {
      var found = false;
      if ($scope.savedBinList[i].order_details.items.length > 0) {
        for (j = 0; j < $scope.savedBinList[i].order_details.items.length; j++) {
          if ($scope.savedBinList[i].order_details.items[j].sku == $scope.vm.readSku) {
            found = true;
            $scope.savedBinList[i].order_details.items[j].quantity -= 1;
            console.log($scope.savedBinList[i].order_details.items[j].quantity);
            if ($scope.savedBinList[i].order_details.items[j].quantity == 0) {
              $scope.savedBinList[i].order_details.items.splice($scope.savedBinList[i].order_details.items.indexOf($scope.savedBinList[i].order_details.items[j]))
              console.log("array after spliced", $scope.savedBinList[i].order_details.items)
              if ($scope.savedBinList[i].order_details.items.length == 0) {
                $scope.savedBinList.splice(i, 1)
                if ($scope.savedBinList.length > 0) {
                  $scope.current_progress += 1
                  $scope.order_complete = true;
                  $http.post(__env.apiUrl+'/shipstation-pick-list-bin-status-change',{api_key:UserProfile.getApiKey(),order_id:$scope.message}).then(function(res){
                  if(res.error==true)
                    console.log("error wthile saving:",$scope.message)
                    else
                      console.log($scope.message,"- saved");
                  })
                  $scope.progress = false
                  $scope.vm.readBin = "";
                  $scope.vm.readSku="";
                  console.log($scope.savedBinList);
                  $scope.showBinFlag = true;
                  $scope.scanSkuFlag = false;
                } else {
                  $scope.current_progress += 1
                  $http.post(__env.apiUrl+'/shipstation-pick-list-bin-status-change',{api_key:UserProfile.getApiKey(),order_id:$scope.message}).then(function(res){
                    if(res.error==true)
                      console.log("error wthile saving:",$scope.message)
                      else
                        console.log($scope.message,"- saved");
                    })
                  $scope.endFlag = true;
                  $scope.progress = false;
                  $scope.scanSkuFlag = false;
                  // $scope.showBin = false
                  $scope.showBinFlag = false;
                  console.log("end");
                }
              }
            } else {
              console.log("read next sku");
              $scope.vm.readSku = ""
            }
            break;
          }
        }
        if (!found) {
          $scope.notfound = true;
        } else $scope.notfound = false
      } else {

        $scope.endFlag = true;
        $scope.scanSkuFlag = false;
      }
    }
    $scope.goback = function () {
      $state.go($state.current, {}, {
        reload: true
      });
    }


  })
}])












