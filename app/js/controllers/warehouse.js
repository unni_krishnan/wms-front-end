"use strict";

/* Controllers */
app.controller("WarehouseController", [
  "$scope",
  "$compile",
  "$http",
  "$state",
  "$cookies",
  "WarehouseService",
  "$uibModal",
  "UserProfile",
  "$log",
  function(
    $scope,
    $compile,
    $http,
    $state,
    $cookies,
    WarehouseService,
    $modal,
    UserProfile,
    $log
  ) {
    $scope.vm = {};
    $scope.vm.status = 1;
    $scope.warehouseList = [];
    $scope.addressPattern = /“/;

    $scope.dataTableOpt = {
      processing: true,
      language: {
        processing: "<img src='img/loading.gif' width='50px' height='50px'>" //add a loading image,simply putting <img src="loader.gif" /> tag.
      },
      serverSide: true,
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      pagingType: "simple_numbers",

      ajax: {
        url: __env.apiUrl + "/warehouse-list",
        type: "POST",
        data: {
          api_key: UserProfile.getApiKey()
        }
      },
      columnDefs: [
        {
          targets: [0]
          // "className": "col-lg-2 col-md-2 col-sm-2",
        },
        {
          targets: [1]
          // "className": "col-lg-2 col-md-2 col-sm-2",
        },
        {
          targets: [2]
          // "className": "col-lg-3 col-md-3 col-sm-3",
        },
        {
          targets: [3],
          // "className": "col-lg-2 col-md-2 col-sm-2",
          render: function(data, type, row) {
            if (data == 1)
              return '<span class="label label-success"> Active</span>';
            else return '<span class="label label-default">Inactive</span>';
          }
        },
        {
          targets: [4],
          className: "row",
          sortable: false,
          render: function(data, type, full) {
            return (
              '<div><span class="btn-primary btn-xs cursor-pointer margin-right-5 margin-top-5 col-lg-3 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip" data-placement="left" title="View" ng-click="showWarehouse(' +
              data +
              ')"><i class="fa fa-info "></i></span></div>' +
              '<div><span class="btn-success btn-xs cursor-pointer margin-right-5 margin-top-5 col-lg-3 col-md-3 col-sm-12 col-xs-12" data-toggle="tooltip" data-placement="left" title="Edit" ng-click="editWarehouse(' +
              data +
              ')"><i class="fa fa-pencil hidden-xs"></i></span></div>' +
              '<div><span class="btn-danger btn-xs cursor-pointer margin-top-5 col-lg-3 col-md-4 col-sm-12 col-xs-12" ng-click="deleteWarehouse(' +
              data +
              ')" data-toggle="tooltip" data-placement="left" title="Delete"><i class="fa fa-trash hidden-xs"></i></span></div>'
            );
          }
        }
      ],
      rowCallback: function(row, data, index) {
        $compile(row)($scope);
        $scope.warehouseList.push(data);
        $("td:nth-child(1)", row).addClass("cursor-pointer");
        $("td:nth-child(1)", row).unbind("click");
        $("td:nth-child(1)", row).bind("click", function() {
          $scope.$apply($scope.showWarehouse(data[4]));
        });

        return row;
      }
    };

    /**
         * Function to show detailed data of warehouse
         */
    $scope.showWarehouse = function(warehouseId) {
      $scope.warehouseList.forEach(function(warehouse) {
        if (warehouse[4] == warehouseId) {
          $cookies.put("warehouse_name", warehouse[1]);
          $state.go("app.warehouse.detail", {
            warehouseId: warehouseId
          });
        }
      }, this);
    };

    /**
         * Function to create a new warehouse
         */
    $scope.addWarehouse = function() {
      var modalInstance = $modal.open({
        templateUrl: "partials/warehouse-create.html",
        controller: "WarehouseModalCtrl",
        size: "lg",
        backdrop: "static",
        resolve: {
          warehouse: null,
          planList: function() {
            return WarehouseService.listPlan().then(function(response) {
              return response.data.data;
            });
          }
        }
      });
    };

    /**
         * Function to updaee the warehouse.
         * @param warehouseId Id of the warehouse to be updated
         */
    $scope.editWarehouse = function(warehouseId) {
      $modal.open({
        templateUrl: "partials/warehouse-edit.html",
        controller: "WarehouseModalCtrl",
        backdrop: "static",
        resolve: {
          planList: null,
          warehouse: function(WarehouseService) {
            return WarehouseService.listWarehouse(warehouseId).then(function(
              response
            ) {
              return response.data.data;
            });
          }
        }
      });
    };

    /**
         * Function to delete the warehouse.
         * @param warehouseId Id of the warehouse to be deleted
         */
    $scope.deleteWarehouse = function(warehouseId) {
      var modalInstance = $modal.open({
        templateUrl: "partials/alert-warning.html",
        controller: "WarningModalCtrl",
        backdrop: "static",
        resolve: {
          data: function() {
            var warning = {};
            warning.title = "Delete";
            warning.body = "Are you sure you want to delete this warehouse?";
            warning.showCancel = true;
            return warning;
          }
        }
      });
      modalInstance.result.then(
        function(selection) {
          // Warning accepted
          WarehouseService.deleteWarehouse({
            warehouse_id: warehouseId
          }).then(function(response) {
            $log.debug("Delete warehouse response ", response);
            if (response.data.error) {
              $modal.open({
                templateUrl: "partials/alert-warning.html",
                controller: "WarningModalCtrl",
                backdrop: "static",
                resolve: {
                  data: function() {
                    var warning = {};
                    warning.title = "Warning";
                    warning.body =
                      "It seems some of the locations inside the warehouse are occupied so you cannot delete the warehouse wholly.";
                    warning.showCancel = false;
                    return warning;
                  }
                }
              });
            } else {
              getWarehouseList();
              $state.go(
                $state.current,
                {},
                {
                  reload: true
                }
              );
            }
          });
        },
        function() {
          //  Warning dismissed
        }
      );
    };

    var getWarehouseList = function() {
      $http
        .post(__env.apiUrl + "/warehouse-getrights", {
          api_key: UserProfile.getApiKey()
        })
        .then(function(response) {
          if (!response.data.error) {
            var warehouseList = [];
            response.data.data.forEach(function(warehouse) {
              warehouseList.push({
                warehouse_id: warehouse.warehouse_id,
                warehouse_name: warehouse.warehouse_name
              });
            }, this);
            console.log("warehouseList of user:", warehouseList);
          }
          $cookies.remove("warehouseList");
          $cookies.remove("selectedWarehouse");
          $cookies.putObject("warehouseList", warehouseList);
        });
    };
  }
]);

app.controller("WarehouseModalCtrl", [
  "$scope",
  "$uibModalInstance",
  "$state",
  "$log",
  "$uibModal",
  "warehouse",
  "WarehouseService",
  "UserProfile",
  "AlertService",
  "planList",
  "$filter",
  function(
    $scope,
    $modalInstance,
    $state,
    $log,
    $modal,
    warehouse,
    WarehouseService,
    UserProfile,
    AlertService,
    planList,
    $filter
  ) {
    $scope.vm = {};
    $scope.warning = {};
    $scope.warning.title;
    $scope.warning.body;
    $scope.vm.warehouse_name;
    $scope.vm.address;
    $scope.vm.short_code;
    $scope.vm.created_user_id;
    $scope.vm.status = 1;
    $scope.showCancel = true;

    if (planList) {
      $scope.planList = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
      angular.forEach(planList, function(value, key) {
        $scope.planList = $filter("filter")($scope.planList, function(
          val,
          index,
          array
        ) {
          if (val != value.plan) return value;
        });
      });
    }

    $scope.saveWarehouse = function() {
      $modalInstance.close();
      if ($scope.vm.warehouse_id) {
        $scope.vm.warehouse_name = $scope.vm.warehouse_name.replace(/“/g, '"');
        $scope.vm.warehouse_name = $scope.vm.warehouse_name.replace(/”/g, '"');
        WarehouseService.editWarehouse($scope.vm).then(
          function(data) {
            $log.debug("Edit Warehouse Response", data);
            if (!data.data.error)
              $state.go($state.current, {}, { reload: true });
            else
              AlertService.showErrorNotification(
                "Sorry!! Something went wrong. "
              );
          },
          function(error) {
            AlertService.showErrorNotification(
              "Sorry!! Something went wrong. "
            );
          }
        );
      } else {
        $scope.vm.address_2 = $scope.vm.address_2 ? $scope.vm.address_2 : "";
        $scope.vm.created_user_id = UserProfile.getUserProfile().id;
        $scope.vm.warehouse_name = $scope.vm.warehouse_name.replace(/“/g, '"');
        $scope.vm.warehouse_name = $scope.vm.warehouse_name.replace(/”/g, '"');
        WarehouseService.createWarehouse($scope.vm).then(
          function(data) {
            $log.debug("Create Warehouse Response", data);
            if (!data.data.error)
              $state.go($state.current, {}, { reload: true });
            else
              AlertService.showErrorNotification(
                "Sorry!! Something went wrong. "
              );
          },
          function(error) {
            AlertService.showErrorNotification(
              "Sorry!! Something went wrong. "
            );
          }
        );
      }
    };

    $scope.cancel = function() {
      $modalInstance.dismiss("cancel");
    };

    $scope.init = function() {
      if (warehouse) {
        $scope.vm.warehouse_id = warehouse.warehouse_id;
        $scope.vm.warehouse_name = warehouse.warehouse_name;
        $scope.vm.address_1 = warehouse.address_1;
        $scope.vm.address_2 = warehouse.address_2;
        $scope.vm.city = warehouse.city;
        $scope.vm.zip_code = warehouse.zip_code;
        $scope.vm.short_code = warehouse.short_code;
        $scope.vm.created_user_id = 1;
        $scope.vm.status = warehouse.status;
      }
    };

    $scope.initWarning = function() {
      $scope.warning.title = "Delete";
      $scope.warning.body = "Are you sure you want to delete this warehouse?";
    };

    $scope.ok = function() {
      $modalInstance.close();
    };
  }
]);
