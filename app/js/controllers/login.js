'use strict';

/* Controllers */
// signin controller
app.controller('LoginFormController', ['$scope', '$http', '$state', '$log', '$cookies', 'AlertService', 'UserProfile',
    function($scope, $http, $state, $log, $cookies, AlertService, UserProfile) {
        $scope.flag = true;
        $scope.isCollapsed = false
        $scope.flagChange = function() {
            $scope.flag = false;
        }
        $scope.forgotPassword = function() {
            var formdata = {}
            formdata.email = $scope.email;
            $http.post(__env.apiUrl + '/user-forgot-password', formdata).then(function(res) {
                if (!res.data.error) {
                    $scope.isCollapsed = !$scope.isCollapsed;
                    // $scope.flag=true;
                } else {
                    AlertService.showErrorAlert(res.data.message,"Please check the address you are Provided");
                }
            })
        }
        $scope.user = {};
        $scope.authError = null;
        $scope.isRemember = false;
        var userProfile;
        var getWarehouseList = function() {
            $http.post(__env.apiUrl + '/warehouse-getrights', { api_key: UserProfile.getApiKey() }).then(function(response) {
                if (!response.data.error) {
                    var warehouseList = [];
                    if (userProfile.user_role != 1) {

                        response.data.data.forEach(function(warehouse) {
                            if (userProfile.user_warehouse_right.split(',').map(Number).indexOf(warehouse.warehouse_id) != -1) {
                                warehouseList.push({ "warehouse_id": warehouse.warehouse_id, "warehouse_name": warehouse.warehouse_name });
                            }
                        }, this);
                    } else if (userProfile.user_role === 1) {
                        response.data.data.forEach(function(warehouse) {
                            if (userProfile.user_warehouse_right.split(',').map(Number).indexOf(warehouse.warehouse_id) === -1) {
                                warehouseList.push({ "warehouse_id": warehouse.warehouse_id, "warehouse_name": warehouse.warehouse_name });
                            }
                        }, this);
                    }
                    if ($scope.isRemember) {
                        var expireDate = new Date();
                        expireDate.setDate(expireDate.getFullYear() + 1);
                        $cookies.putObject('warehouseList', warehouseList, { 'expires': expireDate });
                    } else {
                        $cookies.putObject('warehouseList', warehouseList);
                    }

                }

            });
        };

        $scope.login = function() {
            $scope.authError = null;
            // Try to login
            $http.post(__env.apiUrl + '/admin-login', {
                    username: $scope.user.username,
                    password: $scope.user.password
                })
                .then(function(response) {
                    $log.debug("Login Response ", response);
                    if (response.data.error) {
                        $scope.authError = response.data.message;
                    } else {
                        // Condition to apply only for non super admin
                        UserProfile.setUserProfile(response.data.data, $scope.isRemember);
                        // userProfile = response.data.data;
                        $log.debug('Login Response Data ', response.data.data);
                        // if (UserProfile.getUserProfile().user_role != 1)
                        // getWarehouseList();
                        $state.go('app.dashboard');
                        // else
                        // $state.go('app.dashboard');
                    }
                }, function(x) {
                    $scope.authError = 'Server Error';
                });
        };



    }
]);
