app.controller('PalletIdController', ['$scope', '$uibModal', '$log', 'LocationService', 'AlertService', '$compile', '$state', '$timeout', '$filter','UserProfile', function($scope, $modal, $log, LocationService, AlertService, $compile, $state, $timeout, $filter,UserProfile) {

    $scope.generatePalletId = function() {
        LocationService.generatePalletId().then(function(response) {
            $log.debug('Generate Pallet Id Response ', response);
            if (response.data.error)
                AlertService.showErrorAlert('Error', "Error occured while generating pallet id. Please try again!!", false);
            else
                AlertService.showSuccessNotification('Pallet Id has been generated successfully', 3000);
        });

    };

    $scope.assignToPallet = function() {
        $modal.open({
            templateUrl: 'partials/pallet-id-assign.html',
            controller: 'PalletIdAssignController',
            size: 'md'
        });

    };

    $scope.trackPalletId = function() {
        assignPallet
        $modal.open({
            templateUrl: 'partials/pallet-id-track.html',
            controller: 'PalletIdTrackController',
            size: 'md'
        });

    };

    $scope.dataTableOpts = {
        dom: 'lrtip',
        serverSide: true,
        ajax: {
            "url": __env.apiUrl + '/generate_palletlist',
            "type": "POST",
            data: function (d) {
              d.api_key=UserProfile.getApiKey();
            //   d.warehouse_id = $cookies.getObject("selectedWarehouse").warehouse_id,
            //     d.user_id = $scope.search.user_id || '',
            //     d.location_name = $scope.search.location_name || '',
            //     d.from_date = $filter('date')($scope.search.from_date, "yyyy-MM-dd HH:mm:ss ") || '',
            //     d.to_date = $filter('date')($scope.search.to_date, "yyyy-MM-dd HH:mm:ss ") || ''
            }
        },
        columnDefs: [{
                "targets": [0],
                render: function(data, full, type) {
                    return data;
                }
            },
            {
                "targets": [1],
                render: function(data, full, type) {
                    if (!data)
                        return '<span class="label label-default">Vaccant</span>';
                    else {
                        // var itemSkuArray = data.split(',');
                        var returnString = '';
                        for (var i = 0; i < data.length; i++)
                            returnString += 'SKU : ' + data[i].item_id + '&nbsp&nbsp Quantity : ' + data[i].quantity + '<br>';
                        returnString += '</table>';
                        return returnString;

                    }
                }
            },
            // {
            //     "targets": [2],
            //     render: function(data, full, type) {
            //         if (!data)
            //             return '<span class="label label-default">Vaccant</span>';
            //         else {
            //             var itemQntyArray = data.split(',');
            //             var returnString = '';
            //             for (var i = 0; i < itemQntyArray.length; i++)
            //                 returnString += itemQntyArray[i] + '<br>';
            //             return returnString;

            //         }
            //     }
            // },
            {
                targets: [2],
                render: function(data, full, type) {
                    if (!data)
                        return '<span class="label label-default">Not Yet Placed</span>';
                    else return data;
                }
            },
            {
                targets: [3],
                // className: "align-middle",
                orderable: false,
                render: function(data, full, type) {
                    var returnString = '<span class="btn-danger btn-sm cursor-pointer" ng-click="deletePallet(' + data + ')"><i class="fa fa-trash hidden-xs"></i> Delete Pallet</span> &nbsp';
                    returnString += '<span class="btn-info btn-sm cursor-pointer" ng-click="printPalletId(' + data + ')"><i class="fa fa-print hidden-xs"></i> Print Pallet</span> &nbsp';
                    return returnString;
                }
            }
        ],
        rowCallback: function(row, data, index) {
            $compile(row)($scope);
            return row;
        }
    };

    $scope.printPalletId = function(_palletId) {
        $modal.open({
                templateUrl: 'partials/pallet-id-print.html',
                controller: ['$scope', '$uibModalInstance', 'itemDetails', function($scope, $modalInstance, itemDetails) {
                    $scope.responseData = itemDetails;
                    for (var i = 0; i < itemDetails.length; i++) {
                        $scope.responseData[i].dateCreated = new Date(itemDetails[i].created_at);
                    }
                    console.log('response', itemDetails);
                    // $scope.dateCreated = $filter('date')(new Date(), "MM/dd/yyyy");
                    $scope.options = {
                        width: 2,
                        height: 50,
                        quite: 10,
                        displayValue: false,
                        font: "monospace",
                        textAlign: "center",
                        fontSize: 12,
                        backgroundColor: "",
                        lineColor: "#000"
                    };
                    $scope.cancel = function() {
                        $modalInstance.close();
                    }
                }],
                resolve: {
                    itemDetails: function(LocationService) {
                        return LocationService.getPalletIdBarcode(_palletId).then(function(_response) {
                            return _response.data.data;
                        });
                    }
                }
            })
            // var printContent = '';
            // var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";
            // var popupWin = window.open("", "_blank", winAttr);
            // LocationService.getPalletIdBarcode(_palletId).then(function(response) {
            //     console.log('response ', response);
            //     if (!response.data.error) {
            //         printContent += '<div><table align="center" style="text-align:center;"><tr><td><img src="../img/logo-folded.png"></td><td>Pallet ID :' + _palletId + '</td></tr><tr><td colspan="2"><hr></td></tr>';
            //         printContent += '<tr><td colspan="2">Date Created : ' + $filter('date')(new Date(), "MM/dd/yyyy") + '</td></tr><tr><td colspan="2"><hr></td></tr>';
            //         // printContent += '<tr><td>Item Numbers : </td><td> 123456, 12323464, 55641561 </td></tr>';
            //         printContent += '<tr><td></td><td style="padding-top:30px;">' + response.data.data[0].barcode + '</td></tr></table></div>';

        //         popupWin.document.open();
        //         popupWin.document.write('<html><head><style type="text/css">@media print { body { -webkit-print-color-adjust: exact;  -webkit-align-content: center; align-content: center; } }</style></head><body onload="window.print();window.close();">' + printContent + '</body></html>');
        //         popupWin.document.close();
        //         popupWin.focus();
        //     }
        // });

    };

    $scope.deletePallet = function(_palletId) {
        $log.debug("location Id ", _palletId);
        var modalInstance;
        modalInstance = $modal.open({
            templateUrl: 'partials/alert-warning.html',
            controller: 'WarningModalCtrl',
            resolve: {
                data: function() {
                    var warning = {};
                    warning.title = 'Delete';
                    warning.body = 'Are you sure you want to delete this pallet?';
                    warning.showCancel = true;
                    return warning;
                }
            }
        });

        modalInstance.result.then(function(selection) {
            // Warning accepted

            LocationService.deletePalletId(_palletId).then(function(response) {
                $log.debug("Delete single location server response ", response);
                if (response.data.error) {
                    AlertService.showErrorAlert();
                } else {
                    AlertService.showSuccessNotification('Pallet have been deleted successfully', 3000);
                    $timeout(function() {
                        $state.go($state.current, {}, { reload: true });
                    }, 1000);
                }

            });
        }, function() {
            //  Warning dismissed
        });

    };

}]);

app.controller('PalletIdTrackController', ['$scope', '$uibModalInstance', '$log', '$state', '$uibModal', '$http', 'UserProfile', 'AlertService', function($scope, $modalInstance, $log, $state, $modal, $http, UserProfile, AlertService) {
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}]);

app.controller('PalletIdAssignController', ['$scope', '$uibModalInstance', '$log', '$state', '$uibModal', '$http', 'UserProfile', 'AlertService', 'LocationService', function($scope, $modalInstance, $log, $state, $modal, $http, UserProfile, AlertService, LocationService) {

    $scope.vm = {};
    $scope.vm.data = {};
    $scope.itemRowCount = 1;
    $scope.itemCount = [];
    $scope.itemList = [];
    $scope.itemQnty = [];
    $scope.vm.item_all_data = [];
    $scope.itemCount.push($scope.itemRowCount);

    $scope.assignPallet = function() {

        $scope.vm.data.sku = [];
        $scope.vm.data.quantity = [];

        for (var i = 0; i < $scope.itemCount; i++) {
            $scope.vm.item_all_data[i] = {};
            $scope.vm.item_all_data[i].sku = $scope.itemList[i].sku;
            $scope.vm.item_all_data[i].quantity = $scope.itemQnty[i];
        }
        console.log("All Data", $scope.vm.item_all_data);
        $scope.vm.data.sku = $scope.vm.data.sku.toString();
        $scope.vm.data.quantity = $scope.vm.data.quantity.toString();

        $scope.vm.data.created_by = UserProfile.getUserProfile().id;

        $log.debug("POST data ", $scope.vm);
        LocationService.assignToPallet($scope.vm).then(function(response) {
            $log.debug('Asign to pallet response', response);
            if (response.data.error)
                AlertService.showErrorAlert('Error', "Error occured while assigning to pallet. Please try again!!", false);
            else
                AlertService.showSuccessNotification('Items have been assigned to pallet successfully', 3000);
        });

        $modalInstance.close();

    };

    $scope.getItemCodes = function(val) {
        return $http.post(__env.apiUrl + '/purchase-searchsku', { api_key: UserProfile.getApiKey(), sku: val }).then(function(response) {
            $log.debug('Response SKU Search ', response);
            if (!response.data.error)
                return response.data.data;
        });
    };

    $scope.clickItemRow = function(id) {
        if (id == $scope.itemCount.length - 1) {
            $scope.itemRowCount++;
            $scope.itemCount.push($scope.itemRowCount);
        } else {
            $scope.itemCount.splice(id, 1);
        }
    }



    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}]);
