'use strict';

/* Controllers */
app.controller('UserController', ['$scope', '$stateParams', '$cookies', '$uibModal', '$compile', '$state', '$log', '__env', 'UserProfile', 'UserService',
  function ($scope, $stateParams, $cookies, $modal, $compile, $state, $log, __env, UserProfile, UserService) {
    $scope.vm = {};


    $scope.addUser = function () {
      var modalInstance = $modal.open({
        templateUrl: 'partials/user-add.html',
        controller: 'UserModalCtrl',
        backdrop: 'static',
        size: 'lg',
        resolve: {
          userData: null,
          warehouseList: function ($http, __env, UserProfile) {
            return $http({
              method: 'POST',
              url: __env.apiUrl + '/warehouse-getrights',
              data: {
                api_key: UserProfile.getApiKey()
              }
            }).then(function (response) {
              return response.data.data;
            })
          }, errorFlag: 0,
          userRights: function ($http, __env, UserProfile) {
            return $http({
              method: 'POST',
              url: __env.apiUrl + '/user-getrights',
              data: {
                api_key: UserProfile.getApiKey()
              }
            }).then(function (response) {
              return response.data.data;
            })
          }
        }
      });

    }

    $scope.deleteUser = function (userId) {

      var cuser = $cookies.getObject("userProfile");
      if (cuser.id == userId) {
        var modalInstance = $modal.open({
          templateUrl: 'partials/alert-warning.html',
          controller: 'WarningModalCtrl',
          backdrop: 'static',
          resolve: {
            data: function () {
              var warning = {};
              warning.title = 'Sorry';
              warning.body = ' U can`t kill your self!!';
              warning.showCancel = false;
              return warning;
            }
          }
        });

        modalInstance.result.then(function (selection) {
          // Warning accepted
          modalInstance.dismiss()
        })

      }

      var modalInstance = $modal.open({
        templateUrl: 'partials/alert-warning.html',
        controller: 'WarningModalCtrl',
        backdrop: 'static',
        resolve: {
          data: function () {
            var warning = {};
            warning.title = 'Delete';
            warning.body = 'Are you sure you want to delete this user?';
            warning.showCancel = true;
            return warning;
          }
        }
      });

      modalInstance.result.then(function (selection) {
        // Warning accepted
        UserService.deleteUser({ user_id: userId }).then(function (response) {
          $log.debug("Delete User Response ", response);
          if (!response.data.error)
            $state.go($state.current, {}, { reload: true });
          else
            AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        }, function (error) { AlertService.showErrorNotification("Sorry!! Something went wrong. ");});
      }, function () {
        //  Warning dismissed
      });
    }


    $scope.editUser = function (userId) {

      var currentUserData = $cookies.getObject('userProfile');
      // console.log(currentUserData);
      var editUserData;
      UserService.listUser({
        user_id: userId
      }).then(function (res) {
        // console.log("hai", res.data.data);
        if (currentUserData.id == res.data.data.id) {
          // console.log("admin edit himself");
          $scope.errorFlag = 1;
        }
        else if (res.data.data.user_role == 1) {
          // console.log("admin edit other admin")
          $scope.errorFlag = 2;
        }
        else $scope.errorFlag = 0;

        // $scope.check(userId);
        var modalInstance = $modal.open({
          templateUrl: 'partials/user-edit.html',
          controller: 'UserModalCtrl',
          backdrop: 'static',
          size: 'lg',
          resolve: {
            warehouseList: function ($http, __env, UserProfile) {
              return $http({
                method: 'POST',
                url: __env.apiUrl + '/warehouse-getrights',
                data: {
                  api_key: UserProfile.getApiKey()
                }
              }).then(function (response) {
                return response.data.data;
              })
            },
            errorFlag: $scope.errorFlag,
            userRights: function ($http, __env, UserProfile) {
              return $http({
                method: 'POST',
                url: __env.apiUrl + '/user-getrights',
                data: {
                  api_key: UserProfile.getApiKey()
                }
              }).then(function (response) {
                return response.data.data;
              })
            },
            userData: function (UserService) {
              return UserService.listUser({
                user_id: userId
              }).then(function (response) {
                // console.log("cookie",$cookies.getObject('userProfile'))
                // console.log("userdata",response.data.data)
                return response.data.data;
              });
            }
          }
        })
      });

    }

    $scope.viewUserDetail = function (userId) {
      $state.go('app.users.detail', { userId: userId });
    }

    $scope.dataTableOpt = {
      "processing": true,
      "language": {
        "processing": "<img src='img/loading.gif' width='50px' height='50px'>"
      },
      "serverSide": true,
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      "pagingType": "simple_numbers",

      "ajax": {
        "url": __env.apiUrl + '/user-list',
        "type": "POST",
        "data": {
          api_key: UserProfile.getApiKey()
        },
        autoWidth: false,
      },

      "columnDefs": [{
        targets: [0],
        // "className": "col-lg-2 col-md-2 col-sm-2",
      },
      {
        targets: [1],
        // "className": "col-lg-2 col-md-2 col-sm-2",
      },
      {
        targets: [3],
        // "className": "col-lg-1 col-md-1 col-sm-1",
        // "width": "10%"
        autoWidth: false,
        width: '100px'
      },
      {
        targets: [4],
        // "className": "col-lg-1 col-md-1 col-sm-1",
        render: function (data, type, row) {
          if (data == 1)
            return '<span class="label label-success">Active</span>';
          else return '<span class="label label-default">Inactive</span>';
        }
      },
      {
        targets: [5],
        sortable: false,
        // "drawCallback": function( settings ) {
        // console.log( 'DataTables has redrawn the table' );

        //  },
        className: "col-lg-3 col-md-3 col-sm-3 col-xs-3",
        render: function (data, type, full) {

          return '<span class="btn-primary btn-xs cursor-pointer" data-toggle="tooltip" data-placement="left" title="View" ng-click="viewUserDetail(' + data + ')"><i class="fa fa-info hidden-xs"></i></span> &nbsp' +
            '<span class="btn-success btn-xs cursor-pointer" data-toggle="tooltip" data-placement="left" title="Edit" ng-click="editUser(' + data + ')"><i class="fa fa-pencil hidden-xs"></i></span> &nbsp' +
            '<span  class="btn-danger btn-xs cursor-pointer" data-toggle="tooltip" data-placement="left" title="Delete" ng-click="deleteUser(' + data + ')"><i class="fa fa-trash hidden-xs"></i></span>';


        }
      }

      ],
      "rowCallback": function (row, data, index) {
        $compile(row)($scope);
        return row;
      }
    }

    $scope.init = function () {
      $scope.locationTypes.forEach(function (type) {
        if (type.type_id == $stateParams.typeId)
          $scope.type = type;
      }, this);


    }


  }
]);

/**
 * Controller for Modals in user page
 */
app.controller('UserModalCtrl', ['$window','$scope', '$uibModalInstance', '$http', '$log', '$state', '$uibModal', 'AlertService', 'UserProfile', 'warehouseList', 'errorFlag', 'userRights', 'UserService', 'userData',
  function ($window,$scope, $modalInstance, $http, $log, $state, $modal, AlertService, UserProfile, warehouseList, errorFlag, userRights, UserService, userData) {
    // $scope.errorFlag=errorFlag;
    // console.log(errorFlag);
    if (errorFlag==1){
$scope.isSuperAdmin=true;
    }
else $scope.isSuperAdmin=false;
    $scope.vm = {};
    $scope.vm.status = 1;
    $scope.userRights = userRights;
    $scope.warehouseRights = warehouseList;
    $scope.warehouse_rights;
    $scope.user_rights;
    $scope.vm.user_rights = '';
    $scope.vm.warehouse_rights = '';
    $scope.isUpdate = false;
    $scope.vm.isSuperAdmin = false;
    $scope.check = function () {
      // console.log(errorFlag);
      if (errorFlag == 1) {
        $modalInstance.dismiss();
        var modalInstance = $modal.open({
          templateUrl: 'partials/alert-warning.html',
          controller: 'WarningModalCtrl',
          backdrop: 'static',
          resolve: {
            data: function () {
              var warning = {};
              warning.title = 'Edit';
              warning.body = 'You about to edit your self,you will be logged out after the successfull completion, Do you want to continue?';
              warning.showCancel = true;
              return warning;
            }
          }
        });

        modalInstance.result.then(function (selection) {
          // Warning accepted
          // $modalInstance.dismiss();
          $scope.saveUser(errorFlag)
        }, function () {
          //  Warning dismissed
        });
      }
      else if (errorFlag == 2) {
        $modalInstance.dismiss();
        var modalInstance = $modal.open({
          templateUrl: 'partials/alert-warning.html',
          controller: 'WarningModalCtrl',
          backdrop: 'static',
          resolve: {
            data: function () {
              var warning = {};
              warning.title = 'Edit';
              warning.body = 'Are you editing a user with "Admin-Privilage",Do you want to continue?';
              warning.showCancel = true;
              return warning;
            }
          }
        });
        modalInstance.result.then(function (selection) {
          // Warning accepted
          $scope.saveUser(errorFlag)
        }, function () {
          //  Warning dismissed
        });

      }
      else {
        $scope.saveUser();

      }
    }
    $scope.saveUser = function (errorFlag) {


      if ($scope.vm.isSuperAdmin)
        $scope.vm.user_role = 1;
      else
        $scope.vm.user_role = 2;
      if ($scope.vm.user_id) {
        var user_right_copy=$scope.vm.user_rights;
        var warehouse_right_copy=$scope.vm.warehouse_rights;
        $scope.vm.user_rights = []
      user_right_copy.split(',').forEach(function (dat, index) {
         if (dat != "") {
          $scope.vm.user_rights.push(dat)
        }
      })
      $scope.vm.warehouse_rights = []
      warehouse_right_copy.split(',').forEach(function (val, index) {
        if (val != "") {
          $scope.vm.warehouse_rights.push(val)
        }
      })

        // console.log($scope.vm);
//         $log.debug("Update User Response Rights", $scope.user_rights);
// $log.debug("warehouse Rights", $scope.warehouse_rights);

        UserService.editUser($scope.vm).then(function (response) {
          $log.debug("Update User Response ", response);
          if (!response.data.error) {
            if (errorFlag == 1) {
            UserProfile.clearUserProfile();
            $state.go('access.login',{},{reload:true});
            }
            else
              $state.go($state.current, {}, { reload: true });
          }

          else
            AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        }, function (error) { AlertService.showErrorNotification("Sorry!! Something went wrong. ");});
      } else {
        // Create new user data

        $scope.vm.created_user_id = UserProfile.getUserProfile().id;
        // console.log("second",$scope.vm);
        UserService.createUser($scope.vm).then(function (response) {
          $log.debug("Add User Response ", response);
          if (!response.data.error) {
            if (errorFlag == 1) {
              UserProfile.clearUserProfile()
              $state.go('access.login', {}, { reload: true });
              // $window.location('access.login');
            }
            else
              $state.go($state.current, {}, { reload: true });
          }
          else
            AlertService.showErrorNotification("Sorry!! Something went wrong. ");;
        }, function (error) { AlertService.showErrorNotification("Sorry!! Something went wrong. "); });
      }

      $modalInstance.close();
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    // Watch for multi selection change on user right
    $scope.$watch('user_rights', function () {
      if ($scope.user_rights) {
        $scope.vm.user_rights = '';
        $scope.user_rights.forEach(function (rights, index, array) {
          $scope.vm.user_rights += rights.id;
          if (index != array.length - 1)
            $scope.vm.user_rights += ',';

        }, this);
      }
    }, true);

    // Watch for multi selection change on warehouse right
    $scope.$watch('warehouse_rights', function () {
      if ($scope.warehouse_rights) {
        $scope.vm.warehouse_rights = '';
        $scope.warehouse_rights.forEach(function (rights, index, array) {
          $scope.vm.warehouse_rights += rights.warehouse_id;
          if (index != array.length - 1)
            $scope.vm.warehouse_rights += ',';
        }, this);
      }
    }, true);

    $scope.init = function () {
      if (userData) {
        $scope.user_rights = [];
        $scope.warehouse_rights = [];
        $scope.isUpdate = true;
        $scope.vm.user_id = userData.id;
        $scope.vm.first_name = userData.first_name;
        $scope.vm.last_name = userData.last_name;
        $scope.vm.email = userData.admin_email;
        $scope.vm.status = userData.status;
        $scope.vm.title = userData.title;
        $scope.vm.isSuperAdmin = userData.user_role == 1 ? true : false;
        userData.user_right.split(',').forEach(function (user_right) {
          userRights.forEach(function (element) {
            if (user_right == element.id)
              $scope.user_rights.push(element);
          }, this);
        }, this);
        userData.warehouse_right.split(',').forEach(function (id) {
          warehouseList.forEach(function (element) {
            if (id == element.warehouse_id)
              $scope.warehouse_rights.push(element);
          }, this);
        }, this);

      }
    }

  }
]);

app.controller('UserDetailCtrl', ['$cookies', '$scope', '$http', 'editableOptions', '$log', '$state', '$uibModal', '$localStorage', '$filter', 'editableThemes', 'UserProfile', 'warehouseList', 'userRights', 'UserService', 'userData', 'AlertService',
  function ($cookies, $scope, $http, editableOptions, $log, $state, $modal, $localStorage, $filter, editableThemes, UserProfile, warehouseList, userRights, UserService, userData, AlertService) {

    editableThemes.bs3.inputClass = 'input-sm';
    editableThemes.bs3.buttonsClass = 'btn-sm';
    editableOptions.theme = 'bs3';

    $scope.userData = userData;
    $log.debug("USer Data ", userData);
    $scope.userRights = [];
    $scope.warehouseRights = [];
    if (userData.user_role != 1)
      $scope.tabIndex = $localStorage.userDetailTabIndex ? $localStorage.userDetailTabIndex : 0;
    else
      $scope.tabIndex = 0;
    $scope.assign = [{
      value: 1,
      text: '✓'
    }, {
      value: 0,
      text: 'x'
    }];



    $scope.init = function () {
      userRights.forEach(function (element) {
        var right = {};
        right = element;
        if (userData.user_right.split(',').map(Number).indexOf(element.id) != -1) {
          right.icon = 'check';
          right.status = 1;
        } else {
          right.icon = 'close';
          right.status = 0;
        }
        $scope.userRights.push(right);
      }, this);

      warehouseList.forEach(function (element) {
        var warehouse = {};
        warehouse = element;
        if (userData.warehouse_right.split(',').map(Number).indexOf(element.warehouse_id) != -1) {
          warehouse.icon = 'check';
          warehouse.status = 1;
        } else {
          warehouse.icon = 'close';
          warehouse.status = 0;
        }
        $scope.warehouseRights.push(warehouse);
      }, this);
    }

    $scope.updateUser = function (section) {
      var vm = {};
      if (section == 'basic_info') {
        $localStorage.userDetailTabIndex = 0;
      } else if (section == 'user_right') {
        $localStorage.userDetailTabIndex = 1;
        if ($filter('filter')($scope.userRights, { status: 1 }).length == 0)
          AlertService.showErrorAlert('Error', 'Atleast one user right need to be assigned', false);
        else
          $scope.userRights.forEach(function (element) {
            var position = $scope.userData.user_right.split(',').map(Number).indexOf(element.id);
            if (element.status == 1 && position == -1) {
              if ($scope.userData.user_right.split(',').length > 0)
                $scope.userData.user_right += ',';
              $scope.userData.user_right += element.id;
            } else if (element.status == 0 && position != -1) {
              if (position == 0)
                $scope.userData.user_right = $scope.userData.user_right.substring(2, $scope.userData.user_right.length - 1);
              else if (position == $scope.userData.user_right.split(',').length - 1)
                $scope.userData.user_right = $scope.userData.user_right.substring(0, $scope.userData.user_right.length - 2);
              else
                $scope.userData.user_right = $scope.userData.user_right.replace(',' + element.id + ',', ',');
            }
          }, this);
      } else if (section == 'warehouse_right') {
        $localStorage.userDetailTabIndex = 2;
        if ($filter('filter')($scope.warehouseRights, { status: 1 }).length == 0)
          AlertService.showErrorAlert('Error', 'Atleast one Warehouse need to be assigned', false);
        else
          $scope.warehouseRights.forEach(function (element) {
            var position = $scope.userData.warehouse_right.split(',').map(Number).indexOf(element.warehouse_id);
            if (element.status == 1 && position == -1) {
              if ($scope.userData.warehouse_right.split(',').length > 0)
                $scope.userData.warehouse_right += ',';
              $scope.userData.warehouse_right += element.warehouse_id;
            } else if (element.status == 0 && position != -1) {
              if (position == 0)
                $scope.userData.warehouse_right = $scope.userData.warehouse_right.substring(2, $scope.userData.warehouse_right.length - 1);
              else if (position == $scope.userData.warehouse_right.split(',').length - 1)
                $scope.userData.warehouse_right = $scope.userData.warehouse_right.substring(0, $scope.userData.warehouse_right.length - 2);
              else
                $scope.userData.warehouse_right = $scope.userData.warehouse_right.replace(',' + element.warehouse_id + ',', ',');

            }
          }, this);
      }
      vm.user_id = $scope.userData.id;
      vm.first_name = $scope.userData.first_name;
      vm.last_name = $scope.userData.last_name;
      vm.email = $scope.userData.admin_email;
      vm.title = $scope.userData.title;
      vm.status = $scope.userData.status;
      vm.user_role = $scope.userData.user_role;
      vm.user_rights = []
      $scope.userData.user_right.split(',').forEach(function (dat, index) {
         if (dat != "") {
          vm.user_rights.push(dat)
        }
      })

      vm.warehouse_rights = []
      $scope.userData.warehouse_right.split(',').forEach(function (val, index) {
        if (val != "") {
          vm.warehouse_rights.push(val)
        }
      })

      $log.debug("Update Post Data ", vm);
      UserService.editUser(vm).then(function (response) {
        $log.debug("Update Response ", response);
        if (response.data.error)
          AlertService.showErrorNotification("Sorry!! Something went wrong. ");
        else
          $state.go($state.current, {}, {
            reload: true
          });
      }, function (error) { AlertService.showErrorNotification("Sorry!! Something went wrong. "); })
    }
  }
]);
