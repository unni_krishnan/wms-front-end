'use strict';

app.service("UserProfile", ['$cookies', '$log',
    function($cookies, $log) {


        var userProfile = {

            setUserProfile: function(userData, isRemember) {
                var expireDate;
                if (isRemember) {
                    expireDate = new Date();
                    expireDate.setDate(expireDate.getFullYear() + 1);
                }
                if (expireDate)
                    $cookies.putObject('userProfile', userData, { 'expires': expireDate });
                else
                    $cookies.putObject('userProfile', userData);
            },
            getUserProfile: function() {
                return $cookies.getObject('userProfile');
            },

            clearUserProfile: function() {
                $cookies.remove('userProfile');
                $cookies.remove('warehouseList');
                $cookies.remove('selectedWarehouse');
            },

            isAuthenticated: function() {
                if ($cookies.getObject('userProfile'))
                    return true;
                else return false;
            },

            isSuperAdmin: function() {
                // return true;
                if (!$cookies.getObject('userProfile'))
                    return false;
                else if ($cookies.getObject('userProfile').user_role == 1)
                    return true;
                else return false;
            },

            getApiKey: function() {
                // return '58a6c133942fc';
                return $cookies.getObject('userProfile').api_key;
            },

            hasRole: function(roleId) {
                if (!$cookies.getObject('userProfile'))
                    return false;
                else {
                    if ($cookies.getObject('userProfile').user_right.split(',').map(Number).indexOf(roleId) != -1)
                        return true;
                    else return false;
                }
            }

        }

        return userProfile;

    }
])