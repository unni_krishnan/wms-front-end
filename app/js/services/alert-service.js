app.service('AlertService', ['$uibModal', '$state', '$injector', function($modal, $state, $injector) {

    var model;
    this.showErrorAlert = function(title, message, showCancel) {
        if (!model)
            model = $modal.open({
                templateUrl: 'partials/alert-warning.html',
                controller: 'WarningModalCtrl',
                resolve: {
                    data: function() {
                        var warning = {};
                        warning.title = title || 'Error Occured';
                        warning.body = message || 'It Seems an error has occured. Please try again.';
                        warning.showCancel = showCancel || false;
                        return warning;
                    }
                }
            });
        model.result.then(function(selectedItem) {
            model = '';
        }, function() {
            model = '';
        });
    };
    this.showSuccessAlert = function(message, timeout) {
        if (!model) {
            model = $modal.open({
                templateUrl: 'partials/alert-success.html',
                controller: 'SuccessModalCtrl',
                resolve: {
                    data: function() {
                        return { message: message, timeout: timeout };
                    }
                }
            });
            model.result.then(function() {
                $state.go($state.current, {}, { reload: true });
                model = '';

            }, function() {
                $state.go($state.current, {}, { reload: true });
                model = '';

            });

        }
    };
    this.showSuccessNotification = function(message, duration) {
        var notify = $injector.get('notify');
        notify({
            message: message,
            templateUrl: 'partials/notification-success.html',
            position: 'right',
            duration: duration || 3000
        });
    };
    this.showErrorNotification = function(message, duration) {
      var notify = $injector.get('notify');
      notify({
          message: message,
          templateUrl: 'partials/notification-error.html',
          position: 'right',
          duration: duration || 3000
      });
  };
}]);
