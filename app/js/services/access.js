'use strict';

angular.module('app').factory("Access", ["$q", '$http', 'UserProfile', function($q, $http, UserProfile) {

    var Access = {

        OK: 200,

        // "we don't know who you are, so we can't say if you're authorized to access
        // this resource or not yet, please sign in first"
        UNAUTHORIZED: 401,

        // "we know who you are, and your profile does not allow you to access this resource"
        FORBIDDEN: 403,

        INVALID_API: 100,

        hasRole: function(roleId) {
            if (UserProfile.isSuperAdmin())
                return Access.OK;
            else if (UserProfile.hasRole(roleId)) {
                return Access.OK;
            } else if (UserProfile.isAuthenticated()) {
                return $q.reject(Access.UNAUTHORIZED);
            } else {
                return $q.reject(Access.FORBIDDEN);
            }

        },

        hasAnyRole: function(roles) {
            return UserProfile.then(function(userProfile) {
                if (userProfile.$hasAnyRole(roles)) {
                    return Access.OK;
                } else if (userProfile.$isAnonymous()) {
                    return $q.reject(Access.UNAUTHORIZED);
                } else {
                    return $q.reject(Access.FORBIDDEN);
                }
            });
        },

        isAnonymous: function() {
            return UserProfile.then(function(userProfile) {
                if (userProfile.$isAnonymous()) {
                    return Access.OK;
                } else {
                    return $q.reject(Access.FORBIDDEN);
                }
            });
        },


        isSuperAdmin: function() {
            console.log("Super Admin Called", UserProfile.isSuperAdmin());
            if (UserProfile.isSuperAdmin())
                return Access.OK;
            else if (UserProfile.isAuthenticated())
                return $q.reject(Access.FORBIDDEN);
            else
                return $q.reject(Access.UNAUTHORIZED);
        },

        isApiKeyValid: function() {
            if (UserProfile.isAuthenticated()) {
                var defer = $q.defer();
                $http({
                    url: __env.apiUrl + '/admin-auth',
                    method: 'POST',
                    data: { api_key: UserProfile.getApiKey() }
                }).then(function(response) {
                    console.log('Api Check  Response', response);
                    if (response.data.error) {
                        console.log("Rejecting");
                        return defer.reject(Access.INVALID_API);
                    } else
                        return defer.resolve(Access.OK);
                });

                return defer.promise;
            } else
                return $q.reject(Access.UNAUTHORIZED);
        },
        isAuthenticated: function() {
            console.log("Is Authenticated Called");
            if (UserProfile.isAuthenticated()) {
                return Access.OK;
            } else {
                return $q.reject(Access.UNAUTHORIZED);
            }

        }

    };

    return Access;

}])