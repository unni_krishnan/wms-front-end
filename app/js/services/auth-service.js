'use strict';

angular.module('app').service("Auth", ["$http", function ($http) {

  this.getProfile = function () {
      console.log("CALLING API");
    return $http.get("api/auth");
  };

}])
