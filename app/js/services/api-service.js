'use strict';

app.factory('WarehouseService', ['$http', 'UserProfile', function ($http, UserProfile) {

  // Handler to save warehouse data to server
  var _createWarehouse = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/warehouse-add', formData)
  };

  // Handler to edit the warehouse data
  var _editWarehouse = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/warehouse-update', formData)
  };

  var _listWarehouse = function (warehouseId) {
    var formData = {};
    formData.warehouse_id = warehouseId;
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/warehouse-edit', formData);
  };
  // Handler to delete the warehouse data
  var _deleteWarehouse = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/warehouse-delete', formData)
  };

  var _listPlan = function () {
    var formData = {};
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/warehouse-plans', formData);
  };

  var warehouseService = {
    createWarehouse: _createWarehouse,
    editWarehouse: _editWarehouse,
    deleteWarehouse: _deleteWarehouse,
    listWarehouse: _listWarehouse,
    listPlan: _listPlan
  };

  return warehouseService;
}]);

app.factory('LocationService', ['$http', '$log', 'UserProfile', function ($http, $log, UserProfile) {

  // Handler to save location data to server
  var _createLocation = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    $log.debug('Location Add Post Data', formData);
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/location-add',
      headers: {
        'Content-Type': undefined
      },
      data: formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          formData.append(key, value);
        });
        return formData;
      }
    });
  };

  // Handler to edit location data to server
  var _editLocation = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/location-edit', formData)
  };

  // Handler to delete location data to server
  var _deleteLocation = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/location-delete', formData)
  };

  // Handler to delete location data to server
  var _deleteSingleLocation = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/location-delete-type', formData)
  };

  // Handler to list location data to server
  var _listLocation = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/location-list', formData)
  };

  var _getAllocatedLocationList = function (warehouseId, typeId) {
    return $http.post(__env.apiUrl + '/get-occupied-location-list', {
      warehouse_id: warehouseId,
      api_key: UserProfile.getApiKey(),
      type_id: typeId
    });
  };

  var _generatePalletId = function () {
    return $http.post(__env.apiUrl + '/generate_palletid', {
      api_key: UserProfile.getApiKey()
    });
  };

  var _assignToPallet = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/update_generate_pallet',
      headers: {
        'Content-Type': undefined
      },
      data: _formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          if (key == 'item_all_data')
            formData.append(key, JSON.stringify(value));
          else
            formData.append(key, value);
        });
        return formData;
      }
    });
  };

  var _getItemDetails = function (id,type_id) {
    console.log('ID ', id);
    return $http.post(__env.apiUrl + '/get-occupied-location-items', {
      api_key: UserProfile.getApiKey(),
      location_id: id,
      location_type:type_id
    });
  };

  var _deletePalletId = function (id) {
    return $http.post(__env.apiUrl + '/delete_palletid', {
      api_key: UserProfile.getApiKey(),
      pallet_id: id
    });
  };

  var _getPalletIdBarcode = function (id) {
    return $http.post(__env.apiUrl + '/get_barcode_by_palletid', {
      api_key: UserProfile.getApiKey(),
      pallet_id: id
    });
  };

  var locationService = {
    createLocation: _createLocation,
    editLocation: _editLocation,
    listLocation: _listLocation,
    deleteLocation: _deleteLocation,
    deleteSingleLocation: _deleteSingleLocation,
    getAllocatedLocationList: _getAllocatedLocationList,
    generatePalletId: _generatePalletId,
    assignToPallet: _assignToPallet,
    getItemDetails: _getItemDetails,
    deletePalletId: _deletePalletId,
    getPalletIdBarcode: _getPalletIdBarcode
  }
  return locationService;
}]);

app.factory('UserService', ['$http', 'UserProfile', function ($http, UserProfile) {

  // Handler to save user data to server
  var _createUser = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/user-add', formData)
  };

  // Handler to update user data in server
  var _updateUser = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    console.log("Update Data ", formData);
    return $http.post(__env.apiUrl + '/user-update', formData)
  };

  // Handler to list single user data in server
  var _listUser = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/user-edit', formData)
  };

  // Handler to delete user data in server
  var _deleteUser = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/user-delete', formData)
  };

  // Handler to list all user data from server
  var _listUsers = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/user-list', formData)
  };

  var _createCompanyProfile = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    // return $http.post(__env.apiUrl + '/user-companyadd', _formData)
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/user-companyadd',
      headers: {
        'Content-Type': undefined
      },
      data: _formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          formData.append(key, value);
        });
        return formData;
      }
    });
  };
  var _editCompanyProfile = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/user-companyupdate',
      headers: {
        'Content-Type': undefined
      },
      data: _formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          formData.append(key, value);
        });
        return formData;
      }
    });
  };

  var _listCompanyDetail = function () {
    var formData = {};
    formData.api_key = UserProfile.getApiKey();
    formData.user_id = UserProfile.getUserProfile().id;
    return $http.post(__env.apiUrl + '/user-companydetails', formData)

  };

  var _updateUserProfile = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/user-profile-edit',
      headers: {
        'Content-Type': undefined
      },
      data: _formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          formData.append(key, value);
        });
        return formData;
      }
    });

    // return $http.post(__env.apiUrl + '/user-profile-edit', _formData)

  }
  var _resetPassword = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/user-change-password', _formData)
  }

  var userService = {
    createUser: _createUser,
    editUser: _updateUser,
    listUser: _listUser,
    listUsers: _listUsers,
    deleteUser: _deleteUser,
    createCompany: _createCompanyProfile,
    editCompany: _editCompanyProfile,
    updateUserProfile: _updateUserProfile,
    getCompanyDetail: _listCompanyDetail,
    resetPassword: _resetPassword
  }
  return userService;
}]);

app.factory('VendorService', ['$http', 'UserProfile', function ($http, UserProfile) {

  // Handler to save vendor data to server
  var _createVendor = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/vendor-add', formData)
  };

  // Handler to update vendor data in server
  var _updateVendor = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    console.log("Update Data ", formData);
    return $http.post(__env.apiUrl + '/vendor-update', formData)
  };

  // Handler to list single vendor data in server
  var _listVendor = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/vendor-edit', formData)
  };

  // Handler to delete vendor data in server
  var _deleteVendor = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/vendor-delete', formData)
  };

  // Handler to list all vendor data from server
  var _listVendors = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/vendor-list', formData)
  };

  var vendorService = {
    createVendor: _createVendor,
    editVendor: _updateVendor,
    listVendor: _listVendor,
    listVendors: _listVendors,
    deleteVendor: _deleteVendor,

  }
  return vendorService;
}]);

app.factory('CustomerService', ['$http', 'UserProfile', function ($http, UserProfile) {

  // Handler to save customer data to server
  var _createCustomer = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    console.log("SENDD FORMD ATA", _formData);
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/customer-add',
      headers: {
        'Content-Type': undefined
      },
      data: _formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          formData.append(key, value);
        });
        return formData;
      }
    });

    // $http.post(__env.apiUrl + '/customer-add', formData)
  };

  // Handler to update customer data in server
  var _updateCustomer = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    console.log("Update Data ", formData);
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/customer-update',
      headers: {
        'Content-Type': undefined
      },
      data: formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          console.log("Value === ", value);
          console.log("key === ", key);
          if (key == 'data')
            formData.append(key, JSON.stringify(value));
          else
            formData.append(key, value);
        });
        console.log("FORM DATA  ==== ", formData);
        return formData;
      }
    });

    // $http.post(__env.apiUrl + '/customer-update', formData)
  };

  // Handler to list single customer data in server
  var _listCustomer = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/customer-view', formData)
  };

  // Handler to delete customer data in server
  var _deleteCustomer = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/customer-delete', formData)
  };

  // Handler to list all customer data from server
  var _listCustomers = function () {
    api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/customer-list', api_key)
  };

  var customerService = {
    createCustomer: _createCustomer,
    editCustomer: _updateCustomer,
    listCustomer: _listCustomer,
    listCustomers: _listCustomers,
    deleteCustomer: _deleteCustomer,

  }
  return customerService;
}]);

app.factory('ItemService', ['$http', '$filter', 'UserProfile', function ($http, $filter, UserProfile) {

  // Handler to save item data to server
  var _createItem = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    if (_formData.data.expiration_date) {
      var date = _formData.data.expiration_date;
      _formData.data.expiration_date = $filter('date')(_formData.data.expiration_date, "yyyy-MM-dd HH:mm:ss ");
    }
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/item-add',
      headers: {
        'Content-Type': undefined
      },
      data: _formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          if (key == 'data')
            formData.append(key, JSON.stringify(value));
          else
            formData.append(key, value);
        });
        data.data.expiration_date = new Date(data.data.expiration_date);
        return formData;
      }
    });

    // $http.post(__env.apiUrl + '/item-add', formData)
  };

  // Handler to update item data in server
  var _updateItem = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    if (formData.data.expiration_date) {
      var date = formData.data.expiration_date;
      formData.data.expiration_date = $filter('date')(formData.data.expiration_date, "yyyy-MM-dd HH:mm:ss ");
    }
    console.log("Update Data ", formData);
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/item-update',
      headers: {
        'Content-Type': undefined
      },
      data: formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          console.log("Value === ", value);
          console.log("key === ", key);
          if (key == 'data')
            formData.append(key, JSON.stringify(value));
          else
            formData.append(key, value);
        });
        data.data.expiration_date = new Date(data.data.expiration_date);
        return formData;
      }
    });

    // $http.post(__env.apiUrl + '/item-update', formData)
  };

  // Handler to list single item data in server
  var _listItem = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/item-view', formData)
  };

  // Handler to delete item data in server
  var _deleteItem = function (itemId) {
    var formData = {};
    formData.item_id = itemId;
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/item-delete', formData)
  };

  // Handler to list all item data from server
  var _listItems = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/item-list', formData)
  };

  var itemService = {
    createItem: _createItem,
    editItem: _updateItem,
    listItem: _listItem,
    listItems: _listItems,
    deleteItem: _deleteItem,

  }
  return itemService;
}]);

app.factory('PurchaseOrderService', ['$http', '$filter', 'UserProfile', function ($http, $filter, UserProfile) {

  // Handler to save purchase order to server
  var _createPO = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    _formData.data.ship_date = $filter('date')(_formData.data.ship_date, "yyyy-MM-dd HH:mm:ss ");
    _formData.data.receive_date = $filter('date')(_formData.data.receive_date, "yyyy-MM-dd HH:mm:ss ");
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/purchase-add',
      headers: {
        'Content-Type': undefined
      },
      data: _formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          if (key == 'data')
            formData.append(key, JSON.stringify(value));
          else if (key == 'item_all_data')
            formData.append(key, JSON.stringify(value));
          else
            formData.append(key, value);
        });
        data.data.ship_date = new Date(data.data.ship_date);
        data.data.receive_date = new Date(data.data.receive_date);
        return formData;
      }
    });

    // $http.post(__env.apiUrl + '/item-add', formData)
  };

  // Handler to update purchase order in server
  var _updatePO = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    formData.data.ship_date = $filter('date')(formData.data.ship_date, "yyyy-MM-dd HH:mm:ss ");
    formData.data.receive_date = $filter('date')(formData.data.receive_date, "yyyy-MM-dd HH:mm:ss ");

    console.log("Update Data ", formData);
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/purchase-update',
      headers: {
        'Content-Type': undefined
      },
      data: formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          console.log("Value === ", value);
          console.log("key === ", key);
          if (key == 'data')
            formData.append(key, JSON.stringify(value));
          else
            formData.append(key, value);
        });
        data.data.ship_date = new Date(data.data.ship_date);
        data.data.receive_date = new Date(data.data.receive_date);
        return formData;
      }
    });

    // $http.post(__env.apiUrl + '/item-update', formData)
  };

  // Handler to list single purchase order
  var _listPO = function (poId) {
    var formData = {};
    formData.purchase_id = poId;
    formData.api_key = UserProfile.getApiKey();
    //  formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/purchase-edit', formData)
  };
  var _listAsn = function (poId) {
    var formData = {};
    formData.asn_no = poId;
    formData.api_key = UserProfile.getApiKey();
    // formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/asn-details', formData)
  };

  // Handler to delete purchase order from server
  var _deletePO = function (poId) {
    var formData = {};
    formData.purchase_id = poId;
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/purchase-delete', formData)
  };

  // Handler to list all purchase orders from server
  var _listPOs = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/item-list', formData)
  };

  var _createAsn = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/purchase-asn',
      headers: {
        'Content-Type': undefined
      },
      data: _formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          if (key == 'data')
            formData.append(key, JSON.stringify(value));
          else if (key == 'item_all_data')
            formData.append(key, JSON.stringify(value));
          else
            formData.append(key, value);
        });
        return formData;
      }
    });
    // return $http.post(__env.apiUrl + '/purchase-asn', formData);
  };

  var poService = {
    createPO: _createPO,
    editPO: _updatePO,
    listPO: _listPO,
    listPOs: _listPOs,
    deletePO: _deletePO,
    createAsn: _createAsn,
    listAsn: _listAsn

  }
  return poService;
}]);
app.factory('ShipstationService', ['$http', 'UserProfile', function ($http, UserProfile) {
  var _getAllStore = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    return $http(__env.apiUrl + '/shipstation-store-list', _formData)
  }
  var _getOneOrder = function (storeId) {
    var formData = {}
    formData.store_id = storeId;
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/shipstation-saved-order-list', formData);
  }
  var _addStore = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    console.log(_formData);
    return $http.post(__env.apiUrl + '/shipstation-store-add', _formData)
  }
  var _delStore = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + "/shipstation-store-delete", _formData)
  }
  var shipStationService = {
    listStore: _getAllStore,
    getOrder: _getOneOrder,
    addaStore: _addStore,
    deleteStore: _delStore,
  }
  return shipStationService;
}])


app.factory('InventoryService', ['$http', '$filter', 'UserProfile', function ($http, $filter, UserProfile) {

  var _initiateTransfer = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    _formData.data.user_id = UserProfile.getUserProfile().id;
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/inventory-add',
      headers: {
        'Content-Type': undefined
      },
      data: _formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          if (key == 'data')
            formData.append(key, JSON.stringify(value));
          else
            formData.append(key, value);
        });
        return formData;
      }
    });
  };

  var _reportTransfer = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/inventory-list', formData);
  };

  var _receiveItems = function (_formData) {
    console.log("error data:",_formData)
    _formData.api_key = UserProfile.getApiKey();
    angular.forEach(_formData.item_data, function (value, key) {
      if (value.expiry_date)
        value.expiry_date = $filter('date')(value.expiry_date, "yyyy-MM-dd HH:mm:ss ");

    });
    console.log('Post Data ', _formData);

    return $http({
      method: 'POST',
      url: __env.apiUrl + '/inventory-receiveadd',
      headers: {
        'Content-Type': undefined
      },
      data: _formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          if (key == 'data')
            formData.append(key, JSON.stringify(value));
          else if (key == 'item_data')
            formData.append(key, JSON.stringify(value));
          else if (key == 'item_all_data')
            formData.append(key, JSON.stringify(value));
          else
            formData.append(key, value);
        });
        return formData;
      }
    })
  };


  var _importInventory = function (formData) {
    formData.api_key = UserProfile.getApiKey();
    $log.debug('Import Inventory POST Data', formData);
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/',
      headers: {
        'Content-Type': undefined
      },
      data: formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          formData.append(key, value);
        });
        return formData;
      }
    });
  };

  var poService = {
    initiateTransfer: _initiateTransfer,
    generateReport: _reportTransfer,
    receive: _receiveItems,
    importInventory: _importInventory
  }
  return poService;
}]);

/**
 * Factory to handle all order management related api calls
 */
app.factory('OrderService', ['$http', 'UserProfile', function ($http, UserProfile) {
  var _getOrderDetailsByIds = function (_formData) {
    _formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/shipstation-order-details-by-ids', _formData)
  }

  var _savePickList = function (_formData) {
    console.log("hai",_formData);
    _formData.api_key = UserProfile.getApiKey();
    console.log("formdata", _formData);
    return $http({
      method: 'POST',
      url: __env.apiUrl + '/shipstation-pick-list-add',
      headers: {
        'Content-Type': undefined
      },
      data: _formData,
      transformRequest: function (data, headersGetter) {
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
          if (key == 'data')
            formData.append(key, JSON.stringify(value));
          else if (key == 'order_id')
            formData.append(key, JSON.stringify(value));
          else
            formData.append(key, value);
        });
        console.log("data sending from picklist save service:",formData);
        return formData;
      }
    });

  }
  var _getPickList = function () {
    var _formData = {};
    _formData.api_key = UserProfile.getApiKey();
    return $http.post(__env.apiUrl + '/shipstation-pick-list', _formData)
  }
  var orderService = {
    getOrderDetailsByIds: _getOrderDetailsByIds,
    savePickList: _savePickList,
    getPickList: _getPickList
  }
  return orderService;
}])
