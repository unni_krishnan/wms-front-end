angular.module('app').directive('focusOn', function () {
    return {
        scope: {
            focusOn: '='
        },
        link: function ($scope, $element) {

            $scope.$watch('focusOn', function (shouldFocus) {
                if (shouldFocus) {
                    $element[0].focus();
                }
            });

        }
    };
})