angular.module('app').directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            var isMultiple = attrs.multiple;
            el.bind('change', function (event) {
                if (attrs.multiple) {
                    var file = [];
                    angular.forEach(event.target.files, function (item) {
                        file.push(item);
                    });
                    scope.file = file ? file : undefined;
                    scope.$apply();
                }
                else {
                    var file = event.target.files[0];
                    scope.file = file ? file : undefined;
                    scope.$apply();
                }
            });
        }
    };
});