angular.module('app').directive('emailAvailable', ["$q", "$http", 'UserProfile', function ($q, $http, UserProfile) {
  return {
    require: "ngModel",
    scope: {
      email: "=emailAvailable"
    },
    link: function (scope, element, attributes, ngModel) {
      ngModel.$asyncValidators.emailAvailable = function (_email) {
        var deferred = $q.defer();
        if (scope.$parent.isUpdate)
          return $q.resolve();
        else
          $http({
            method: 'POST',
            url: __env.apiUrl + '/user-emailcheck',
            data: {
              api_key: UserProfile.getApiKey(),
              email: _email
            }
          }).then(function (response) {
            var rep = response.data.message;
            if (rep != "Email available") {
              deferred.resolve();
            } else {
              deferred.reject();
            }
          });
        return deferred.promise;
      };
    }
  };
}]);
angular.module('app').directive('vemailAvailable', ["$q", "$http", 'UserProfile', function ($q, $http, UserProfile) {
  return {
    require: "ngModel",
    scope: {
      email: "=emailAvailable"
    },
    link: function (scope, element, attributes, ngModel) {
      ngModel.$asyncValidators.emailAvailable = function (_email) {
        var deferred = $q.defer();
        if (scope.$parent.isUpdate)
          return $q.resolve();
        else
          $http({
            method: 'POST',
            url: __env.apiUrl + '/vendor-email-check',
            data: {
              api_key: UserProfile.getApiKey(),
              email: _email
            }
          }).then(function (response) {
            var rep = response.data.message;
            if (rep != "Email available") {
              deferred.resolve();
            } else {
              deferred.reject();
            }
          });
        return deferred.promise;
      };
    }
  };
}]);
