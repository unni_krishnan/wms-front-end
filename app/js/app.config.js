var env = {};

// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
}
var app = angular.module('app')
    .constant('__env', env)
    .config(
        ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$logProvider', '__env',

            function($controllerProvider, $compileProvider, $filterProvider, $provide, $logProvider, __env) {

                // lazy controller, directive and service
                app.controller = $controllerProvider.register;
                app.directive = $compileProvider.directive;
                app.filter = $filterProvider.register;
                app.factory = $provide.factory;
                app.service = $provide.service;
                app.constant = $provide.constant;
                app.value = $provide.value;
                $logProvider.debugEnabled(__env.enableDebug);
            }
        ])
    .config(['$cookiesProvider', function($cookiesProvider) {
        // Set $cookies defaults
        var exp_date = new Date();
        exp_date.setDate(exp_date.getDate() + 1);
        $cookiesProvider.defaults.path = '/';
        $cookiesProvider.defaults.secure = false;
        // $cookiesProvider.defaults.expires = exp_date;
        // $cookiesProvider.defaults.domain = __env.domain;
    }])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push(function($q, $rootScope) {

            var counter = 0;
            $rootScope.loading = false;

            var seeCounter = function(i) {
                counter += i;
                if (counter > 0) {
                    $rootScope.loading = true;
                } else {
                    $rootScope.loading = false;
                }
            };

            return {
                request: function(config) {
                    seeCounter(1);
                    return config;
                },
                response: function(response) {
                    seeCounter(-1);
                    return response;
                },
                responseError: function(rejection) {
                    seeCounter(-1);
                    return $q.reject(rejection);
                }
            };
        });
    }]);