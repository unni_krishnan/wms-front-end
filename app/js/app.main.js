'use strict';
angular.module('app').controller('AppCtrl', ['$scope', '$q', '$state', '$http', '$log', '$uibModal', '$filter', '$cookies', 'Access', 'UserProfile', 'AlertService', '$timeout',
    function($scope, $q, $state, $http, $log, $modal, $filter, $cookies, Access, UserProfile, AlertService, $timeout) {

        var menufold = false;
        var screenWidth = window.innerWidth;
        if (screenWidth < 767) {
            var menufold = true;
        }

        $scope.app = {
            name: 'WMS',
            version: '0.0.1',
            color: {
                primary: '#673AB7',
                accent: '#FF6E40',
                info: '#26C6DA',
                success: '#46be8a',
                warning: '#fdb45d',
                danger: '#F44336',
                secondary: '#a9a9a9',
                text: '#767676'
            },
            settings: {
                menuProfile: true,
                menuFolded: menufold,
                chatFolded: true,
                layoutBoxed: false,
                searchFocus: false,
                pagetitle: 'WMS',
            }
        }

        // $scope.selectedWarehouse = $cookies.getObject('selectedWarehouse');
        // $scope.warehouseList = $cookies.getObject('warehouseList');

        // $scope.setWarehouse = function (warehouseIndex) {
        //   $cookies.putObject('selectedWarehouse', $filter('filter')($scope.warehouseList, { warehouse_id: warehouseIndex }, true)[0]);
        //   $scope.selectedWarehouse = $filter('filter')($scope.warehouseList, { warehouse_id: warehouseIndex }, true)[0];
        //   $state.go($state.current, {}, { reload: true });

        $scope.logout = function() {
            $http.post(__env.apiUrl + '/admin-logout', {
                'api_key': UserProfile.getApiKey()
            }).then(function(response) {
                $log.debug("Logout Response Data ", response);
                if (!response.data.error) {
                    UserProfile.clearUserProfile();
                    $state.go('access.login', {}, { reload: true });
                } else {
                    if (response.data.message == 'Invalid api key') {
                        UserProfile.clearUserProfile();
                        $state.go('access.login', {}, { reload: true });
                    }
                }
            });
        }

        $scope.menuChatToggle = function(type, value) {
            if (type == "menu" && !value) {
                $scope.app.settings.chatFolded = true;
            }
            if (type == "chat" && !value) {
                $scope.app.settings.menuFolded = true;
            }
        }
        $scope.changeMenuHeight = function() {
            //console.log($scope.settings.menuProfile);
            if ($scope.app.settings.menuFolded == true) {
                var navHeight = angular.element("#main-content section.wrapper .content-wrapper").innerHeight() + 90;
            } else {
                var navHeight = $(window).innerHeight() - 60;
            }
            //console.log(navHeight);
            angular.element("#main-menu-wrapper").height(navHeight);
        }
        $scope.$watch('app.settings.menuFolded', function() {
            $scope.changeMenuHeight();
        });

        // var onStateChange =
        $scope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                $log.debug("======================State Changed ================================");
                // }
                $scope.profileData = UserProfile.getUserProfile();
                if ($scope.profileData && $scope.profileData.profile_image)
                    $scope.profileImagePath = __env.uploadUrl + '/user/' + $scope.profileData.profile_image;

                $scope.isSuperAdmin = Access.isSuperAdmin() == Access.OK ? true : false;
                $scope.isAuthenticated = Access.isAuthenticated() == Access.OK ? true : false;
                console.log("IS autthenticated ", $scope.isAuthenticated);
                $scope.hasVendorPrivilege = Access.hasRole(1) == Access.OK ? true : false;
                $scope.hasCustomerPrivilege = Access.hasRole(2) == Access.OK ? true : false;
                $scope.hasItemSetupPrivilege = Access.hasRole(3) == Access.OK ? true : false;
                $scope.hasPurchasingPrivilege = Access.hasRole(4) == Access.OK ? true : false;
                $scope.hasOrderManagementPrivilege = Access.hasRole(5) == Access.OK ? true : false;
                $scope.hasInventoryPrivilege = Access.hasRole(6) == Access.OK ? true : false;
                $scope.hasReportsPrivilege = Access.hasRole(7) == Access.OK ? true : false;

                if (!$scope.isAuthenticated && toState.name != 'access.login') {
                    event.preventDefault();
                    $state.go('access.login');
                } else if ($scope.isAuthenticated && toState.name == 'access.login') {
                    event.preventDefault();
                    $state.go('app.dashboard');
                }
            });

        $scope.$on('$viewContentLoaded', function(next, current) {
            angular.element(document).ready(function() {
                $scope.changeMenuHeight();
            });
        });
        $scope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
            console.log("State Change ERROR", error);
            switch (error) {

                case Access.UNAUTHORIZED:
                    UserProfile.clearUserProfile();
                    $state.go("access.login");
                    break;

                case Access.FORBIDDEN:
                    $state.go("forbidden");
                    break;
                case Access.INVALID_API:
                    var modalInstance = $modal.open({
                        templateUrl: 'partials/alert-warning.html',
                        controller: 'WarningModalCtrl',
                        resolve: {
                            data: function() {
                                var warning = {};
                                warning.title = 'Session Expired';
                                warning.body = 'It seems your session has expired. Please re-login';
                                warning.showCancel = false;
                                return warning;
                            }
                        }
                    });

                    modalInstance.result.then(function(selection) {
                        UserProfile.clearUserProfile();
                        $state.go("access.login");
                    }, function() {
                        UserProfile.clearUserProfile();
                        $state.go("access.login");
                    });
                    break;
                default:
                    console.log("$stateChangeError event ");
                    // $state.go("access.login");
                    break;

            }

        });
        $scope.ElementInView = function(inview, event, addclass, removeclass) {
            var id = event.inViewTarget.id;
            /*console.log(event);  */
            if (inview && id != "") {
                if (addclass != "") {
                    $("#" + id).addClass(addclass);
                } else {
                    $("#" + id).removeClass(removeclass);
                }
            }
            return false;
        }
        $scope.testLines = [];
        for (var i = 20; i >= 0; i--) {
            $scope.testLines.push(i);
        };
        $scope.lineInView = function(index, inview, inviewpart, event) {
            /*console.log(inview+" "+index+" "+inviewpart+" "+event);    */
            /*console.log(event.inViewTarget.id);  */
            return false;
        }
    }
]);