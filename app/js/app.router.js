"use strict";

/**
 * Config for the router
 */
angular.module("app").config([
  "$stateProvider",
  "$urlRouterProvider",
  "JQ_CONFIG",
  "$locationProvider",
  function($stateProvider, $urlRouterProvider, JQ_CONFIG, $locationProvider) {
    if (window.history && window.history.pushState) {
      //$locationProvider.html5Mode(true); will cause an error $location in HTML5 mode requires a  tag to be present! Unless you set baseUrl tag after head tag like so: <head> <base href="/">

      // to know more about setting base URL visit: https://docs.angularjs.org/error/$location/nobase

      // if you don't wish to set base URL then use this
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });
    }

    // $locationProvider.html5Mode(true);

    $urlRouterProvider
      .when("/activate-account/{activationCode}", "/activate/{activationCode}")
      .otherwise("/login");

    $stateProvider

      .state("app", {
        abstract: true,
        url: "",
        templateUrl: "partials/app.html",
        resolve: {
          deps: [
            "$ocLazyLoad",
            function($ocLazyLoad) {
              return $ocLazyLoad.load("cgNotify");
            }
          ]
        }
      })
      .state("app.dashboard", {
        url: "/dashboard",
        templateUrl: "partials/app_dashboard.html",
        controller: "DashboardLineCtrl",
        resolve: {
          deps: [
            "$ocLazyLoad",
            function($ocLazyLoad) {
              // .then(
              return $ocLazyLoad
                .load("chart.js").then(function() {
                  return $ocLazyLoad.load("js/controllers/dashboard.js").then(function(){
                    return $ocLazyLoad.load("countTo").then(function() {
                      return $ocLazyLoad.load("ngMorris")

                    })

                  });
                });
              // .then(
              //   function () {
              //     return $ocLazyLoad.load('../bower_components/font-awesome/css/font-awesome.css');
              //   }
              // );
            }
          ]
        }
      })
      .state("access", {
        url: "",
        template: '<div ui-view class=""></div>',
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/login.js"]);
            }
          ]
        }
      })
      .state("access.login", {
        url: "/login",
        templateUrl: "partials/login.html"
      })

      /**
       * Warehouse Section routes
       */
      .state("app.warehouse", {
        abstract: true,
        url: "",
        template: '<div ui-view class=""></div>',
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.isSuperAdmin() == Access.OK)
                return Access.isApiKeyValid();
              else return $q.reject(Access.FORBIDDEN);
            }
          ],
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/warehouse.js"]);
            }
          ]
        }
      })
      .state("app.warehouse.list", {
        url: "/warehouse-list",
        templateUrl: "partials/warehouse-list.html",
        controller: "WarehouseController",
        resolve: {
          validApi: [
            "Access",
            function(Access) {
              return Access.isApiKeyValid();
            }
          ]
        }
      })
      .state("app.warehouse.detail", {
        url: "/warehouse/{warehouseId}",
        templateUrl: "partials/warehouse-detail.html",
        controller: "LocationController",
        resolve: {
          locationTypes: function($http) {
            return $http
              .get(__env.apiUrl + "/location-types")
              .then(function(response) {
                return response.data;
              });
          },
          locationCount: function($http, $stateParams, UserProfile) {
            return $http
              .post(__env.apiUrl + "/location-count", {
                api_key: UserProfile.getApiKey(),
                warehouse_id: $stateParams.warehouseId
              })
              .then(function(response) {
                console.log("Location Count Response ", response);
                return response.data;
              });
          },
          locationBarcode: function() {
            return;
          },
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/location.js"]);
            }
          ]
        },
        validApi: [
          "Access",
          function(Access) {
            return Access.isApiKeyValid();
          }
        ]
      })
      .state("app.warehouse.location", {
        url: "",
        template: '<div ui-view class=""></div>',
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load([
                "js/controllers/location.js",
                "js/directives/file-upload.js"
              ]);
            }
          ],
          validApi: [
            "Access",
            function(Access) {
              return Access.isApiKeyValid();
            }
          ]
        }
      })
      .state("app.warehouse.location.list", {
        url: "/warehouse/{warehouseId}/location/{typeId}/{status}",
        templateUrl: "partials/location-list.html",
        controller: "LocationController",
        resolve: {
          locationTypes: function($http, __env) {
            return $http({
              method: "GET",
              url: __env.apiUrl + "/location-types"
            }).then(function(data) {
              return data.data;
            });
          },
          locationCount: function($http, $stateParams, UserProfile) {
            return $http({
              method: "POST",
              url: __env.apiUrl + "/location-count",
              data: {
                api_key: UserProfile.getApiKey(),
                warehouse_id: $stateParams.warehouseId
              }
            }).then(function(response) {
              return response.data;
            });
          },

          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              return $ocLazyLoad
                .load("angularBarCode")
                .then(function() {
                  return uiLoad.load(["js/directives/ngprint.js"]);
                })
                .then(
                  $ocLazyLoad.load("smart-table").then(function() {
                    return $ocLazyLoad.load("js/controllers/table-smart.js");
                  })
                );
            }
          ],
          validApi: [
            "Access",
            function(Access) {
              return Access.isApiKeyValid();
            }
          ]
        }
      })

      /**
       * Users Section
       */
      .state("app.users", {
        abstract: true,
        url: "",
        template: '<div ui-view class=""></div>',
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.isSuperAdmin() == Access.OK)
                return Access.isApiKeyValid();
              else return $q.reject(Access.FORBIDDEN);
            }
          ],
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/user.js"]);
            }
          ]
        }
      })
      .state("app.users.list", {
        url: "/users",
        templateUrl: "partials/users.html",
        controller: "UserController",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/directives/email-check.js"]);
            }
          ]
        }
      })
      .state("app.users.detail", {
        url: "/user/{userId}",
        templateUrl: "partials/user-detail.html",
        controller: "UserDetailCtrl",
        resolve: {
          deps: [
            "$ocLazyLoad",
            function($ocLazyLoad) {
              return $ocLazyLoad.load("xeditable");
            }
          ],
          userData: function($stateParams, UserService) {
            return UserService.listUser({
              user_id: $stateParams.userId
            }).then(function(response) {
              return response.data.data;
            });
          },
          warehouseList: function($http, __env, UserProfile) {
            return $http({
              method: "POST",
              url: __env.apiUrl + "/warehouse-getrights",
              data: {
                api_key: UserProfile.getApiKey()
              }
            }).then(function(response) {
              return response.data.data;
            });
          },
          userRights: function($http, __env, UserProfile) {
            return $http({
              method: "POST",
              url: __env.apiUrl + "/user-getrights",
              data: {
                api_key: UserProfile.getApiKey()
              }
            }).then(function(response) {
              return response.data.data;
            });
          }
        }
      })
      .state("activate", {
        url: "/activate/{activationCode}",
        templateUrl: "partials/user-activate.html",
        controller: function(
          $scope,
          $stateParams,
          $state,
          $http,
          $timeout,
          $interval,
          __env,
          UserProfile
        ) {
          $scope.isWaiting = true;
          $scope.response = {};
          console.log("Activation Code ", $stateParams.activationCode);
          $http({
            url: __env.apiUrl + "/admin-activate",
            method: "POST",
            data: {
              activation_code: $stateParams.activationCode
            }
          }).then(function(response) {
            console.log("Activation Response ", response);
            $scope.isWaiting = false;
            if (response.data.error) {
              $scope.response.error = true;
              $scope.response.message = "Activation Error";
              $scope.response.errorMessage = response.data.message;
            } else {
              $scope.response.error = false;
              $scope.response.message = "Activation sucessfull";
              $scope.counter = 5;
              $interval(
                function() {
                  $scope.counter--;
                },
                1000,
                0
              );
              $timeout(function() {
                UserProfile.setUserProfile(response.data.data);
                $state.go("app.dashboard");
              }, 5000);
            }
          });
        },
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load([
                "../bower_components/font-awesome/css/font-awesome.css"
              ]);
            }
          ]
        }
      })
      .state("forbidden", {
        url: "/forbidden",
        templateUrl: "partials/403.html"
      })
      .state("access.500", {
        url: "/500",
        templateUrl: "partials/500.html"
      })
      .state("app.profile", {
        url: "/profile",
        templateUrl: "partials/profile.html",
        controller: "ProfileController",
        resolve: {
          validApi: [
            "Access",
            function(Access) {
              return Access.isApiKeyValid();
            }
          ],
          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              return $ocLazyLoad.load("xeditable").then(function() {
                return uiLoad.load([
                  "js/controllers/profile.js",
                  "js/directives/file-upload.js"
                ]);
              });
            }
          ],
          companyDetail: [
            "UserService",
            function(UserService) {
              return UserService.getCompanyDetail().then(function(response) {
                if (!response.data.error) return response.data.data[0];
              });
            }
          ]
        }
      })
      /**
       * Vendor Section
       */
      .state("app.vendors", {
        url: "/vendors",
        templateUrl: "partials/vendors.html",
        controller: "VendorController",
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.hasRole(1) == Access.OK) return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ],
          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              return $ocLazyLoad.load("xeditable").then(function() {
                return uiLoad.load([
                  "js/controllers/vendor.js",
                  "js/directives/email-check.js"
                ]);
              });
            }
          ]
        }
      })

      /**
       * Customer Section
       */

      .state("app.customers", {
        url: "/customers",
        templateUrl: "partials/customers.html",
        controller: "CustomerController",
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.hasRole(2) == Access.OK) return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ],
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load([
                "js/controllers/customer.js",
                "js/directives/file-upload.js"
              ]);
            }
          ]
        }
      })
      .state("app.customer-detail", {
        url: "/customer/{customerId}",
        templateUrl: "partials/customer-detail.html",
        controller: "CustomerDetailController",
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.hasRole(2) == Access.OK) return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ],
          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              return $ocLazyLoad.load("xeditable").then(function() {
                return uiLoad.load([
                  "js/controllers/customer.js",
                  "js/directives/file-upload.js"
                ]);
              });
            }
          ],
          customerData: function($stateParams, CustomerService) {
            return CustomerService.listCustomer({
              customer_id: $stateParams.customerId
            }).then(function(response) {
              return response.data.data;
            });
          }
        }
      })
      /**Item Section */
      .state("app.items", {
        abstract: true,
        url: "",
        template: '<div ui-view class=""></div>',
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.hasRole(3) == Access.OK) return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ],
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load([
                "js/controllers/item.js",
                "js/directives/file-upload.js"
              ]);
            }
          ]
        }
      })
      .state("app.items.list", {
        url: "/items",
        templateUrl: "partials/items.html",
        controller: "ItemController",
        resolve: {
          userData: function($http, UserProfile) {
            return $http
              .post(__env.apiUrl + "/item-namelist", {
                api_key: UserProfile.getApiKey()
              })
              .then(function(response) {
                return response.data.data;
              });
          }
        }
      })
      .state("app.item-detail", {
        url: "/item/{itemId}",
        templateUrl: "partials/item-detail.html",
        controller: "ItemDetailController",
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.hasRole(3) == Access.OK) return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ],
          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              return $ocLazyLoad.load("xeditable").then(function() {
                return uiLoad.load([
                  "js/controllers/item.js",
                  "js/directives/file-upload.js"
                ]);
              });
            }
          ],
          userData: function($http, UserProfile) {
            return $http
              .post(__env.apiUrl + "/item-namelist", {
                api_key: UserProfile.getApiKey()
              })
              .then(function(response) {
                return response.data.data;
              });
          },
          itemData: function($stateParams, ItemService) {
            return ItemService.listItem({
              item_id: $stateParams.itemId
            }).then(function(response) {
              return response.data.data;
            });
          }
        }
      })
      /**Purchasing Section */
      .state("app.purchasing", {
        abstract: true,
        url: "",
        template: '<div ui-view class=""></div>',
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.hasRole(4) == Access.OK) return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ],
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/purchase-order.js"]);
            }
          ]
        }
      })
      .state("app.purchasing.po-list", {
        url: "/purchase-order",
        templateUrl: "partials/purchase-order-list.html",
        controller: "PurchaseOrderController"
      })
      .state("app.purchasing.detail", {
        url: "/order/{poId}",
        templateUrl: "partials/purchase-order-detail.html",
        controller: "PurchaseOrderDetailController",
        resolve: {
          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              return $ocLazyLoad.load("xeditable").then(function() {
                return uiLoad.load([
                  "js/controllers/purchase-order.js",
                  "js/directives/ngprint.js"
                ]);
              });
            }
          ],
          userList: function($http, UserProfile) {
            return $http
              .post(__env.apiUrl + "/item-namelist", {
                api_key: UserProfile.getApiKey()
              })
              .then(function(response) {
                return response.data.data;
              });
          },
          purchaseData: function($stateParams, PurchaseOrderService) {
            return PurchaseOrderService.listPO($stateParams.poId).then(function(
              response
            ) {
              console.log("response ", response);
              return response.data.data;
            });
          }
        }
      })
      .state("app.asn", {
        url: "/asn",
        templateUrl: "partials/asn-list.html",
        controller: "ASNController",
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.hasRole(4) == Access.OK) return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ],
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/asn.js"]);
            }
          ]
        }
      })
      .state("app.asnDetail", {
        url: "/asn/{poId}",
        templateUrl: "partials/asn-details.html",
        controller: "AsnviewControl",
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.hasRole(4) == Access.OK) return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ],
          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              return $ocLazyLoad.load("xeditable").then(function() {
                return uiLoad.load([
                  "js/controllers/asn.js",
                  "js/directives/ngprint.js"
                ]);
              });
            }
          ],
          // userList: function() { return null},
          userList: function($http, UserProfile) {
            return $http
              .post(__env.apiUrl + "/item-namelist", {
                api_key: UserProfile.getApiKey()
              })
              .then(function(response) {
                return response.data.data;
              });
          },
          // purchaseData:function(){return null}
          purchaseData: function($stateParams, PurchaseOrderService) {
            return PurchaseOrderService.listAsn($stateParams.poId).then(
              function(response) {
                console.log("response ", response);
                return response.data.data;
              }
            );
          }
        }
      })
      /**
       * Inventory Management
       */
      .state("app.inventory", {
        url: "/inventory",
        abstract: true,
        template: '<div ui-view class=""></div>',
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.hasRole(6) == Access.OK) return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ],
          warehouseList: function($http, UserProfile) {
            return $http({
              method: "POST",
              url: __env.apiUrl + "/warehouse-getrights",
              data: {
                api_key: UserProfile.getApiKey()
              }
            }).then(function(response) {
              return response.data.data;
            });
          },
          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              return $ocLazyLoad.load("angularBarCode").then(function() {
                return uiLoad.load([
                  "js/controllers/inventory.js",
                  "js/directives/focus-on.js",
                  "js/directives/ngprint.js"
                ]);
              });
            }
          ]
        }
      })
      .state("app.inventory.dashboard", {
        url: "/dashboard",
        templateUrl: "partials/inventory-dashboard.html",
        controller: "InventoryDashboardController",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/inventory-dashboard.js"]);
            }
          ]
        }
      })
      .state("app.inventory.live", {
        url: "/live",
        templateUrl: "partials/inventory-live.html",
        controller: "InventoryLiveController"
      })
      .state("app.inventory.movement", {
        url: "/movement",
        templateUrl: "partials/inventory-movement.html",
        controller: "InventoryTransferController"
      })
      .state("app.inventory.quick_transfer", {
        url: "/quick-transfer",
        templateUrl: "partials/inventory-quick-transfer.html",
        controller: "InventoryTransferController"
      })
      .state("app.inventory.receving", {
        url: "/receving",
        templateUrl: "partials/item-receving.html",
        controller: "RecevingController",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load([
                "js/controllers/receving.js",
                "js/directives/file-upload.js",
                "js/directives/ngprint.js"
              ]);
            }
          ]
        }
      })
      .state("app.inventory.ItemReceive", {
        url: "/warehouse/{asn_number}",
        templateUrl: "partials/item-recieve-start.html",
        controller: "ItemRecieveStartCntrl",
        resolve: {
          id_number: function($stateParams, $http) {
            return $stateParams.asn_number;
          },
          warehouseList: function($http, UserProfile) {
            return $http
              .post(__env.apiUrl + "/warehouse-getrights", {
                api_key: UserProfile.getApiKey()
              })
              .then(function(response) {
                return response.data.data;
              });
          }
        }
      })

      .state("app.inventory.adjustments", {
        url: "/adjustments",
        templateUrl: "partials/inventory-adjustments.html",
        controller: "InventoryAdjustmentController",
        resolve: {
          warehouseList: function($http, UserProfile) {
            return $http
              .post(__env.apiUrl + "/warehouse-getrights", {
                api_key: UserProfile.getApiKey()
              })
              .then(function(response) {
                return response.data.data;
              });
          }
        }
      })
      .state("app.inventory.ownership", {
        url: "/ownership",
        templateUrl: "partials/inventory-ownership.html",
        controller: "InventoryOwnershipController"
      })
      .state("app.inventory.blindcount", {
        url: "/cyclecount/blind",
        templateUrl: "partials/inventory-cyclecount.html",
        controller: "InventoryCycleCountController",
        resolve: {
          showCount: function() {
            return false;
          },
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/directives/ngprint.js"]);
            }
          ]
        }
      })
      .state("app.inventory.systemcount", {
        url: "/cyclecount/system",
        templateUrl: "partials/inventory-cyclecount.html",
        controller: "InventoryCycleCountController",
        resolve: {
          showCount: function() {
            return true;
          },
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/directives/ngprint.js"]);
            }
          ]
        }
      })
      .state("app.inventory.pallet", {
        url: "/pallet",
        templateUrl: "partials/pallet-id.html",
        controller: "PalletIdController",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/pallet-id.js"]);
            }
          ]
        }
      })
      /**
       * Order Management
       */
      .state("app.order", {
        url: "/orderManagement",
        abstract: true,
        template: '<div ui-view class=""></div>',
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.hasRole(5) == Access.OK) return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ]
        }
      })

      .state("app.order.dashboard", {
        url: "/dashboard",
        templateUrl: "partials/order-dashboard.html",
        controller: "DashboardCtroller",
        resolve: {
          deps: [
            "$ocLazyLoad",
            function($ocLazyLoad) {
              return $ocLazyLoad.load("chart.js").then(function() {
                return $ocLazyLoad.load("js/controllers/order.js");
              });
            }
          ]
        }
      })
      .state("app.order.shiped-order-details", {
        url: "/shiped-order-details/{orderiD}/{id}",
        templateUrl: "partials/order-label-details.html",
        controller: "shipedOrderController",
        resolve: {
          orderiD: function($stateParams) {
            return $stateParams.orderiD;
          },
          box_id: function($stateParams) {
            return $stateParams.id;
          }
        }
      })
      .state("app.order.pick-list", {
        url: "/picklist",
        templateUrl: "partials/order-pick-list.html",
        controller: "orderPickListController",
        resolve: {
          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              return $ocLazyLoad.load("angularBarCode").then(function() {
                return uiLoad.load([
                  "js/controllers/order.js",
                  "js/directives/focus-on.js"
                ]);
              });
            }
          ]
        }
      })
      .state("app.order.picking", {
        url: "/picking",
        templateUrl: "partials/order-picking.html",
        controller: "orderPickingController",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load([
                "js/controllers/order-picking.js",
                "js/directives/focus-on.js"
              ]);
            }
          ]
        }
      })
      .state("app.order.packing", {
        url: "/packing",
        templateUrl: "partials/order-packing.html",
        controller: "orderPackingController",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load([
                "js/controllers/order-packing.js",
                "js/directives/focus-on.js"
              ]);
            }
          ]
        }
      })
      .state("app.order.packship", {
        url: "/packship",
        templateUrl: "partials/order-packship.html",
        controller: "packShipController",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load([
                "js/controllers/order-packship.js",
                "js/directives/focus-on.js"
              ]);
            }
          ]
        }
      })
      .state("app.order.order-list", {
        url: "/list",
        templateUrl: "partials/order-list.html",
        controller: "OrderListController",
        resolve: {
          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              return $ocLazyLoad.load("angularBarCode").then(function() {
                return uiLoad.load([
                  "js/directives/ngprint.js",
                  "js/controllers/order.js"
                ]);
              });
            }
          ],
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.isSuperAdmin == Access.OK)
                return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ]
        }
      })
      .state("app.order.shipping", {
        url: "/ordershipping",
        templateUrl: "partials/order-shipping.html",
        controller: "ShippingControllers",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/order.js"]);
            }
          ]
        }
      })
      .state("app.shipstation", {
        url: "/shipstation",
        templateUrl: "partials/shipstation-list.html",
        controller: "ShipStationController",
        resolve: {
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.isSuperAdmin == Access.OK)
                return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ],
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/shipstation.js"]);
            }
          ]
        }
      })

      .state("app.store-detail", {
        url: "/shipstation-detail/{storeId}",
        templateUrl: "partials/order-list.html",
        controller: "StoreController",
        resolve: {
          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              return $ocLazyLoad.load("angularBarCode").then(function() {
                return uiLoad.load([
                  "js/directives/ngprint.js",
                  "js/controllers/shipstation.js"
                ]);
              });
            }
          ],
          validApi: [
            "Access",
            "$q",
            function(Access, $q) {
              if (Access.isSuperAdmin == Access.OK)
                return Access.isApiKeyValid();
              else $q.reject(Access.FORBIDDEN);
            }
          ]
        }
      })
      .state("app.report", {
        url: "/report",
        templateUrl: "partials/report.html",
        controller: "ReportCntrl",
        resolve: {
          deps: [
            "$ocLazyLoad",
            "uiLoad",
            function($ocLazyLoad, uiLoad) {
              // .then(
              return $ocLazyLoad.load("countTo").then(function() {
                return $ocLazyLoad.load("chart.js").then(function() {
                  return $ocLazyLoad.load("ngMorris").then(function() {
                    return uiLoad.load(["js/controllers/report.js"]);
                  });
                });
              });
            }
          ]
        }
      })

      .state("app.eco-category-edit", {
        url: "/category-edit",
        templateUrl: "partials/eco-category-edit.html",
        resolve: {
          validApi: [
            "Access",
            function(Access) {
              return Access.isApiKeyValid();
            }
          ]
        }
      })
      .state("app.eco-tags", {
        url: "/tags",
        templateUrl: "partials/eco-tags.html"
      })
      .state("app.eco-tag-add", {
        url: "/tag-add",
        templateUrl: "partials/eco-tag-add.html"
      })
      .state("app.eco-tag-edit", {
        url: "/tag-edit",
        templateUrl: "partials/eco-tag-edit.html"
      })

      .state("app.eco-user-add", {
        url: "/user-add",
        templateUrl: "partials/eco-user-add.html"
      })
      .state("app.eco-user-edit", {
        url: "/user-edit",
        templateUrl: "partials/eco-user-edit.html"
      })
      .state("app.mail", {
        abstract: true,
        url: "/mail",
        //template: '<div ui-view class=""></div>',
        templateUrl: "partials/mail.html",
        // use resolve to load other dependences
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load([
                "../bower_components/font-awesome/css/font-awesome.css",
                "js/controllers/mail.js",
                "js/services/mail-service.js",
                JQ_CONFIG.moment
              ]);
            }
          ]
        }
      })
      .state("app.eco-orders", {
        url: "/orders",
        templateUrl: "partials/eco-orders.html"
      })
      .state("app.eco-order-add", {
        url: "/order-add",
        templateUrl: "partials/eco-order-add.html"
      })
      .state("app.eco-order-edit", {
        url: "/order-edit",
        templateUrl: "partials/eco-order-edit.html"
      })
      .state("app.mail.list", {
        url: "/{fold}",
        templateUrl: "partials/mail-list.html"
      })
      .state("app.mail.compose", {
        url: "/compose",
        templateUrl: "partials/mail-compose.html"
      })
      .state("app.mail.view", {
        url: "/{mailId:[0-9]{1,4}}",
        templateUrl: "partials/mail-view.html"
      })
      .state("app.eco-report-site", {
        url: "/report-site",
        templateUrl: "partials/eco-report-site.html",
        resolve: {
          deps: [
            "$ocLazyLoad",
            function($ocLazyLoad) {
              return $ocLazyLoad.load("chart.js").then(function() {
                return $ocLazyLoad.load("js/controllers/eco-report-site.js");
              });
            }
          ]
        }
      })
      .state("app.eco-report-visitors", {
        url: "/report-visitors",
        templateUrl: "partials/eco-report-visitors.html",
        resolve: {
          deps: [
            "$ocLazyLoad",
            function($ocLazyLoad) {
              return $ocLazyLoad.load("chart.js").then(function() {
                return $ocLazyLoad.load(
                  "js/controllers/eco-report-visitors.js"
                );
              });
            }
          ]
        }
      })
      .state("app.eco-report-statistics", {
        url: "/report-statistics",
        templateUrl: "partials/eco-report-statistics.html",
        resolve: {
          deps: [
            "$ocLazyLoad",
            function($ocLazyLoad) {
              return $ocLazyLoad.load([
                "js/controllers/eco-report-statistics.js"
              ]);
            }
          ]
        }
      })
      .state("app.eco-account-settings", {
        url: "/account-settings",
        templateUrl: "partials/eco-account-settings.html"
      })
      .state("app.eco-activity", {
        url: "/activity",
        templateUrl: "partials/eco-activity.html",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load([
                "../bower_components/font-awesome/css/font-awesome.css"
              ]);
            }
          ]
        }
      })
      .state("app.eco-members", {
        url: "/members",
        templateUrl: "partials/eco-members.html",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/eco-members.js"]);
            }
          ]
        }
      })
      .state("app.eco-member-add", {
        url: "/member-add",
        templateUrl: "partials/eco-member-add.html"
      })
      .state("app.eco-member-edit", {
        url: "/member-edit",
        templateUrl: "partials/eco-member-edit.html"
      })
      .state("app.eco-member-profile", {
        url: "/member-profile",
        templateUrl: "partials/eco-member-profile.html",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load([
                "../bower_components/font-awesome/css/font-awesome.css"
              ]);
            }
          ]
        }
      })
      .state("app.eco-friends", {
        url: "/friends",
        templateUrl: "partials/eco-friends.html",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/eco-friends.js"]);
            }
          ]
        }
      })
      .state("app.eco-upload", {
        url: "/upload",
        templateUrl: "partials/eco-upload.html",
        resolve: {
          deps: [
            "$ocLazyLoad",
            function($ocLazyLoad) {
              return $ocLazyLoad.load("angularFileUpload").then(function() {
                return $ocLazyLoad.load("js/controllers/eco-upload.js");
              });
            }
          ]
        }
      })
      .state("app.eco-media", {
        url: "/media",
        templateUrl: "partials/eco-media.html",
        resolve: {
          deps: [
            "uiLoad",
            function(uiLoad) {
              return uiLoad.load(["js/controllers/eco-media.js"]);
            }
          ]
        }
      });
  }
]);
